#!/usr/bin/env bash
set -e

echo "Generating backend"
stack2nix . > backend.nix

echo "Generating frontend"
cd frontend
spago2nix generate
cd ..

echo "Generating style"
cd style
node2nix --development -8 --composition package.nix --lock package-lock.json

