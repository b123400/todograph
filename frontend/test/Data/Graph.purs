module Test.Data.Graph where

import Prelude

import Control.Comonad.Cofree as Cofree
import Data.Array as Array
import Data.Default (default)
import Data.Either (Either(..), hush)
import Data.Graph as G
import Data.Ins as I
import Data.Lens ((^.), (.~))
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.Tree as T
import Data.Tuple (Tuple(..))
import Model.Relationship (Relationship(..))
import Test.Unit (TestSuite, suite, test)
import Test.Unit.Assert as Assert

main :: TestSuite
main = suite "Graph" do
  suite "RawGraph" do
    test "children" do
      let g = G.Graph { nodes: [1, 2, 3], relationships: Set.fromFoldable [Relationship 1 2, Relationship 1 3]  }
      Assert.equal (G.children 1 g) ([2, 3])

    test "addNode" do
      let g = G.Graph { nodes: [1, 2, 3], relationships: Set.empty }
          (G.Graph added) = G.addNode 4 g
      Assert.assert "contain node" (Array.elem 4 added.nodes)

    test "parents" do
      let g = G.Graph { nodes: [1, 2, 3], relationships: Set.fromFoldable [Relationship 2 1, Relationship 3 1]  }
      Assert.equal (G.parents 1 g) ([2, 3])

    test "pathExist" do
      let g = G.Graph { nodes: [1, 2, 3, 4], relationships: Set.fromFoldable [Relationship 1 2, Relationship 2 3, Relationship 3 4]  }
      Assert.assert "path exist" (G.pathExist 1 4 g)

    test "not pathExist" do
      let g = G.Graph { nodes: [1, 2, 3, 4], relationships: Set.fromFoldable [Relationship 1 2, Relationship 3 4]  }
      Assert.assertFalse "path exist" (G.pathExist 1 4 g)

    test "descendant" do
      let g = G.Graph { nodes: [1, 2, 3, 4, 5, 6]
                      , relationships: Set.fromFoldable [ Relationship 1 2
                                                        , Relationship 2 3
                                                        , Relationship 3 4
                                                        , Relationship 1 5
                                                        ]}
          desc = G.descendant 1 g
      Assert.equal (Set.fromFoldable [2, 3, 4, 5]) (Set.fromFoldable desc)

    suite "orderInGraph" do
      suite "no condition" do
        test "first = before second" do
          let g = G.Graph { nodes: [1, 2, 3, 4, 5], relationships: Set.empty }
              o = G.orderInGraph 1 Just g
          Assert.equal (T.Before 2) o

        test "after" do
          let g = G.Graph { nodes: [1, 2, 3], relationships: Set.empty }
              o = G.orderInGraph 2 Just g
          Assert.equal (T.After 1) o

        test "no order" do
          let g = G.Graph { nodes: [1], relationships: Set.empty }
              o = G.orderInGraph 1 Just g
          Assert.equal T.NoOrder o

      suite "with condition" do
        test "first = before second" do
          let g = G.Graph { nodes: [Right 1, Left 2, Right 3, Left 4], relationships: Set.empty }
              o = G.orderInGraph (Right 1) hush g
          Assert.equal (T.Before 3) o

        test "after" do
          let g = G.Graph { nodes: [Right 1, Left 2, Right 3], relationships: Set.empty }
              o = G.orderInGraph (Right 3) hush g
          Assert.equal (T.After 1) o

        test "no order" do
          let g = G.Graph { nodes: [Right 1 :: Either Int Int], relationships: Set.empty }
              o = G.orderInGraph (Right 1) hush g
          Assert.equal T.NoOrder o

  suite "BodyGraph" do
    test "add-get body" do
      let g = default :: G.WithBody String G.Graph Int
          added = G.addBody 1 "1" g
          got = G.getBody 1 added
      Assert.equal (Just "1") got

    test "deleteNode = deleteBody" do
      let g = default :: G.WithBody String G.Graph Int
          afterDelete = G.addBody 1 "1"
                    >>> G.deleteNode 1
                    >>> G.getBody 1
      Assert.equal Nothing (afterDelete g)

    test "deleteBody ins" do
      let g = default :: G.WithBody String G.Graph Int
          Tuple ins g = G.deleteBody 1 $ G.addBody 1 "1" g
      Assert.equal [I.DeleteBody 1 T.DeleteRelationships] ins

  suite "RelGraph" do
    test "addRelationship" do
      let g = G.Graph { nodes: [1,2], relationships: Set.empty }
          Tuple ins (G.Graph { relationships }) = G.addRelationship 1 2 g
      Assert.equal ([I.AddRel 1 2] :: Array (I.Ins Int Int)) ins
      Assert.equal (Set.fromFoldable [Relationship 1 2]) relationships
    test "deleteRelationship" do
      let g = G.Graph { nodes: [1,2], relationships: Set.fromFoldable [Relationship 1 2]}
          Tuple ins (G.Graph { relationships }) = G.deleteRelationship 1 2 g
      Assert.equal ([I.DeleteRel 1 2] :: Array (I.Ins Int Int)) ins
      Assert.assert "relationship is empty" $ Set.isEmpty relationships

  suite "ForestGraph" do
    suite "shiftDown" do
      test "returns the correct path" do
        let g = (default :: G.WithForest G.Graph Int)
              # G.addNode 1
              # G.addNode 2
              # G.addNode 3
            Tuple (Tuple ins newG) newPath = G.shiftDown [2] g
        case newPath of
          Nothing -> Assert.assert "New path is Nothing" false
          Just p -> do
            Assert.equal [1,2] p
            Assert.assert "new path exists in new graph" $ T.pathExistsInForest p $ G.getForest newG

    suite "shiftUp" do
      test "returns the correct path" do
        let Tuple _ g = (default :: G.WithForest G.Graph Int)
                      # G.addNode 1
                      # G.addNode 2
                      # G.addNode 3
                      # G.addRelationship 1 2
            Tuple (Tuple ins newG) newPath = G.shiftUp [1,2] g
        case newPath of
          Nothing -> Assert.assert "New path is Nothing" false
          Just p -> do
            Assert.equal [2] p
            Assert.assert "new path exists in new graph" $ T.pathExistsInForest p $ G.getForest newG

    suite "move" do
      test "with correct order" do
        let Tuple _ g = flip I.runInsM_ (default :: G.WithForest G.Graph Int) $ do
              I.insM $ G.addNode 1
                   >>> G.addNode 2
                   >>> G.addNode 3
                   >>> G.addNode 4
              I.insM $ G.move [4] [4] (Just 1)
            G.Graph { nodes } = g ^. G.graphL
        Assert.equal [1,4,2,3] nodes

      test "reload tree" do
        let Tuple _ g = flip I.runInsM_ (default :: G.WithForest G.Graph Int) $ do
              I.insM $ G.addNode 1
                   >>> G.addNode 2
                   >>> G.addNode 3
                   >>> G.addNode 4
              I.insM $ G.move [4] [4] (Just 1)
            G.WithForest { forest } = g
            nodes = Cofree.head <$> forest
        Assert.equal [1,4,2,3] nodes

  suite "Seleced Graph" do
    test "delete node = reset select" do
      let g = default :: G.SelectedA G.Graph Int
          selected = (G.addNode 1 g) # G.selectedL .~ Just [1]
          deleted = G.deleteNode 1 selected
          afterDeleted = deleted ^. G.selectedL
      Assert.equal Nothing afterDeleted

    test "delete body = reset select" do
      let g = default :: G.SelectedA (G.WithBody Int G.Graph) Int
          selected = (G.addBody 1 1 g) # G.selectedL .~ Just [1]
          Tuple _ deleted = G.deleteBody 1 selected
          afterDeleted = deleted ^. G.selectedL
      Assert.equal Nothing afterDeleted
