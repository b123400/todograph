module Test.Data.Tree where


import Prelude

import Control.Comonad.Cofree (mkCofree)
import Data.Tree as T
import Test.Unit (TestSuite, suite, test)
import Test.Unit.Assert as Assert

main :: TestSuite
main = suite "Tree" do
  suite "pathExistsInForest" do
    test "success leaf" do
      let forest = [
        mkCofree 1 [
          mkCofree 2 [
            mkCofree 3 [
              mkCofree 4 []
            ]
          ]
        ]
      ]
      let exists = T.pathExistsInForest [1,2,3,4] forest
      Assert.assert "Path should exists" exists

    test "success branch" do
      let forest = [
        mkCofree 1 [
          mkCofree 2 [
            mkCofree 3 [
              mkCofree 4 []
            ]
          ]
        ]
      ]
      let exists = T.pathExistsInForest [1,2,3] forest
      Assert.assert "Path should exists" exists

    test "failure" do
      let forest = [
        mkCofree 1 [
          mkCofree 2 [
            mkCofree 3 [
              mkCofree 4 []
            ]
          ]
        ]
      ]
      let exists = T.pathExistsInForest [1,2,5,4] forest
      Assert.assertFalse  "Path should not exists" exists

  suite "applyOrderInArray" do
    test "NoOrder" do
      let orig = [1,2,3,4]
          after = T.applyOrderInArray 2 T.NoOrder orig
      Assert.equal orig after

    test "After down" do
      let orig = [1,2,3,4]
          after = T.applyOrderInArray 2 (T.After 4) orig
      Assert.equal [1,3,4,2] after

    test "After up" do
      let orig = [1,2,3,4]
          after = T.applyOrderInArray 3 (T.After 1) orig
      Assert.equal [1,3,2,4] after

    test "Before up" do
      let orig = [1,2,3,4]
          after = T.applyOrderInArray 3 (T.Before 1) orig
      Assert.equal [3,1,2,4] after

    test "Before down" do
      let orig = [1,2,3,4]
          after = T.applyOrderInArray 1 (T.Before 4) orig
      Assert.equal [2,3,1,4] after
