module Test.Data.Ins where

import Prelude

import API.Ins as AI
import Data.Default (default)
import Data.Ins as I
import Data.Maybe (Maybe(..))
import Data.Set.NonEmpty as SN
import Data.Tree (DependsOnMigration(..))
import Data.Tree as T
import Model.HybridTask (ComposingTaskId(..), TempId(..))
import Model.Task (Task(..))
import Test.Unit (TestSuite, suite, test)
import Test.Unit.Assert as Assert

main :: TestSuite
main = suite "Ins" do
  test "Group by Id" do
    let (Task defaultTask) = default
        task = Task defaultTask { body = "WOW" }
        ins = [ I.UpdateBody task "1"
              , I.AddRel "1" "2"
              , I.DeleteRel "3" "1"
              , I.DeleteRel "2" "3"
              ]
        reqs = AI.insToReqs ins
    Assert.equal [ AI.Update { id: "1"
                             , dependDiff: { createDependsOn: ["2"]
                                           , deleteDependedBy: ["3"]
                                           , createDependedBy: []
                                           , deleteDependsOn: []
                                           }
                             , body: Just task
                             }
                 , AI.Update { id: "2"
                             , dependDiff: { createDependsOn: []
                                           , deleteDependedBy: []
                                           , createDependedBy: []
                                           , deleteDependsOn: ["3"]
                                           }
                             , body: Nothing
                             }
                 ] reqs
  test "reqByEndpoint" do
    let req = AI.reqByEndpoint { ids: SN.singleton "1", ins: [I.AddRel "1" "2", I.DeleteBody "1" DeleteRelationships]}
    Assert.equal [ AI.Update { id: "1"
                             , dependDiff: { createDependsOn: ["2"]
                                           , deleteDependedBy: []
                                           , createDependedBy: []
                                           , deleteDependsOn: []
                                           }
                             , body: Nothing
                             }
                 , AI.Delete { id: "1"
                             , migration: DeleteRelationships
                             }
                 ] req
  test "batch by endpoint" do
    let (Task defaultTask) = default
        task = Task defaultTask { body = "WOW" }
        ins = [ I.UpdateBody task "1"
              , I.AddRel "1" "2"
              , I.DeleteBody "1" DeleteOrphans
              ]
        reqs = AI.insToReqs ins
    Assert.equal [ AI.Update { id: "1"
                             , dependDiff: { createDependsOn: ["2"]
                                           , deleteDependedBy: []
                                           , createDependedBy: []
                                           , deleteDependsOn: []
                                           }
                             , body: Just task
                             }
                 , AI.Delete { id: "1"
                             , migration: DeleteOrphans
                             }
                 ] reqs

  test "Merge reorder and AddRel, DeleteRel" do
    let ins = [ I.Reorder "1" (T.After "2")
              , I.AddRel "1" "2"
              , I.DeleteRel "1" "3"
              ]
        reqs = AI.insToReqs ins
    Assert.equal [ AI.Reorder { id: "1"
                              , dependDiff: { createDependsOn: ["2"]
                                            , createDependedBy: []
                                            , deleteDependsOn: ["3"]
                                            , deleteDependedBy: []
                                            }
                              , order: T.After "2"
                              }] reqs

  test "Ignore composing Id" do
    let ins = [ I.AddRel (Composing $ TempId "1") (Existing "2")
              , I.DeleteRel (Existing "3") (Composing $ TempId "1")
              , I.DeleteRel (Existing "2") (Existing "3")
              ]
        reqs = AI.insToReqs ins
    Assert.equal [ AI.Update { id: "2"
                             , dependDiff: { createDependsOn: []
                                           , deleteDependedBy: []
                                           , createDependedBy: []
                                           , deleteDependsOn: ["3"]
                                           }
                             , body: Nothing
                             }
                 ] reqs

  test "Make Create Req from Update(Composing)" do
    let (Task defaultTask) = default
        task = Task defaultTask { body = "WOW" }
        ins = [ I.UpdateBody task (Composing $ TempId "1")
              , I.AddRel (Composing $ TempId "1") (Existing "2")
              ]
        reqs = AI.insToReqs ins
    Assert.equal [ AI.Create { replaceId: Composing $ TempId "1"
                             }
                 ] reqs
