module Test.Main where

import Prelude

import Effect (Effect)
import Test.Data.Graph as Graph
import Test.Data.Ins as Ins
import Test.Data.Tree as Tree
import Test.Unit.Main (runTest)

main :: Effect Unit
main = runTest do
  Graph.main
  Ins.main
  Tree.main
