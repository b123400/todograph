module Router where

import Prelude

import Control.Alternative ((<|>))
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String.Common (joinWith, split)
import Data.String.Pattern (Pattern(..))
import Data.Tree (Path)
import Data.Tuple (Tuple(..))
import Model.Task (TaskId)
import Model.Graph (GraphId)
import Routing.Match (Match(..), lit, param, str, optionalMatch )

data Route = GraphList
           | TaskList GraphId (Maybe (Path TaskId)) Boolean
           | Index

routing :: Match Route
routing = TaskList <$> (route "graphs" *> str)
                   <*> (optionalMatch $ param "path" <#> split (Pattern "/"))
                   <*> (optionalMatch (param "includesCompleted" <#> ((==) "true")) <#> fromMaybe false)
      <|> GraphList <$ (route "graphs")
      <|> Index <$ anyRoute
  where
    route str = lit "" *> lit str

    anyRoute :: Match Unit
    anyRoute = Match \_route-> pure $ Tuple mempty unit

toUrl :: Route -> String
toUrl GraphList = "#/graphs"
toUrl (TaskList graphId mTaskIds showCompleted)
  = "#/graphs/" <> graphId <> params
  -- TODO: proper URL building
  where params = if Map.isEmpty paramsMap then "" else "?" <> paramsStr
        paramsMap = Map.mapMaybe identity $ Map.fromFoldable
                                          [(Tuple "path" $ joinWith "/" <$> mTaskIds)
                                          ,(Tuple "includesCompleted" $ if showCompleted then Just "true" else Nothing)
                                          ]
        paramsStr = joinWith "&" $ (\(Tuple a b)-> a <> "=" <> b) <$> Map.toUnfoldable paramsMap

toUrl Index = "#/graphs"
