module Router.Signal where

import Prelude
import Component.Main as CM
import Effect (Effect)
import Effect.Aff (Aff, launchAff)
import Effect.Class (liftEffect)
import Halogen as H
import Router (routing)
import Routing.Hash (matches)

routeSignal :: H.HalogenIO CM.Query Void Aff -> Aff (Effect Unit)
routeSignal driver = liftEffect do
  matches routing hashChanged
  where
    hashChanged _ newRoute = do
      _ <- launchAff $ driver.query <<< H.tell <<< CM.QuerySetState $ newRoute
      pure unit
