module Component.Main where

import Prelude

import Component.GraphList as GraphList
import Component.TaskListAndEdit as TaskListAndEdit
import Data.Maybe (Maybe(..))
import Data.Symbol (SProxy(..))
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Router (Route(..))
import Routing.PushState (makeInterface)
import Foreign.NullOrUndefined (null)

type State = Route

type Input = State
type Message = Void
data Action = SetState State
data Query a = QuerySetState State a

type Slots =
  ( graphList :: H.Slot GraphList.Query Void Unit
  , taskListAndEdit :: H.Slot TaskListAndEdit.Query Void Unit
  )

component :: forall m. MonadAff m => H.Component HH.HTML Query Input Message m
component =
  H.mkComponent
    { initialState: identity
    , render
    , eval: H.mkEval H.defaultEval
        { receive = Just <<< SetState
        , handleQuery = handleQuery
        , handleAction = handleAction
        }
    }
  where

  initialState :: State
  initialState = GraphList

  render :: State -> H.ComponentHTML Action Slots m
  render state = case state of
    Index -> HH.div_ []
    GraphList ->
      HH.slot (SProxy :: SProxy "graphList") unit GraphList.component unit absurd
    TaskList graphId taskPath showCompleted ->
      HH.slot
        (SProxy :: SProxy "taskListAndEdit")
        unit
        TaskListAndEdit.component
        { graphId, showCompleted, taskPath }
        (const Nothing)

  handleQuery :: forall a. Query a -> H.HalogenM State Action Slots Message m (Maybe a)
  handleQuery (QuerySetState state next) = do
    handleAction (SetState state)
    pure $ Just next

  handleAction :: Action -> H.HalogenM State Action Slots Message m Unit
  handleAction = case _ of
    SetState state -> do
      H.put state
      case state of
        Index -> do
          H.put GraphList
          H.liftEffect $ do
            nav <- makeInterface
            nav.pushState null "#/graphs"
        _ -> pure unit
