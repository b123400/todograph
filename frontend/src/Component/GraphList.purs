module Component.GraphList where

import Prelude

import API.Graph (getGraphs)
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Effect.Aff (message, try)
import Effect.Aff.Class (class MonadAff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Model.Entity (Entity(..))
import Model.Graph (Graph(..), GraphEntity)
import Router (Route(..), toUrl)

data State = Loading
           | Loaded (Array (GraphEntity))
           | Errored String
type Input = Unit
type Message = Void
type Slot = Unit
data Query a
data Action = LoadGraphs

type Slots = ()

component :: forall m. MonadAff m => H.Component HH.HTML Query Input Message m
component =
  H.mkComponent
    { initialState: const initialState
    , render
    , eval: H.mkEval H.defaultEval
        { handleAction = handleAction
        , initialize = Just LoadGraphs
        }
    }
  where

  initialState :: State
  initialState = Loading

  render :: State -> H.ComponentHTML Action Slots m
  render (Errored message) =
    HH.div_ [HH.text $ "Error: " <> message]

  render Loading =
    HH.div_ [HH.text "Loading..."]

  render (Loaded []) = HH.div_ [HH.text "You don't have any graph yet, ", HH.a [HP.href "/graphs/new"] [HH.text "click here to create one."]]

  render (Loaded graphs) =
    HH.div_
      $ [ HH.p [] [HH.text "Here are your graphs:"]
        , HH.ul_ $
             (graphs <#> \(Entity gId (Graph g))->
               HH.li_ [
                 HH.a [HP.href $ toUrl $ TaskList gId Nothing false] [HH.text g.name]
               ])
        , HH.p_ [
            HH.a [HP.href "/graphs/new"] [HH.text "Create new graph"]
          ]
        ]

  handleAction :: Action -> H.HalogenM State Action Slots Message m Unit
  handleAction = case _ of
    LoadGraphs -> do
      eGraphs <- H.liftAff $ try getGraphs
      case eGraphs of
        Left err -> H.put $ Errored $ message err
        Right graphs -> H.put $ Loaded graphs
      pure unit
