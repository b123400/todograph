module Component.TaskListAndEdit where

import Prelude

import API.Manager as APIM
import API.Task as API
import Component.TaskEditor as TaskEditor
import Component.TaskList as TaskList
import Data.Array (length, mapMaybe)
import Data.Graph as G
import Data.GraphManager as GM
import Data.Lens (preview, (^.), (.~))
import Data.Maybe (Maybe(..))
import Data.String.Random (randomString)
import Data.Symbol (SProxy(..))
import Data.Traversable (for_)
import Data.Tree (Path, isValidForestPath, firstPathWithLeaf)
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Foreign (unsafeToForeign)
import Halogen (ClassName(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HQ
import IxQueue as Q
import Model.Graph (GraphId)
import Model.HybridTask (ComposingTaskId(..))
import Model.Task (Task, TaskId)
import Model.Task.Class (taskIdP)
import Record as Record
import Router as Router
import Web.HTML (window)
import Web.HTML.History (pushState, DocumentTitle(..), URL(..))
import Web.HTML.HTMLDocument (title)
import Web.HTML.Location (hash)
import Web.HTML.Window (document, history, location)

type ComposedGraph = G.WithId GraphId (G.WithForest (G.SelectedA (G.WithBody Task G.Graph)))

type State =
  { graphManager :: Maybe (GM.GraphManager Task ComposedGraph ComposingTaskId)
  , graphId :: GraphId
  , cleanUp :: Effect Unit
  , showCompleted :: Boolean
  , inputTaskPath :: Maybe (Path TaskId) -- The task path in the url
  , cache :: Maybe { graph :: ComposedGraph ComposingTaskId }
  }


type Input =
  { graphId :: GraphId
  , showCompleted :: Boolean
  , taskPath :: Maybe (Path TaskId)
  }
type Message = Void
data Action = Initialise
            | UpdateInput Input
            | CleanUp
            | ReceivedGraphUpdate
            | TaskListEvent TaskList.Message
data Query a

type Slots =
  ( "taskList" :: H.Slot (TaskList.Query ComposedGraph) TaskList.Message Unit
  , "taskEditor" :: H.Slot (TaskEditor.Query ComposedGraph ComposingTaskId) Void Unit
  )
_taskList :: SProxy "taskList"
_taskList = SProxy
_taskEditor :: SProxy "taskEditor"
_taskEditor = SProxy

component :: forall m. MonadAff m => H.Component HH.HTML Query Input Message m
component =
  H.mkComponent
    { initialState: \{graphId, showCompleted, taskPath}->
      { graphId
      , showCompleted
      , inputTaskPath: taskPath
      , graphManager: Nothing
      , cleanUp: pure unit
      , cache: Nothing
      }
    , render
    , eval: H.mkEval H.defaultEval
        { receive = Just <<< UpdateInput
        , handleAction = handleAction
        , initialize = Just Initialise
        , finalize = Just CleanUp
        }
    }
  where

  render :: State -> H.ComponentHTML Action Slots m
  render {graphManager: Just gm, showCompleted } =
    HH.div
      [ HP.class_ $ ClassName "task-list-and-edit" ]
      ([ HH.div [ HP.class_ $ ClassName "task-list-and-edit__task-list" ] $ [ HH.slot _taskList unit TaskList.component { graphManager: gm, includesCompleted: showCompleted } (Just <<< TaskListEvent) ]
       , taskEditorDiv gm
       ])
  render _ = HH.div [] []

  taskEditorDiv gm =
    HH.div [ HP.class_ $ ClassName "task-list-and-edit__edit" ] [ HH.slot _taskEditor unit TaskEditor.component gm (const Nothing) ]

  handleAction :: Action -> H.HalogenM State Action Slots Message m Unit
  handleAction = case _ of
    Initialise -> do
      { graphId } <- H.get
      _ <- updateGraphManager
      pure unit

    UpdateInput {graphId, showCompleted, taskPath} -> do
      existing@{ graphManager } <- H.get

      if (graphId /= existing.graphId || showCompleted /= existing.showCompleted || taskPath /= existing.inputTaskPath)
        then H.modify_ $ Record.merge {graphId, showCompleted, inputTaskPath: taskPath}
        else pure unit

      if (graphId /= existing.graphId || showCompleted /= existing.showCompleted)
        then do
          gm <- updateGraphManager
          _ <- H.query _taskList unit $ H.tell $ TaskList.NewInput { graphManager: gm, includesCompleted: showCompleted }
          _ <- H.query _taskEditor unit $ H.tell $ TaskEditor.UpdateGraphManager gm
          pure unit
        else if (taskPath /= existing.inputTaskPath)
        then for_ graphManager $ \gm -> H.liftEffect $ GM.modify' gm $ updateGraphWithSelectedIdValidated (map Existing <$> taskPath)
        else pure unit

    CleanUp -> do
      { cleanUp } <- H.get
      H.liftEffect cleanUp

    ReceivedGraphUpdate -> do
      { graphId, cache, graphManager, showCompleted } <- H.get
      case {c: cache, g: graphManager} of
        {c: Just { graph }, g: Just gm}-> do
          newGraph <- H.liftEffect $ GM.readGraph gm
          let newSelected = newGraph ^. G.selectedL
              newSelectedExisting = (mapMaybe $ preview taskIdP) <$> newSelected
              pathForUrl = if (length <$> newSelected) == (length <$> newSelectedExisting)
                           then newSelectedExisting
                           else Nothing
          -- TODO: better State -> url mechanism
          when (graph ^. G.selectedL /= newSelected) $ do
            let url = Router.toUrl $ Router.TaskList graphId pathForUrl showCompleted
            H.liftEffect $ pushUrl url
            H.modify_ $ \s-> s
              { inputTaskPath = newSelectedExisting
              }
          updateCache gm
          pure unit
        _ -> pure unit

    TaskListEvent (TaskList.UpdatedIncludesCompleted newIncludesCompleted) -> do
      { graphId, inputTaskPath } <- H.get
      let url = Router.toUrl $ Router.TaskList graphId inputTaskPath newIncludesCompleted
      H.liftEffect $ pushUrl url

  newFilledGraphManager = do
    { graphId, showCompleted, inputTaskPath } <- H.get
    graph <- H.liftAff $ API.getTasks graphId showCompleted
    gm <- H.liftEffect GM.create
    let newGraph = updateGraphWithSelectedIdValidated (map Existing <$> inputTaskPath) (G.mapKey Existing graph)

    H.liftEffect $ GM.update' newGraph gm
    pure gm

  updateGraphManager = do
    { cleanUp } <- H.get
    H.liftEffect cleanUp
    gm <- newFilledGraphManager
    queueId <- H.liftEffect randomString
    let queue = GM.subscribe gm
        eventSource = HQ.effectEventSource (\emitter -> Q.on queue queueId (const $ HQ.emit emitter ReceivedGraphUpdate) $> mempty)
    unobserve <- H.liftEffect $ APIM.observeGM gm
    H.modify_ $ \g -> g
      { graphManager = Just gm
      , cleanUp = Q.del queue queueId *> unobserve
      }
    -- TODO: unsubscribe
    _ <- H.subscribe eventSource
    updateCache gm
    pure gm

  updateCache gm = do
    graph <- H.liftEffect $ GM.readGraph gm
    H.modify_ $ \s-> s { cache = Just { graph }}

  pushUrl url = do
    win <- window
    his <- history win
    titleStr <- document win >>= title
    loc <- location win
    h <- hash loc
    when (h /= url) $
      pushState (unsafeToForeign {}) (DocumentTitle titleStr) (URL url) his

  updateGraphWithSelectedIdValidated newSelectedPath graph =
    let forest = G.getForest graph
        validatedPath = case newSelectedPath of
                          Nothing -> Nothing
                          Just path | isValidForestPath path forest -> Just path
                          Just [singleTaskId] -> firstPathWithLeaf singleTaskId forest
                          _ -> Nothing
    in graph # G.selectedL .~ validatedPath
