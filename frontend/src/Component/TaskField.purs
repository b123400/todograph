module Component.TaskField where

import Prelude

import Data.Foldable (for_)
import Data.Maybe (Maybe(..))
import Data.Lens ((.~))
import Effect.Aff.Class (class MonadAff)
import Halogen (ClassName(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Model.Task (Task(..), TaskState(..))
import Model.Task as Task
import Web.Event.Event (Event, preventDefault)
import Web.HTML.HTMLElement (focus)
import Web.UIEvent.KeyboardEvent (key, shiftKey)
import Web.UIEvent.KeyboardEvent as KE

type State = { task :: Task
             }
type Input = Task
type Slots = ()

data Action
  = SetBody String
  | SetDone Boolean
  | UpdateTask Task
  | Submit
  | Backspace Event
  | Raise Message
  | PreventDefault Event Action

data Query a
  = Focus a -- Want to focus the input element

data Message = WantToSubmit Task
             | UpdatedTask Task
             | Tabbed
             | ShiftTabbed
             | UpPressed
             | DownPressed
             | BackspacePressed
             | Focused
             | Blured

component :: forall m. MonadAff m => H.Component HH.HTML Query Input Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval H.defaultEval
        { receive = Just <<< UpdateTask
        , handleAction = handleAction
        , handleQuery = handleQuery
        }
    }
  where

  initialState :: Task -> State
  initialState t = { task: t
                   }

  render :: State -> H.ComponentHTML Action Slots m
  render {task : (Task state)} =
    HH.div
      [ HP.class_ $ ClassName "task-field" ]
      [ HH.input
          [ HP.type_ HP.InputCheckbox
          , HP.class_ $ ClassName "task-field__checkbox"
          , HP.checked $ state.state == Done
          , HE.onChecked (Just <<< SetDone)
          ]
      , HH.input
          [ HP.value state.body
          , HP.placeholder "New task"
          , HP.ref (H.RefLabel "text-field")
          , HE.onValueInput (Just <<< SetBody)
          , HE.onFocus (const $ Just $ Raise Focused)
          , HE.onBlur (const $ Just $ Raise Blured)
          , HE.onKeyDown (\e->
              case key e of
                "Enter"            -> Just Submit
                "Tab" | shiftKey e -> Just $ PreventDefault (KE.toEvent e) (Raise ShiftTabbed)
                      | otherwise  -> Just $ PreventDefault (KE.toEvent e) (Raise Tabbed)
                "ArrowUp"          -> Just $ Raise UpPressed
                "ArrowDown"        -> Just $ Raise DownPressed
                "Backspace"        -> Just $ Backspace (KE.toEvent e)
                _ -> Nothing
            )
          ]
      ]
  handleAction :: Action -> H.HalogenM State Action Slots Message m Unit
  handleAction = case _ of
    SetBody newBody -> do
      Task state <- H.gets _.task
      let newTask = Task state { body = newBody }
      H.modify_ $ \st -> st {task = newTask}
      H.raise $ UpdatedTask newTask

    SetDone newDone -> do
      let taskState = if newDone then Done else Pending
      state <- H.get
      let newTask = state.task # Task.state .~ taskState
          newState = state { task = newTask }
      H.put state
      H.raise $ UpdatedTask newTask

    UpdateTask task-> do
      H.put { task }

    Submit-> do
      state <- H.get
      H.raise $ WantToSubmit state.task

    Backspace event -> do
      Task { body } <- H.gets _.task
      if body == ""
          then do
            H.liftEffect $ preventDefault event
            H.raise BackspacePressed
          else pure unit

    Raise message -> do
      H.raise message

    PreventDefault event action -> do
      H.liftEffect $ preventDefault event
      handleAction action

  handleQuery :: forall a. Query a -> H.HalogenM State Action Slots Message m (Maybe a)
  handleQuery (Focus next) = do
    mElement <- H.getHTMLElementRef $ H.RefLabel "text-field"
    H.liftEffect $ for_ mElement focus
    pure $ Just next
