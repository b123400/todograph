module Component.TaskList where

import Prelude

import API.Task as API
import Component.TaskField as TaskField
import Control.Alt ((<|>))
import Control.Comonad.Cofree (head, tail)
import Control.Monad.Maybe.Trans (MaybeT(..), lift, runMaybeT)
import Control.Monad.State (class MonadState)
import Data.Array (intercalate, length, null, singleton, snoc, unsafeIndex, (!!), (:))
import Data.Array as Array
import Data.Default (class Default)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Ord (genericCompare)
import Data.Graph as G
import Data.GraphManager as GM
import Data.Foldable (all, foldl)
import Data.Ins as I
import Data.Int (floor, toNumber)
import Data.Lens ((^.))
import Data.Lens as L
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.MediaType (MediaType(..))
import Data.Newtype (class Newtype, unwrap)
import Data.Set (Set)
import Data.Set as Set
import Data.String (Pattern(..), split)
import Data.String.Random (randomString)
import Data.Symbol (SProxy(..))
import Data.Traversable (for, for_, sequence)
import Data.Tree (DependsOnMigration(..), Forest, Path, Tree, closedForest, nextInForest, prevInForest)
import Effect (Effect)
import Effect.Aff (Milliseconds(..), delay)
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect)
import Halogen (ClassName(..))
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HQ
import IxQueue as Q
import Model.HybridTask (ComposingTaskId(..), HybridTask(..), newDefaultTempTask)
import Model.Relationship.Sorted (Sorted, fromSortString, previousDropTargets, toSortString, toSorted)
import Model.Task (Task(..), TaskState(..))
import Partial.Unsafe (unsafePartial)
import Web.Event.Event (currentTarget, preventDefault, stopPropagation)
import Web.HTML.Event.DataTransfer as DT
import Web.HTML.Event.DragEvent (DragEvent, dataTransfer)
import Web.HTML.Event.DragEvent as DE
import Web.HTML.Event.DragEvent.Cursor (clientX, clientY)
import Web.HTML.HTMLElement (fromEventTarget, getBoundingClientRect)

newtype State (g :: Type -> Type) = State
  { graphManager :: GM.GraphManager Task g ComposingTaskId
  , dropTarget :: Maybe DropTarget
  , closedSlots :: Set Slot
  , cache :: Maybe { graph :: g ComposingTaskId }
  , cleanUpGm :: Effect Unit
  -- The taskId that currently has focus (can input text)
  -- It's possible to have no focus but selected task
  , focusTaskId :: Maybe ComposingTaskId
  , includesCompleted :: Boolean
  }
type Input (g :: Type -> Type) =
  { graphManager :: GM.GraphManager Task g ComposingTaskId
  , includesCompleted :: Boolean
  }

type Slot = Array ComposingTaskId

type Slots =
  ( taskField :: H.Slot TaskField.Query TaskField.Message Slot
  )
_taskField :: SProxy "taskField"
_taskField = SProxy

data Action = Init
           | CleanUp
           | ReceivedGraphUpdate
           | HandleTaskField Slot TaskField.Message
           | DragStart Slot DragEvent
           | DragOver Slot DragEvent
           | DragEnd Slot DragEvent
           | Drop DragEvent
           | ToggleCloseState Slot
           | SetIncludesCompleted Boolean
data Query g a = NewInput (Input g) a
data Message = UpdatedIncludesCompleted Boolean
data DropTarget = TargetTop -- The root top node
                | TargetAfter Slot
                | TargetChild Slot -- The first child of a node

derive instance newtypeState :: Newtype (State g) _
derive instance genericDropTarget :: Generic DropTarget _
instance eqDropTarget :: Eq DropTarget where eq = genericEq
instance ordDropTarget :: Ord DropTarget where compare = genericCompare

component
  :: forall g m
  .  MonadAff m
  => G.IsGraph g
  => G.RawGraph g
  => G.RelGraph g
  => G.BodyGraph g Task
  => G.SelectAGraph g
  => G.ForestGraph g
  => G.IdGraph g String
  => G.MapKey g
  => Default (g String)
  => H.Component HH.HTML (Query g) (Input g) Message m
component =
  H.mkComponent
    { initialState: initialState
    , render
    , eval: H.mkEval H.defaultEval
        { receive = const Nothing
        , handleAction = handleAction
        , handleQuery = handleQuery
        , initialize = Just Init
        , finalize = Just CleanUp
        }
    -- Does not receive input update. NewInput should be called manually when there is new input
    -- because otherwise it'll be called every time the parent's state is set, that's too much.
    }
  where

  initialState :: Input g -> State g
  initialState { graphManager, includesCompleted }
    = State { graphManager
            , dropTarget: Nothing
            , closedSlots: Set.empty
            , cache: Nothing
            , cleanUpGm: pure unit
            , focusTaskId: Nothing
            , includesCompleted
            }
  render :: (State g) -> H.ComponentHTML Action Slots m
  render (State state@{ cache: Nothing }) = HH.div [] []
  render (State state@{ includesCompleted, cache: Just { graph } }) =
    HH.div [ HP.class_ $ ClassName "task-list"] ([toolbar] <> topDropTarget <> (nodeElement true [] <$> G.getForest graph))
    where toolbar = HH.div [ HP.class_ $ ClassName "task-list__toolbar" ]
                           [ HH.label [] [ HH.input [ HP.type_ HP.InputCheckbox
                                                    , HP.checked includesCompleted
                                                    , HE.onChecked (Just <<< SetIncludesCompleted)
                                                    ]
                                         , HH.text "Show completed"
                                         ]
                           ]
          topDropTarget = if state.dropTarget == Just TargetTop
                            then [HH.div [ HP.class_ $ ClassName "task-list__drop"] []]
                            else []
          nodeElement isRoot parentSlot node =
            let slot = parentSlot `snoc` head node
                closed = Set.member slot state.closedSlots
                fieldElement = fromMaybe (HH.div_ []) $ G.getBody (head node) graph <#> \task->
                                 HH.div [ HP.classes $ [ ClassName "task" ]
                                                    <> [ ClassName $ case (head node) of
                                                         (Composing _)-> "--composing"
                                                         (Existing _)-> "--existing"]
                                                    <> (if Just slot == graph ^. G.selectedL
                                                           then [ ClassName "--selected" ]
                                                           else if isTaskIdProxySelected slot
                                                           then [ ClassName "--proxy-selected" ]
                                                           else [])
                                                    <> (if (Array.null $ tail node)
                                                           then [ClassName "--empty"]
                                                           else if closed then [ClassName "--closed"]
                                                           else [ClassName "--opened"])
                                        ]
                                        [ HH.div [ HP.class_ (ClassName "task__drag-point")
                                                 , HE.onClick (const $ Just $ ToggleCloseState slot)
                                                 ] []
                                        , HH.slot _taskField
                                                  slot
                                                  TaskField.component
                                                  (task)
                                                  (Just <<< HandleTaskField slot)
                                        ]
                childrenE = if closed then [] else nodeElement false slot <$> tail node
                afterSeparator = if state.dropTarget == (Just $ TargetAfter slot)
                                     then Just $ HH.div [ HP.class_ $ ClassName "task-list__drop"] []
                                     else Nothing
                childSeparator = if state.dropTarget == (Just $ TargetChild slot)
                                     then Just $ HH.div [ HP.classes [ClassName "task-list__drop", ClassName "--child"]] []
                                     else Nothing
              in HH.div [ HP.classes ([ClassName "task-tree"] <> (if isRoot then [ClassName "--root"] else []))
                        , HP.draggable true
                        , HE.onDragStart (Just <<< DragStart slot)
                        , HE.onDragOver (Just <<< DragOver slot)
                        , HE.onDragEnd (Just <<< DragEnd slot)
                        , HE.onDrop (Just <<< Drop)
                        ] $ [fieldElement] <> (maybe [] singleton childSeparator) <> childrenE <> (maybe [] singleton afterSeparator)

          -- The same Task (but with different path) is selected
          isTaskIdProxySelected slot = (Array.last =<< graph ^. G.selectedL) == Array.last slot

  handleQuery :: forall a. Query g a -> H.HalogenM (State g) Action Slots Message m (Maybe a)
  handleQuery (NewInput input next) = do
      handleAction CleanUp
      H.put $ initialState input
      handleAction Init
      pure $ Just next

  handleAction :: Action -> H.HalogenM (State g) Action Slots Message m Unit
  handleAction = case _ of
    Init -> do
      State { graphManager } <- H.get
      mInitSlot <- insertInitialTempTask
      graph <- updateCache
      queueId <- H.liftEffect randomString
      let queue = GM.subscribe graphManager
          eventSource = HQ.effectEventSource (\emitter -> Q.on queue queueId (const $ HQ.emit emitter ReceivedGraphUpdate) $> mempty)
      H.modify_ $ \(State s)-> State s
        { closedSlots = initiallyClosedSlots graph
        , cleanUpGm = s.cleanUpGm *> (void $ Q.del queue queueId)
        }
      -- TODO: unsubscribe later?
      _ <- H.subscribe eventSource

      let focusSlot = graph ^. G.selectedL <|> mInitSlot
      for_ focusSlot $ \fSlot-> do
        -- Wait until the dom reload so the new slot exists
        H.liftAff $ delay $ Milliseconds 1.0
        H.query _taskField fSlot $ H.tell TaskField.Focus

    CleanUp -> do
      State { cleanUpGm } <- H.get
      H.liftEffect cleanUpGm
      H.modify_ $ \(State s)-> State s
        { cleanUpGm = pure unit
        }

    ReceivedGraphUpdate -> do
      (State { graphManager, cache, focusTaskId }) <- H.get
      newGraph <- H.liftEffect $ GM.readGraph graphManager
      let mPrevSelected = cache >>= \{ graph } -> graph ^. G.selectedL
          mCurrentSelected = newGraph ^. G.selectedL

      void $ insertInitialTempTask
      void updateCache

      when (focusTaskId == (Array.last =<< mPrevSelected) && mCurrentSelected /= mPrevSelected) do
        for_ mCurrentSelected $ \newSlot-> do
          -- Wait until the dom reload so the new slot exists
          H.liftAff $ delay $ Milliseconds 1.0
          H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot (TaskField.WantToSubmit newTask) -> do
      (State { graphManager }) <- H.get
      case Array.last slot of
        Nothing -> pure unit
        Just tid ->
          H.liftEffect $ GM.modify graphManager $ G.nudge tid
      mNewSlot <- insertTempTask slot
      for_ mNewSlot $ \newSlot-> do
        -- Wait until the dom reload so the new slot exists
        H.liftAff $ delay $ Milliseconds 1.0
        H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot (TaskField.UpdatedTask t) -> do
      (State state) <- H.get
      for_ (Array.last slot) $ \taskId -> do
        H.liftEffect $ GM.modify state.graphManager (G.setBody taskId t)

    HandleTaskField slot TaskField.Focused -> do
      (State state) <- H.get
      H.liftEffect $ GM.modify' state.graphManager (L.set G.selectedL (Just slot))
      for_ (Array.last slot) $ \taskId->
        H.modify_ $ \(State st)-> State st { focusTaskId = Just taskId }

    HandleTaskField slot TaskField.Blured -> do
      H.modify_ $ \(State st)-> State st { focusTaskId = Nothing }

    HandleTaskField slot TaskField.Tabbed -> do
      (State { graphManager }) <- H.get
      mNewSlot <- H.liftEffect $ GM.modifyV graphManager $ I.runInsM $ do
        newSlot <- I.insM $ G.shiftDown slot
        I.insM $ L.set G.selectedL newSlot
        pure newSlot
      for_ mNewSlot $ \newSlot-> do
        -- Wait until the dom reload so the new slot exists
        H.liftAff $ delay $ Milliseconds 1.0
        H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot TaskField.ShiftTabbed -> do
      (State { graphManager }) <- H.get
      mNewSlot <- H.liftEffect $ GM.modifyV graphManager $ I.runInsM $ do
        newSlot <- I.insM $ G.shiftUp slot
        I.insM $ L.set G.selectedL newSlot
        pure newSlot
      for_ mNewSlot $ \newSlot-> do
        -- Wait until the dom reload so the new slot exists
        H.liftAff $ delay $ Milliseconds 100.0
        H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot TaskField.UpPressed -> do
      -- Update task here?
      (State { graphManager, closedSlots }) <- H.get
      mSlot <- H.liftEffect $ GM.modifyV graphManager $ I.runInsM $ do
        graph <- I.read
        let forest = G.getForest graph
            closed = closedForest closedSlots forest
            mPrev = prevInForest closed slot
        I.insM $ L.set G.selectedL mPrev
        pure mPrev
      for_ mSlot $ \newSlot->
        H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot TaskField.DownPressed -> do
      (State { closedSlots, graphManager }) <- H.get
      mSlot <- H.liftEffect $ GM.modifyV graphManager $ I.runInsM $ do
        graph <- I.read
        let forest = G.getForest graph
            closed = closedForest closedSlots forest
            mNext = nextInForest closed slot
        I.insM $ L.set G.selectedL mNext
        pure mNext

      for_ mSlot $ \newSlot->
        H.query _taskField newSlot $ H.tell TaskField.Focus

    HandleTaskField slot TaskField.BackspacePressed -> do
      (State state@{ graphManager, closedSlots }) <- H.get
      mNewFocus <- runMaybeT $ do
        taskId <- MaybeT $ pure $ slot !! (length slot - 1)
        let mParentId = slot !! (length slot - 2)
        graph <- H.liftEffect $ GM.readGraph graphManager
        let parentIds = G.parents taskId graph
            forest = G.getForest graph
            closed = closedForest closedSlots forest
            prev = prevInForest closed slot

        deleteRelationshipsOnly <- case mParentId of
                                   Nothing -> pure false
                                   -- TODO: ask: do you want to delete the node or just relationship?
                                   Just _ -> pure true

        case mParentId of
          Just parentId ->
            H.liftEffect $ GM.modify graphManager $ G.deleteRelationship parentId taskId
          Nothing -> do
            -- TODO: ask: What to do with dependents?
            childrenMigration <- pure DeleteRelationships
            H.liftEffect $ GM.modify graphManager $ G.delete taskId childrenMigration
        MaybeT $ pure prev
      for_ mNewFocus $ \newFocus->
        H.query _taskField newFocus $ H.tell TaskField.Focus

    DragStart slot e -> do
      H.liftEffect $ stopPropagation $ DE.toEvent e
      let d = dataTransfer e
      H.liftEffect $ do
        DT.setData (MediaType "task") (slotToString slot) d
        DT.setData (MediaType "source") "task-list" d
      case Array.last slot of
        Nothing -> pure unit
        Just tid -> H.liftEffect $ DT.setData (MediaType "sortedId") (toSortString tid) d
      H.liftEffect $ DT.setDropEffect DT.Move d

    DragOver slot e -> do
      H.liftEffect $ stopPropagation $ DE.toEvent e
      H.liftEffect $ preventDefault $ DE.toEvent e
      let x = clientX e
          y = clientY e
          mTarget = fromEventTarget =<< (currentTarget $ DE.toEvent e)
      H.liftEffect $ DT.setDropEffect DT.Move (dataTransfer e)
      mRect <- H.liftEffect $ maybe (pure Nothing) (map Just) $ getBoundingClientRect <$> mTarget
      State state <- H.get
      graph <- H.liftEffect $ GM.readGraph state.graphManager
      let rowHeight = 33.0 -- TODO: THIS SIZE IS HARDCODED
          leftChildMargin = 25.0
      for_ mRect $ \rect-> do
        let sortedForest = toSorted $ G.getForest graph
            dropTarget = if (toNumber y) < rect.top + rowHeight / 2.0
                         then let prevDropTargets = fromMaybe [TargetTop] $ getPreviousTargets sortedForest slot
                                  unboundedIndex = floor $ ((toNumber x) - rect.left) / leftChildMargin
                                  index = min (length prevDropTargets - 1) (max 0 unboundedIndex)
                              in  unsafePartial $ unsafeIndex prevDropTargets index
                         else if (toNumber x) < rect.left + leftChildMargin then TargetAfter slot
                         else TargetChild slot
        if state.dropTarget /= Just dropTarget
          then do
            H.put $ State state { dropTarget = Just dropTarget }
            H.liftAff $ delay (Milliseconds 1000.0)
            (State laterState) <- H.get
            let mLaterDropTarget = laterState.dropTarget
            case mLaterDropTarget of
              Just (TargetChild laterSlot) | mLaterDropTarget == Just dropTarget && Set.member laterSlot laterState.closedSlots ->
                H.modify_ (\(State st)-> State st { closedSlots = Set.delete laterSlot st.closedSlots })
              _ -> pure unit
          else pure unit

    DragEnd slot e -> do
      H.liftEffect $ stopPropagation $ DE.toEvent e
      dropEffect <- H.liftEffect $ DT.dropEffect $ dataTransfer e
      H.modify_ (\(State st) -> State st { dropTarget = Nothing })

    Drop e -> do
      H.liftEffect $ stopPropagation $ DE.toEvent e
      void $ runMaybeT $ do
        (State state@{ graphManager }) <- lift H.get
        fromSlot <- MaybeT $ H.liftEffect $ stringToSlot <$> DT.getData (MediaType "task") (dataTransfer e)
        dropTarget <- MaybeT $ pure $ case state.dropTarget of
          Just (TargetChild c) | c == fromSlot -> Just $ TargetAfter c
          a -> a
        lastPath <- MaybeT $ pure $ Array.last fromSlot
        newSlot <- MaybeT $ pure $ case dropTarget of
                        TargetTop -> Just [lastPath]
                        (TargetAfter s) -> do {init: init} <- Array.unsnoc s
                                              Just $ init `snoc` lastPath
                        (TargetChild s) -> Just $ s `snoc` lastPath

        let ordering = case dropTarget of
                         TargetAfter s -> Array.last s
                         TargetTop -> Nothing
                         TargetChild _ -> Nothing
        H.liftEffect $ GM.modify graphManager $ G.move fromSlot newSlot ordering

    ToggleCloseState slot -> do
      (State st) <- H.get
      let newClosedSlots = if (Set.member slot st.closedSlots)
                     then Set.delete slot st.closedSlots
                     else Set.insert slot st.closedSlots
      H.put (State st { closedSlots = newClosedSlots })

    SetIncludesCompleted includesCompleted -> do
      (State st) <- H.get
      H.put $ State st { includesCompleted = includesCompleted }
      graph <- H.liftEffect $ GM.readGraph st.graphManager
      newGraph <- H.liftAff $ API.getTasks (graph ^. G.idL) includesCompleted
      H.liftEffect $ GM.update' (G.mapKey Existing newGraph) st.graphManager
      H.raise $ UpdatedIncludesCompleted includesCompleted

-- insert a temp task under a specific task / at root
insertTempTask
  :: forall g m
  .  MonadEffect m
  => G.RawGraph g
  => G.BodyGraph g Task
  => G.ForestGraph g
  => Path ComposingTaskId
  -> H.HalogenM (State g) Action Slots Message m (Maybe Slot)
insertTempTask path = do
  { graphManager, closedSlots } <- H.gets unwrap
  newTempTask@(HybridTask newTempId task) <- H.liftEffect newDefaultTempTask
  graph <- H.liftEffect $ GM.readGraph graphManager
  for (Array.unsnoc path) $ \({ init, last })-> do
    let hasChild = not $ Array.null $ G.children last graph
        isClosed = Set.member path closedSlots
        createAsFirstChild = hasChild && not isClosed
        newPath = if createAsFirstChild
                  then Array.snoc path newTempId
                  else Array.snoc init newTempId
    void $ H.liftEffect $ GM.modifyV graphManager $ I.runInsM $ do
      I.insM $ G.addBody newTempId task
      I.insM $ G.move [newTempId] newPath (if createAsFirstChild then Nothing else Just last)
    pure newPath

insertInitialTempTask
  :: forall g m
  .  MonadEffect m
  => G.IsGraph g
  => G.BodyGraph g Task
  => H.HalogenM (State g) Action Slots Message m (Maybe Slot)
insertInitialTempTask = do
  { graphManager } <- H.gets unwrap
  newTempTask@(HybridTask newTempId task) <- H.liftEffect newDefaultTempTask
  graph <- H.liftEffect $ GM.readGraph graphManager
  let G.Graph { nodes } = graph ^. G.graphL
  case Array.last nodes of
    Just (Composing _) -> pure Nothing
    _ -> do
      void $ H.liftEffect $ GM.modify' graphManager $ G.addBody newTempId task
      pure $ Just [newTempId]

slotToString :: Slot -> String
slotToString slot = intercalate " " $ toSortString <$> slot

stringToSlot :: String -> Maybe Slot
stringToSlot string = sequence $ fromSortString <$> split (Pattern " ") string

-- 1
-- |_ 1.1
-- | |_ 1.1.1
-- |_2
-- getPreviousTargets = [after 1, after 1.1, after 1.1.1, child 1.1.1]
getPreviousTargets :: Sorted ComposingTaskId -> Path ComposingTaskId -> Maybe (Array DropTarget)
getPreviousTargets = previousDropTargets TargetTop TargetAfter TargetChild

slotsDeeperOrEqualTo :: forall a. Ord a => Int -> Forest a -> Set (Path a)
slotsDeeperOrEqualTo n forest = Set.fromFoldable $ slotsDeeperOrEqualTo' n =<< forest
  where slotsDeeperOrEqualTo' :: Int -> Tree a -> Array (Path a)
        slotsDeeperOrEqualTo' 0 node = Array.cons [head node] $ (Array.cons $ head node) <$> (slotsDeeperOrEqualTo' 0 =<< tail node)
        slotsDeeperOrEqualTo' x node = (Array.cons (head node)) <$> (slotsDeeperOrEqualTo' (x - 1) =<< (tail node))


withCache :: forall c m b a. Applicative m => Maybe c -> a -> (c -> m b) -> m a
withCache Nothing next _ = pure next
withCache (Just cache) next op = op cache $> next

updateCache :: forall g m. Bind m => MonadState (State g) m => MonadEffect m => m (g ComposingTaskId)
updateCache = do
  State s@{ graphManager } <- H.get
  graph <- H.liftEffect $ GM.readGraph graphManager
  H.put $ State s { cache = Just { graph }}
  pure graph

initiallyClosedSlots
  :: forall g a
  .  Ord a
  => G.IsGraph g
  => G.RawGraph g
  => G.ForestGraph g
  => G.BodyGraph g Task
  => g a
  -> Set (Path a)
initiallyClosedSlots graph = initiallyClosedSlots' $ G.getForest graph
  where initiallyClosedSlots' forest =
          foldl Set.union Set.empty $ forest <#> \f->
            let h = head f
            in if Set.member (head f) closedId
               then Set.singleton [h]
               else Set.map ((:) h) $ initiallyClosedSlots' (tail f)
        closedId = Set.intersection hasDependenciesId dependenciesFinishedId
        hasDependenciesId =
          let G.Graph { nodes } = graph ^. G.graphL
          in Set.fromFoldable $ flip Array.filter nodes $ \key-> not $ null $ G.children key graph
        dependenciesFinishedId =
          let G.Graph { nodes } = graph ^. G.graphL
              selfFinished = Set.fromFoldable $ flip Array.filter nodes $ \key->
                               case G.getBody key graph of
                                 Just (Task { state }) | state == Done -> true
                                 _ -> false
              childrenFinished = Set.fromFoldable $ flip Array.filter nodes $ \key->
                                   all (flip Set.member selfFinished) $ G.children key graph
          in Set.union selfFinished childrenFinished
