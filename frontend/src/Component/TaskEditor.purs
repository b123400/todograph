module Component.TaskEditor where

import Prelude

import Data.Array (last)
import Data.DateTime.DateTimeLocal (toDateTimeLocalString)
import Data.Foldable (for_)
import Data.Graph as G
import Data.GraphManager as GM
import Data.Lens ((.~), (^.))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.MediaType (MediaType(..))
import Data.String.Random (randomString)
import Data.Tuple (Tuple(..))
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Halogen.Query.EventSource as HQ
import IxQueue as Q
import Model.Relationship.Sorted (class FromSortString, fromSortString)
import Model.Task (Task, autocomplete, body, comment, dueDate)
import Model.Task.Class (class IsTaskEntity, taskL)
import Web.Event.Event (preventDefault, stopPropagation)
import Web.HTML.Event.DataTransfer as DT
import Web.HTML.Event.DragEvent (DragEvent, dataTransfer)
import Web.HTML.Event.DragEvent as DE


type State (g :: Type -> Type) k =
  { graphManager :: GM.GraphManager Task g k
  , queueId :: Maybe String
  , cache :: Maybe (Cache g k)
  }
type Cache (g :: Type -> Type) k =
  { task :: Task
  , graph :: g k
  , taskId :: k
  }
type Input (g :: Type -> Type) k = GM.GraphManager Task g k

data Action
  = Initialise
  | CleanUp
  | ReceivedGraphUpdate
  | SetBody String
  | SetComment String
  | SetAutoComplete Boolean
  | DragOver RelationshipType DragEvent
  | Drop RelationshipType DragEvent

data Query g k a = UpdateGraphManager (GM.GraphManager Task g k) a
type Message = Void
type Slots = ()

data RelationshipType = DependsOn | DependedBy

component
  :: forall m g k
  .  MonadAff m
  => Ord k
  => FromSortString k
  => G.BodyGraph g Task
  => G.RawGraph g
  => G.RelGraph g
  => G.SelectAGraph g
  => H.Component HH.HTML (Query g k) (Input g k) Message m
component =
  H.mkComponent
    { initialState
    , render
    , eval: H.mkEval H.defaultEval
        { handleAction = handleAction
        , handleQuery = handleQuery
        , initialize = Just Initialise
        , finalize = Just CleanUp
        }
    }
  where

  initialState gm = { graphManager: gm, cache: Nothing, queueId: Nothing }

  render :: (State g k) -> H.ComponentHTML Action Slots m
  render {cache: (Just state)} =
    let t = state.task
        tBody = t ^. body
        tDueDate = t ^. dueDate
        tDueDateString = fromMaybe "" $ toDateTimeLocalString <$> tDueDate
        tComment = t ^. comment
        tAutocomplete = t ^. autocomplete
    in
    HH.div
      [ HP.class_ (H.ClassName "task-editor")]
      [ HH.div
        [ HP.class_ (H.ClassName "task-editor__body") ]
        [ HH.input [ HP.class_ (H.ClassName "task-editor__body-input")
                   , HP.value tBody
                   , HP.placeholder "New task"
                   , HE.onValueInput (Just <<< SetBody)
                   ]
        ]
      , HH.div
        [ HP.class_ (H.ClassName "task-editor__form") ]
        [ HH.table_
          [ HH.tr_
            [ HH.td_ [ HH.text "Due date" ]
            , HH.td_ [ HH.input [ HP.class_ (H.ClassName "task-editor__form-input")
                       , HP.value tDueDateString
                       , HP.type_ HP.InputDatetimeLocal
                       ]
                     ]
            ]
          , HH.tr_
            [ HH.td_ [ HH.text "Comment" ]
            , HH.td_
              [ HH.textarea [ HP.class_ (H.ClassName "task-editor__form-textarea")
                            , HP.value tComment
                            , HE.onValueInput (Just <<< SetComment)
                            ]
              ]
            ]
          , HH.tr_
            [ HH.td_ [ HH.text "Auto-complete"]
            , HH.td_
              [ HH.input [ HP.class_ (H.ClassName "task-editor__autocomplete")
                         , HP.type_ HP.InputCheckbox
                         , HP.checked tAutocomplete
                         , HE.onChecked (Just <<< SetAutoComplete)
                         ]
              ]
            ]
          , HH.tr_
            [ HH.td_ [ HH.text "Depends on:"]
            , HH.td
              [ HE.onDragOver (Just <<< DragOver DependsOn)
              , HE.onDrop (Just <<< Drop DependsOn)
              ]
              [ refTasks $ selectedDependsOn state.taskId state ]
            ]
          , HH.tr_
            [ HH.td_ [ HH.text "Depended by:" ]
            , HH.td
              [ HE.onDragOver (Just <<< DragOver DependedBy)
              , HE.onDrop (Just <<< Drop DependedBy)
              ]
              [ refTasks $ selectedDependedBy state.taskId state ]
            ]
          ]
        ]
      ]
  render _ = HH.div [] []

  refTasks :: forall t. IsTaskEntity t k => Array t -> H.ComponentHTML Action Slots m
  refTasks tasks =
    HH.ul_ (tasks <#> \t-> HH.li_ [ HH.text $ t ^. taskL <<< body ])

  handleQuery :: forall a. Query g k a -> H.HalogenM (State g k) Action Slots Message m (Maybe a)
  handleQuery (UpdateGraphManager gm next) = do
    n1 <- handleAction CleanUp
    H.put $ initialState gm
    handleAction Initialise
    pure $ Just next

  handleAction :: Action -> H.HalogenM (State g k) Action Slots Message m Unit
  handleAction Initialise = do
    { graphManager } <- H.get
    updateCache
    queueId <- H.liftEffect randomString
    H.modify_ $ \s-> s { queueId = Just queueId }
    let queue = GM.subscribe graphManager
        eventSource = HQ.effectEventSource (\emitter-> Q.on queue queueId (\_-> HQ.emit emitter ReceivedGraphUpdate) $> mempty)
    _subscriberId <- H.subscribe eventSource
    -- TODO: unsubscribe event source?
    pure unit

  handleAction CleanUp = do
    { graphManager, queueId } <- H.get
    let queue = GM.subscribe graphManager
    H.liftEffect $ for_ queueId $ Q.del queue

  handleAction ReceivedGraphUpdate = do
    updateCache

  handleAction (SetBody newBody) = do
    { graphManager, cache } <- H.get
    for_ cache $ \{taskId}->
      H.liftEffect $ GM.modify graphManager (G.modifyBody taskId $ body .~ newBody)

  handleAction (SetComment newComment) = do
    { graphManager, cache } <- H.get
    for_ cache $ \{taskId}->
      H.liftEffect $ GM.modify graphManager (G.modifyBody taskId $ comment .~ newComment)

  handleAction (SetAutoComplete bool) = do
    { graphManager, cache } <- H.get
    for_ cache $ \{taskId}->
      H.liftEffect $ GM.modify graphManager (G.modifyBody taskId $ autocomplete .~ bool)

  handleAction (DragOver relationshipType e) = do
    H.liftEffect $ do
      stopPropagation $ DE.toEvent e
      preventDefault $ DE.toEvent e
      let dt = dataTransfer e
      source <- DT.getData (MediaType "source") dt
      if source == "task-list"
         then DT.setDropEffect DT.Link dt
         else pure unit

  handleAction (Drop relationshipType event) = withCache $ \state-> do
    { graphManager, cache } <- H.get
    let d = dataTransfer event
    mAnotherTaskId <- H.liftEffect $ fromSortString <$> DT.getData (MediaType "sortedId") d
    for_ (Tuple <$> mAnotherTaskId <*> cache) $ \(Tuple anotherTaskId { taskId })->
      H.liftEffect $ GM.modify graphManager $ case relationshipType of
        DependsOn -> G.addRelationship taskId anotherTaskId
        DependedBy -> G.addRelationship anotherTaskId taskId

updateCache
  :: forall g k q s o m
  .  MonadEffect m
  => G.BodyGraph g Task
  => G.SelectAGraph g
  => Ord k
  => H.HalogenM (State g k) q s o m Unit
updateCache = do
  { graphManager } <- H.get
  graph <- H.liftEffect $ GM.readGraph graphManager
  let mTaskId = last =<< graph ^. G.selectedL
      mTask = flip G.getBody graph =<< mTaskId
  case {i: mTaskId, t: mTask} of
    {i: Just taskId, t: Just task} -> H.modify_ $ \s-> s { cache = Just { graph, task, taskId }}
    _ -> pure unit


withCache :: forall c r q s o m b. (c -> H.HalogenM {cache :: Maybe c |r} q s o m b) -> H.HalogenM {cache :: Maybe c |r} q s o m Unit
withCache op = do
  s <- H.get
  case s.cache of
    Just c -> void $ op c
    _ -> pure unit

selectedDependsOn :: forall g k. Ord k => G.BodyGraph g Task => G.RawGraph g => k -> Cache g k -> Array (Tuple k Task)
selectedDependsOn taskId { graph } = G.childrenBody taskId graph

selectedDependedBy :: forall g k. Ord k => G.BodyGraph g Task => G.RawGraph g => k -> Cache g k -> Array (Tuple k Task)
selectedDependedBy taskId { graph } = G.parentsBody taskId graph
