module Model.Task where

import Prelude

import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, fromString, jsonEmptyObject, toString, (.:), (.:?), (:=), (~>))
import Data.DateTime (DateTime)
import Data.DateTime.ISO (ISO, unwrapISO)
import Data.Default (class Default)
import Data.Either (note)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Show (genericShow)
import Data.Lens (Lens')
import Data.Lens.Iso.Newtype (_Newtype)
import Data.Lens.Record (prop)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, wrap)
import Data.String.Read (class Read, read)
import Data.Symbol (SProxy(..))
import Model.Entity (Entity)

type TaskId = String
type TaskEntity = Entity TaskId Task

newtype Task = Task { body :: String
                    , state :: TaskState
                    , dueDate :: Maybe DateTime
                    , comment :: String
                    , autocomplete :: Boolean
                    }

data TaskState = Pending
               | Done

instance showTaskState :: Show TaskState where
  show Pending = "pending"
  show Done    = "done"

instance readTaskState :: Read TaskState where
  read "pending" = Just Pending
  read "done"    = Just Done
  read _         = Nothing

derive instance newtypeTask :: Newtype Task _

instance encodeJsonTask :: EncodeJson Task where
  encodeJson (Task t) = "body" := t.body
                     ~> "state" := t.state
                     ~> "dueDate" := ((wrap <$> t.dueDate) :: Maybe ISO)
                     ~> "comment" := t.comment
                     ~> "autocomplete" := t.autocomplete
                     ~> jsonEmptyObject

instance decodeJsonTask :: DecodeJson Task where
  decodeJson json = do
    obj <- decodeJson json
    body' <- obj .: "body"
    state' <- obj .: "state"
    dueDate' <- obj .:? "dueDate"
    comment' <- obj .: "comment"
    autocomplete' <- obj .: "autocomplete"
    pure $ Task { body: body'
                , state: state'
                , dueDate: (unwrapISO <$> dueDate')
                , comment: comment'
                , autocomplete: autocomplete'
                }

instance defaultTask :: Default Task where
  default = Task { body: "", state: Pending, dueDate: Nothing, comment: "", autocomplete: false }

instance encodeJsonTaskState :: EncodeJson TaskState where
  encodeJson = fromString <<< show

instance decodeJsonTaskState :: DecodeJson TaskState where
  decodeJson json = note "invalidate task state" $ read =<< toString json


derive instance genericTask :: Generic Task _
instance eqTask :: Eq Task where eq = genericEq
instance showTask :: Show Task where show = genericShow
derive instance genericTaskState :: Generic TaskState _
instance eqTaskState :: Eq TaskState where eq = genericEq

body :: Lens' Task String
body = _Newtype <<< prop (SProxy :: SProxy "body")

state :: Lens' Task TaskState
state = _Newtype <<< prop (SProxy :: SProxy "state")

dueDate :: Lens' Task (Maybe DateTime)
dueDate = _Newtype <<< prop (SProxy :: SProxy "dueDate")

comment :: Lens' Task String
comment = _Newtype <<< prop (SProxy :: SProxy "comment")

autocomplete :: Lens' Task Boolean
autocomplete = _Newtype <<< prop (SProxy :: SProxy "autocomplete")

