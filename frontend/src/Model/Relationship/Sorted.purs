module Model.Relationship.Sorted where

import Prelude

import Control.Comonad.Cofree (head, tail)
import Data.Array (elemIndex, index, length, take, (!!), (..))
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Ord (genericCompare)
import Data.Maybe (Maybe(..))
import Data.String (joinWith, split)
import Data.String.Pattern (Pattern(..))
import Data.Traversable (sequence)
import Data.Tree (Forest, Path)


class ToSortString a where
  toSortString :: a -> String

instance toSortStringString :: ToSortString String where
  toSortString = identity

class FromSortString a where
  fromSortString :: String -> Maybe a

instance fromSortStringString :: FromSortString String where
  fromSortString = Just

newtype SortKey a = SortKey String
newtype Sorted a = Sorted (Array (SortKey a))

derive instance genericSortKey :: Generic (SortKey a) _
instance eqSortKey :: Eq a => Eq (SortKey a) where eq = genericEq
instance ordSortKey :: Ord a => Ord (SortKey a) where compare = genericCompare

toSorted :: forall a. ToSortString a => Forest a -> Sorted a
toSorted forest = Sorted $ join $ treeToSorted <$> forest
  where treeToSorted tree = let headString = toSortString $ head tree
                                (Sorted children) = toSorted $ tail tree
                            in  [SortKey headString] <> ((\(SortKey c)-> SortKey $ headString <> ">" <> c) <$> children)

toSortKey :: forall a. ToSortString a => Path a -> SortKey a
toSortKey path = SortKey $ joinWith ">" $ toSortString <$> path

fromSortKey :: forall a. FromSortString a => SortKey a -> Maybe (Path a)
fromSortKey (SortKey k) = sequence $ fromSortString <$> (split (Pattern ">") k)

indexInForest :: forall a. Eq a=> ToSortString a=> Sorted a -> Path a -> Maybe Int
indexInForest (Sorted keyArr) path = elemIndex (toSortKey path) keyArr

nextInForest :: forall a. Eq a=> ToSortString a=> FromSortString a=> Sorted a -> Path a -> Maybe (Path a)
nextInForest sorted@(Sorted keyArr) path = fromSortKey =<< index keyArr =<< ((+) 1) <$> indexInForest sorted path

prevInForest :: forall a. Eq a=> ToSortString a=> FromSortString a=> Sorted a -> Path a -> Maybe (Path a)
prevInForest sorted@(Sorted keyArr) path = fromSortKey =<< index keyArr =<< (_ - 1) <$> indexInForest sorted path

previousDropTargets
  :: forall a d
  .  Eq a
  => ToSortString a
  => FromSortString a
  => d             -- TargetTop
  -> (Path a -> d) -- TargetAfter
  -> (Path a -> d) -- TargetChild
  -> Sorted a
  -> Path a
  -> Maybe (Array d)
previousDropTargets targetTop targetAfter targetChild sorted@(Sorted keyArr) path =
  case indexInForest sorted path of
    Nothing -> Nothing
    Just 0 -> Just [targetTop]
    Just index -> do
      prev <- fromSortKey =<< keyArr !! index
      let p = parents prev
      Just $ (targetAfter <$> p) <> [targetChild prev]
  where -- parent [1,2,3,4] == [[1], [1,2], [1,2,3]]
        parents :: Path a -> Array (Path a)
        parents [] = []
        parents [_] = []
        parents paths = (\count-> take count paths) <$> (1 .. (length paths -1))
