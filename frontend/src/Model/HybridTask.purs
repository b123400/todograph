module Model.HybridTask where

import Prelude

import API.Task (DependDiff)
import Data.Array (mapMaybe)
import Data.Default (default)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Ord (genericCompare)
import Data.Generic.Rep.Show (genericShow)
import Data.Lens (Lens', lens, prism')
import Data.Map (Map)
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)
import Data.String.CodeUnits (uncons)
import Data.String.Random (randomString)
import Effect (Effect)
import Model.Entity (Entity(..))
import Model.Relationship (Relationship, RelationshipDiff)
import Model.Relationship.Sorted (class FromSortString, class ToSortString, toSortString)
import Model.Task (Task, TaskEntity, TaskId)
import Model.Task.Class (class IsTaskEntity, class LikeTaskId)


-- A type for task editing
-- A common case of task is a view mixing saved tasks with temp tasks,
-- Just by using Task is not enough because there is no id.
-- This is a convenient type for handling both.
newtype TempId = TempId String
data ComposingTaskId = Composing TempId
                     | Existing TaskId
data HybridTask = HybridTask ComposingTaskId Task

derive instance newtypeTempId :: Newtype TempId _
derive instance genericTempId :: Generic TempId _
instance eqTempId :: Eq TempId where eq = genericEq
instance ordTempId :: Ord TempId where compare = genericCompare
instance showTempId :: Show TempId where show = genericShow
derive instance genericComposingTaskId :: Generic ComposingTaskId _
instance eqComposingTaskId :: Eq ComposingTaskId where eq = genericEq
instance ordComposingTaskId :: Ord ComposingTaskId where compare = genericCompare
instance showComposingTaskId :: Show ComposingTaskId where show = genericShow
derive instance genericHybridTask :: Generic HybridTask _
instance eqHybridTask :: Eq HybridTask where eq = genericEq

instance toSortStringTempId :: ToSortString TempId where
  toSortString (TempId id) = toSortString id

instance toSortStringComposingTaskId :: ToSortString ComposingTaskId where
  toSortString (Composing id) = "c" <> toSortString id
  toSortString (Existing id) = "e" <> toSortString id

instance fromSortStringComposingTaskId :: FromSortString ComposingTaskId where
  fromSortString str =
    case uncons str of
      Just {head: 'e', tail}-> Just $ Existing tail
      Just {head: 'c', tail}-> Just $ Composing $ TempId tail
      _ -> Nothing

instance hybridIsTask :: IsTaskEntity HybridTask ComposingTaskId where
  taskL = lens (\(HybridTask _ t)-> t) (\(HybridTask i _) t-> HybridTask i t)
  taskIdL = lens (\(HybridTask i _)-> i) (\(HybridTask _ t) i-> HybridTask i t)

instance composingFromTaskId :: LikeTaskId ComposingTaskId where
  taskIdP = prism' Existing (case _ of Existing a -> Just a
                                       _ -> Nothing)

newTempId :: Effect ComposingTaskId
newTempId = Composing <$> TempId <$> randomString

newTempTask :: Task -> Effect HybridTask
newTempTask t = HybridTask <$> newTempId <*> (pure t)

newDefaultTempTask :: Effect HybridTask
newDefaultTempTask = newTempTask default

fromTaskEntity :: TaskEntity -> HybridTask
fromTaskEntity (Entity tid t) = HybridTask (Existing tid) t

toTaskEntity :: HybridTask -> Maybe TaskEntity
toTaskEntity (HybridTask (Existing tid) t) = Just $ Entity tid t
toTaskEntity (HybridTask (Composing _) _) = Nothing

task :: Lens' HybridTask Task
task = lens (\(HybridTask _ t)-> t) (\(HybridTask id _)-> HybridTask id)


type HybridMap = Map ComposingTaskId HybridTask
type HybridRelationship = Relationship ComposingTaskId ComposingTaskId
type HRelDiff = RelationshipDiff ComposingTaskId


toDependDiff :: HRelDiff -> DependDiff
toDependDiff
  { createDependsOn
  , createDependedBy
  , deleteDependsOn
  , deleteDependedBy } =

  { createDependsOn:  f createDependsOn
  , createDependedBy: f createDependedBy
  , deleteDependsOn:  f deleteDependsOn
  , deleteDependedBy: f deleteDependedBy
  }
  where existOnly :: ComposingTaskId -> Maybe String
        existOnly (Composing _) = Nothing
        existOnly (Existing i) = Just i
        f = mapMaybe existOnly

