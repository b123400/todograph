module Model.Entity where

import Prelude
import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, encodeJson, (.:), (:=), (~>))

data Entity k v = Entity k v

instance encodeJsonEntity :: (EncodeJson k, EncodeJson v)=> EncodeJson (Entity k v) where
  encodeJson (Entity key value) = "id" := key
                                  ~> encodeJson value

instance decodeJsonEntity :: (DecodeJson k, DecodeJson v)=> DecodeJson (Entity k v) where
  decodeJson json = do
    obj <- decodeJson json
    key <- obj .: "id"
    value <- decodeJson json
    pure $ Entity key value
