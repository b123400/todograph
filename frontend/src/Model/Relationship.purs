module Model.Relationship where

import Prelude

import Data.Argonaut (class DecodeJson, decodeJson, (.:))
import Data.Bifunctor (class Bifunctor)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Ord (genericCompare)
import Data.Generic.Rep.Show (genericShow)
import Data.Newtype (class Newtype)
import Model.Task (TaskId)

data Relationship a b = Relationship a b

instance bifunctorRelationship :: Bifunctor Relationship where
  bimap left right (Relationship l r) = Relationship (left l) (right r)

derive instance genericRelationship :: Generic (Relationship a b) _
instance eqRelationship :: (Eq a, Eq b)=> Eq (Relationship a b) where eq = genericEq
instance ordRelationship :: (Ord a, Ord b) => Ord (Relationship a b) where compare = genericCompare
instance showRelationship :: (Show a, Show b) => Show (Relationship a b) where show = genericShow

newtype Depend = Depend (Relationship TaskId TaskId)
derive instance newtypeDepend :: Newtype Depend _

derive instance genericDepend :: Generic Depend _
instance eqDepend :: Eq Depend where eq = genericEq
instance ordDepend :: Ord Depend where compare = genericCompare

instance decodeJsonRelationship :: DecodeJson Depend where
  decodeJson json = do
    obj <- decodeJson json
    taskId <- obj .: "taskId"
    dependsOnTaskId <- obj .: "dependsOnTaskId"
    pure $ Depend $ Relationship taskId dependsOnTaskId

type RelationshipDiff a = { createDependsOn :: Array a
                          , deleteDependsOn :: Array a
                          , createDependedBy :: Array a
                          , deleteDependedBy :: Array a
                          }

emptyDiff :: forall a. RelationshipDiff a
emptyDiff =
  { createDependsOn: []
  , deleteDependsOn: []
  , createDependedBy: []
  , deleteDependedBy: []
  }
