module Model.Task.Class where

import Prelude

import Data.Lens (Lens', Prism', _1, _2, lens, prism')
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple)
import Model.Entity (Entity(..))
import Model.Task (Task, TaskId)


class IsTaskEntity t k | t -> k where
  taskL :: Lens' t Task
  taskIdL :: Lens' t k

instance taskIsEntity :: IsTaskEntity (Entity String Task){-TaskEntity-} String where
  taskL = lens (\(Entity _ t)-> t) (\(Entity i _) t-> Entity i t)
  taskIdL = lens (\(Entity id _)-> id) (\(Entity _ t) i-> Entity i t)

instance tupleIsEntity :: IsTaskEntity (Tuple k Task) k where
  taskL = _2
  taskIdL = _1

class LikeTaskId t where
  taskIdP :: Prism' t TaskId

instance taskIdFromString :: LikeTaskId String where
  taskIdP = prism' identity Just
