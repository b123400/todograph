module Model.Graph where

import Prelude

import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, jsonEmptyObject, (.:), (:=), (~>))
import Model.Entity (Entity)

type GraphId = String
data Graph = Graph { name :: String
                   }
type GraphEntity = Entity GraphId Graph

instance encodeJsonGraph :: EncodeJson Graph where
  encodeJson (Graph g) = "name" := g.name
                      ~> jsonEmptyObject

instance decodeJsonGraph :: DecodeJson Graph where
  decodeJson json = do
    obj <- decodeJson json
    name <- obj .: "name"
    pure $ Graph { name }
