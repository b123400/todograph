module Main where

import Prelude

import Component.Main as CM
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class (liftEffect)
import Effect.Console (log)
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Router.Signal as R
import Router (Route(..))
import Web.DOM.ParentNode (QuerySelector(..))

main :: Effect Unit
main = launchAff_ $ do
  _ <- HA.awaitBody
  melement <- HA.selectElement $ QuerySelector "#application"
  case melement of
    Just element -> do
      driver <- runUI CM.component GraphList element
      _removeRoutingListener <- R.routeSignal driver
      pure unit
    Nothing -> liftEffect $ log "Cannot find #application div"
