module API.Manager where

import Prelude

import API.Ins (class ToReq, Req(..), insToReqs, isEmpty)
import API.Task (DependDiff)
import API.Task as API
import Data.Array (mapMaybe)
import Data.Array as Array
import Data.Graph as G
import Data.GraphManager as GM
import Data.Ins (InsG, insM, runInsM_)
import Data.Ins as I
import Data.Lens (preview, review, (^.))
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.String.Random (randomString)
import Data.Time.Duration (Milliseconds(..))
import Data.Traversable (for_, traverse_)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Aff (Aff, delay, launchAff_)
import Effect.Class (liftEffect)
import Effect.Ref as Ref
import IxQueue as Q
import Model.Entity (Entity(..))
import Model.Graph (GraphId)
import Model.Task (Task, TaskId)
import Model.Task.Class (class LikeTaskId, taskIdP)

execReq
  :: forall g k
  .  Ord k
  => LikeTaskId k
  => G.IsGraph g
  => G.RawGraph g
  => G.BodyGraph g Task
  => G.RelGraph g
  => G.IdGraph g GraphId
  => g k
  -> Req k
  -> Aff (g k -> InsG Task g k)
execReq prevGraph (Update { id, dependDiff, body }) = do
  Tuple (Entity tid newTask) otherTasks <- API.updateTask id body dependDiff
  pure $ runInsM_ $ do
    graph <- I.read
    let isIdChanged taskId = (G.getBody taskId prevGraph) /= (G.getBody taskId graph)
    let taskId = review taskIdP tid
    when (not $ isIdChanged taskId) $ insM $ G.setBody taskId newTask

    for_ otherTasks $ \(Entity thisId t)-> do
      let thisId' = review taskIdP thisId
      when (not $ isIdChanged thisId') $
        insM $ G.setBody thisId' t

execReq graph (Create { replaceId }) = do
  let graphId = graph ^. G.idL
      mBody = G.getBody replaceId graph
      dependsOn = mapMaybe (preview taskIdP) $ G.children replaceId graph
      dependedBy = mapMaybe (preview taskIdP) $ G.parents replaceId graph
      order = G.orderInGraph replaceId (preview taskIdP) graph
  case mBody of
    Nothing -> pure $ Tuple []
    Just body -> do
      Tuple (Entity tid task) otherTasks <- API.createTask graphId dependsOn dependedBy order body
      let newId = review taskIdP tid
      pure $ runInsM_ $ do
        newGraph <- I.read
        let isIdChanged taskId = (G.getBody taskId graph) /= (G.getBody taskId newGraph)
        insM $ G.swap replaceId newId
        insM $ G.setBody newId task
        for_ otherTasks $ \(Entity thisId t)-> do
          let thisId' = review taskIdP thisId
          when (not $ isIdChanged thisId') $ insM $ G.setBody thisId' t

execReq graph (Delete { id, migration }) = do
  otherTasks <- API.deleteTask id migration
  pure $ runInsM_ $ do
    for_ otherTasks $ \(Entity thisId t)-> do
      newGraph <- I.read
      let isIdChanged taskId = (G.getBody taskId graph) /= (G.getBody taskId newGraph)
          thisId' = review taskIdP thisId
      when (not $ isIdChanged thisId') $ insM $ G.setBody thisId' t

execReq graph (Reorder { id, dependDiff, order }) = do
  otherTasks <- API.reorderTask order dependDiff id
  pure $ runInsM_ $ do
    for_ otherTasks $ \(Entity thisId t)-> do
      newGraph <- I.read
      let isIdChanged taskId = (G.getBody taskId graph) /= (G.getBody taskId newGraph)
          thisId' = review taskIdP thisId
      when (not $ isIdChanged thisId') $ insM $ G.setBody thisId' t

observeGM
  :: forall g k
  .  Ord k
  => G.IsGraph g
  => G.RawGraph g
  => G.IdGraph g String
  => G.BodyGraph g Task
  => G.RelGraph g
  => ToReq k
  => LikeTaskId k
  => GM.GraphManager Task g k
  -> Effect (Effect Unit) -- The Effect to unsubscribe
observeGM gm = do
  let queue = GM.subscribe gm
  queueId <- randomString

  reqHandler <- optimise $ \req-> launchAff_ $ do
    g <- liftEffect $ GM.readGraph gm
    endoIns <- execReq g req
    liftEffect $ GM.modifyWithId (Just queueId) gm endoIns

  Q.on queue queueId $ traverse_ reqHandler <<< insToReqs

  pure $ void $ Q.del queue queueId

optimise
  :: forall k
  .  Ord k
  => LikeTaskId k
  => (Req k -> Effect Unit)
  -> Effect (Req k -> Effect Unit)
optimise run = do
  pendingReqs <- Ref.new $ Map.empty
  pure $ handleReq pendingReqs
  where
    handleReq pendings req =
      if (isEmpty req)
         then pure unit
         else debounce pendings req

    debounce pendings req@(Update rec@{ id, dependDiff, body: Just newBody }) = do
      pendingMap <- Ref.read pendings
      let mapKey = getId req
      case Map.lookup mapKey pendingMap of
        Just (Tuple _ (Update existing)) -> do
          -- User is trying to update with a high frequency
          let merged = mergeUpdate existing rec
          if merged == existing
             then pure unit
             else scheduleExec (Update merged) pendings
        _ -> do
          scheduleExec req pendings

    debounce pendings req@(Create { replaceId }) = do
      pendingMap <- Ref.read pendings
      let mapKey = getId req
      scheduleExec req pendings

    debounce pendings req@(Delete { id }) = do
      pendingMap <- Ref.read pendings
      let mapKey = getId req
      case Map.lookup mapKey pendingMap of
        Nothing -> pure unit
        Just existing -> do
          Ref.modify_ (Map.delete mapKey) pendings
      run req
    debounce _ a = run a

    scheduleExec req pendings = do
      -- If there is no changes within the period, we send the request
      -- otherwise just ignore because we have scheduled another execution
      let mapKey = getId req
      launchAff_ $ do
        myScheduleId <- liftEffect randomString
        liftEffect $ Ref.modify_ (Map.insert mapKey (Tuple myScheduleId req)) pendings
        delay $ Milliseconds 1000.0
        afterWait <- liftEffect (Map.lookup mapKey <$> Ref.read pendings)
        if afterWait == Just (Tuple myScheduleId req)
          then liftEffect $ do
                 Ref.modify_ (Map.delete mapKey) pendings
                 run req
          else pure unit

    mergeUpdate
      :: { id :: TaskId, dependDiff :: DependDiff, body :: Maybe Task }
      -> { id :: TaskId, dependDiff :: DependDiff, body :: Maybe Task }
      -> { id :: TaskId, dependDiff :: DependDiff, body :: Maybe Task }
    mergeUpdate { dependDiff: d1 } { dependDiff: d2, body, id } =
      { id, body, dependDiff: mergeDependDiff d1 d2 }

    mergeDependDiff { createDependsOn: co1
                    , deleteDependsOn: do1
                    , createDependedBy: cb1
                    , deleteDependedBy: db1
                    }
                    { createDependsOn: co2
                    , deleteDependsOn: do2
                    , createDependedBy: cb2
                    , deleteDependedBy: db2
                    } =
      { createDependsOn: (Array.difference co1 do2) <> co2
      , deleteDependsOn: (Array.difference do1 co2) <> do2
      , createDependedBy: (Array.difference cb1 db2) <> cb2
      , deleteDependedBy: (Array.difference db1 cb2) <> db2
      }

    getId :: Req k -> k
    getId (Update { id }) = review taskIdP id
    getId (Delete { id }) = review taskIdP id
    getId (Create { replaceId }) = replaceId
    getId (Reorder { id }) = review taskIdP id
