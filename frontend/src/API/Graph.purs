module API.Graph where

import Prelude

import Data.Argonaut (encodeJson)
import Effect.Aff (Aff)
import Model.Graph (Graph, GraphEntity)
import Affjax as AX
import Affjax.RequestBody as AReq
import Affjax.ResponseFormat as ARes
import Affjax.Utility (orThrow)

import API.Utility (post)
import Web.CSRF.Yesod (withCSRFToken)

getGraphs :: Aff (Array GraphEntity)
getGraphs = do
  orThrow $ AX.get ARes.json "/api/graphs"

createGraph :: Graph -> Aff GraphEntity
createGraph graph = do
  orThrow $ withCSRFToken post ARes.json "/api/graphs" (AReq.json $ encodeJson graph)
