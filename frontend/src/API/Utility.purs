module API.Utility where

import Affjax as AX
import Affjax.RequestBody as RequestBody
import Affjax.ResponseFormat as ResponseFormat
import Affjax.RequestHeader as RequestHeader
import Data.Either (Either(..))
import Data.Maybe (Maybe(..))
import Data.HTTP.Method (Method(POST, PUT))
import Effect.Aff (Aff)

post
  :: forall a
  .  Array RequestHeader.RequestHeader
  -> ResponseFormat.ResponseFormat a
  -> AX.URL
  -> RequestBody.RequestBody
  -> Aff (Either AX.Error (AX.Response a))
post headers rf u c = AX.request (AX.defaultRequest
  { method = Left POST
  , url = u
  , content = Just c
  , responseFormat = rf
  , headers = headers
  })

put
  :: forall a
  .  Array RequestHeader.RequestHeader
  -> ResponseFormat.ResponseFormat a
  -> AX.URL
  -> RequestBody.RequestBody
  -> Aff (Either AX.Error (AX.Response a))
put headers rf u c = AX.request (AX.defaultRequest
  { method = Left PUT
  , url = u
  , content = Just c
  , responseFormat = rf
  , headers = headers
  })
