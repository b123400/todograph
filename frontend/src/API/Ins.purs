module API.Ins where

import Prelude

import API.Task (DependDiff)
import Data.Array ((:))
import Data.Array as Array
import Data.Foldable (foldl)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Show (genericShow)
import Data.Ins (Ins)
import Data.Ins as I
import Data.Maybe (Maybe(..), maybe)
import Data.Set (Set)
import Data.Set as Set
import Data.Set.NonEmpty as SN
import Data.Tree (DependsOnMigration(..), Order(..))
import Data.Tuple (Tuple(..))
import Model.HybridTask (ComposingTaskId(..))
import Model.Relationship (emptyDiff)
import Model.Task (Task, TaskId)


type Batch v k = { ids :: SN.NonEmptySet k, ins :: Array (Ins v k) }
data Req k = Update { id :: TaskId, dependDiff :: DependDiff, body :: Maybe Task }
           | Delete { id :: TaskId, migration :: DependsOnMigration TaskId }
           -- Create, and then replace the entry of k
           | Create { replaceId :: k }
           | Reorder { id :: TaskId, dependDiff :: DependDiff, order :: Order TaskId }

derive instance genericReq :: Generic (Req k) _
instance eqReq :: Eq k => Eq (Req k) where eq = genericEq
instance showReq :: Show k => Show (Req k) where show = genericShow

insToReqs :: forall k. ToReq k => Ord k => Array (Ins Task k) -> Array (Req k)
insToReqs arr = Array.concat $ reqByEndpoint <$> batchesById arr

class ToReq k where
  newReq :: k -> Ins Task k -> Maybe (Req k)
  merge :: Ins Task k -> (Req k) -> Maybe (Req k)

instance taskIdToReq :: ToReq String{-TaskId-} where
  newReq id = Just <<< case _ of
    I.UpdateBody _ _ -> baseUpdate
    I.AddRel _ _     -> baseUpdate
    I.DeleteRel _ _  -> baseUpdate
    I.DeleteBody _ _ -> Delete { id, migration: DeleteRelationships }
    I.Reorder _ _    -> Reorder { id, dependDiff: emptyDiff, order: NoOrder }
    I.Nudge _        -> baseUpdate
    where baseUpdate = Update { id, dependDiff: emptyDiff, body: Nothing }

  merge (I.UpdateBody v _) (Update u) = Just $ Update u { body = Just v }

  merge (I.AddRel from to) (Update u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Update u { dependDiff = d { createDependsOn = Array.snoc d.createDependsOn to } }
    else if id == to
    then Just $ Update u { dependDiff = d { createDependedBy = Array.snoc d.createDependedBy from } }
    else Just $ Update u

  merge (I.DeleteRel from to) (Update u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Update u { dependDiff = d { deleteDependsOn = Array.snoc d.deleteDependsOn to } }
    else if id == to
    then Just $ Update u { dependDiff = d { deleteDependedBy = Array.snoc d.deleteDependedBy from } }
    else Just $ Update u

  merge (I.DeleteBody _ migration) (Delete r) = Just $ Delete r { migration = migration }

  merge (I.Reorder k order) r@(Reorder u) = Just $ Reorder u { order = order }
  merge (I.AddRel from to) (Reorder u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Reorder u { dependDiff = d { createDependsOn = Array.snoc d.createDependsOn to } }
    else if id == to
         then Just $ Reorder u { dependDiff = d { createDependedBy = Array.snoc d.createDependedBy from } }
         else Just $ Reorder u

  merge (I.DeleteRel from to) (Reorder u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Reorder u { dependDiff = d { deleteDependsOn = Array.snoc d.deleteDependsOn to } }
    else if id == to
         then Just $ Reorder u { dependDiff = d { deleteDependedBy = Array.snoc d.deleteDependedBy from } }
         else Just $ Reorder u

  merge (I.Reorder k order) r@(Update u@{ id, dependDiff, body: Nothing })
    | id /= k = Nothing
    | otherwise = Just $ Reorder { id, dependDiff, order }
  merge (I.Nudge _) r@(Update _) = Just r
  merge _ _ = Nothing


instance composingToReq :: ToReq ComposingTaskId where
  newReq :: ComposingTaskId -> Ins Task ComposingTaskId -> Maybe (Req ComposingTaskId)
  newReq (Existing id) = Just <<< case _ of
    I.UpdateBody _ _ -> baseUpdate
    I.AddRel _ _     -> baseUpdate
    I.DeleteRel _ _  -> baseUpdate
    I.DeleteBody _ _ -> Delete { id, migration: DeleteRelationships }
    I.Reorder _ _    -> Reorder { id, dependDiff: emptyDiff, order: NoOrder }
    I.Nudge _        -> baseUpdate
    where baseUpdate = Update { id, dependDiff: emptyDiff, body: Nothing }

  newReq id@(Composing _) = case _ of
    I.UpdateBody v _ -> Just $ Create { replaceId: id }
    I.Nudge _        -> Just $ Create { replaceId: id }
    _ -> Nothing

  merge :: Ins Task ComposingTaskId -> Req ComposingTaskId -> Maybe (Req ComposingTaskId)
  merge (I.UpdateBody v _) (Update u) = Just $ Update u { body = Just v }

  merge (I.AddRel (Existing from) (Existing to)) (Update u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Update u { dependDiff = d { createDependsOn = Array.snoc d.createDependsOn to } }
    else if id == to
    then Just $ Update u { dependDiff = d { createDependedBy = Array.snoc d.createDependedBy from } }
    else Just $ Update u

  merge (I.DeleteRel (Existing from) (Existing to)) (Update u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Update u { dependDiff = d { deleteDependsOn = Array.snoc d.deleteDependsOn to } }
    else if id == to
    then Just $ Update u { dependDiff = d { deleteDependedBy = Array.snoc d.deleteDependedBy from } }
    else Just $ Update u

  merge (I.DeleteBody _ migration) (Delete r) = m <#> \mig-> Delete r { migration = mig }
    where m = case migration of DeleteRelationships -> Just DeleteRelationships
                                DeleteOrphans -> Just DeleteOrphans
                                BecomeDependsOn (Existing t) -> Just $ BecomeDependsOn t
                                _ -> Nothing

  merge (I.Reorder (Existing k) order) r@(Reorder u) = (composingOrder order) <#> \ord-> Reorder u { order = ord }

  merge (I.AddRel (Existing from) (Existing to)) (Reorder u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Reorder u { dependDiff = d { createDependsOn = Array.snoc d.createDependsOn to } }
    else if id == to
         then Just $ Reorder u { dependDiff = d { createDependedBy = Array.snoc d.createDependedBy from } }
         else Just $ Reorder u

  merge (I.DeleteRel (Existing from) (Existing to)) (Reorder u@{ id, dependDiff: d }) =
    if id == from
    then Just $ Reorder u { dependDiff = d { deleteDependsOn = Array.snoc d.deleteDependsOn to } }
    else if id == to
         then Just $ Reorder u { dependDiff = d { deleteDependedBy = Array.snoc d.deleteDependedBy from } }
         else Just $ Reorder u

  merge (I.Reorder (Existing k) order) r@(Update u@{ id, dependDiff, body: Nothing })
    | id /= k = Nothing
    | otherwise = (composingOrder order) <#> \ord-> Reorder { id, dependDiff, order: ord }
  merge (I.UpdateBody _ k@(Composing _)) r@(Create { replaceId })
    | replaceId == k = Just r
    | otherwise = Nothing
  merge (I.Nudge _) u@(Update _) = Just u
  merge (I.Nudge _) c@(Create _) = Just c
  merge _ _ = Nothing


composingOrder :: Order ComposingTaskId -> Maybe (Order TaskId)
composingOrder = case _ of
  NoOrder -> Just NoOrder
  After (Existing a) -> Just $ After a
  Before (Existing a) -> Just $ Before a
  _ -> Nothing

reqByEndpoint :: forall k. ToReq k => Ord k => Batch Task k -> Array (Req k)
reqByEndpoint { ids, ins } = case Array.uncons ins of
  Nothing -> []
  Just { head, tail } ->
    let mBaseReq = newReq taskId head
    in case mBaseReq of
         Nothing ->
           let restBatch = { ids: _, ins: tail } <$> (SN.fromSet $ relatedIdsInArr tail)
           in maybe [] reqByEndpoint restBatch
         Just baseReq ->
           let Tuple req rest = mergeTilNothing ins baseReq
               restBatch = { ids: _, ins: rest } <$> (SN.fromSet $ relatedIdsInArr rest)
           in req : (maybe [] reqByEndpoint restBatch)
  where taskId = SN.min ids
        mergeTilNothing inss acc
          | Just { head, tail } <- Array.uncons inss =
              case merge head acc of
                Nothing -> Tuple acc inss
                Just merged -> mergeTilNothing tail merged
          | otherwise = Tuple acc []

batchesById :: forall v k. Ord k => Array (Ins v k) -> Array (Batch v k)
batchesById = foldl handle []
  where handle arr ins
          | Just { init, last } <- Array.unsnoc arr =
              case addToBatch ins last of
                Just newBatch -> Array.snoc init newBatch
                Nothing -> Array.snoc arr $ toBatch ins
          | otherwise = [toBatch ins]

toBatch :: forall v k. Ord k => Ins v k -> Batch v k
toBatch ins = { ids: relatedIds ins , ins: [ins] }

addToBatch :: forall v k. Ord k => Ins v k -> Batch v k -> Maybe (Batch v k)
addToBatch newIns b@{ ids, ins } = { ids: _, ins: Array.snoc ins newIns } <$> commons
  where commons = SN.fromSet $ commonIds newIns $ SN.toSet ids

relatedIdsInArr :: forall v k. Ord k => Array (Ins v k) -> Set k
relatedIdsInArr =
  foldl (\acc ins->
    let set = SN.toSet $ relatedIds ins
    in if Set.isEmpty acc
       then set
       else Set.intersection acc set) Set.empty

relatedIds :: forall v k. Ord k => Ins v k -> SN.NonEmptySet k
relatedIds = case _ of
  I.AddRel a b -> SN.insert a $ SN.singleton b
  I.UpdateBody v k -> SN.singleton k
  I.DeleteRel a b -> SN.insert a $ SN.singleton b
  I.DeleteBody a _ -> SN.singleton a
  I.Reorder k _ -> SN.singleton k
  I.Nudge k -> SN.singleton k

commonIds :: forall v k. Ord k => Ins v k -> Set k -> Set k
commonIds = Set.intersection <<< SN.toSet <<< relatedIds

isEmpty :: forall k. Req k -> Boolean
isEmpty (Update { dependDiff, body: Nothing }) | dependDiff == emptyDiff = true
isEmpty (Delete _) = false
isEmpty (Reorder { dependDiff, order: NoOrder }) | dependDiff == emptyDiff = true
isEmpty (Create _) = false
isEmpty _ = false
