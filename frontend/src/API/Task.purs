module API.Task where

import Prelude

import API.Utility (post, put)
import Affjax as AX
import Affjax.RequestBody as AReq
import Affjax.ResponseFormat as ARes
import Affjax.Utility (orThrow)
import Data.Argonaut (class DecodeJson, class EncodeJson, decodeJson, encodeJson, jsonEmptyObject, (.:), (:=), (~>))
import Data.Argonaut.Encode ((~>?))
import Data.Default (class Default, default)
import Data.Foldable (foldl)
import Data.Graph as G
import Data.Lens ((.~))
import Data.Maybe (Maybe(..))
import Data.Tree (DependsOnMigration(..), Order(..))
import Data.Tuple (Tuple(..), snd)
import Effect.Aff (Aff)
import Model.Entity (Entity(..))
import Model.Graph (GraphId)
import Model.Relationship (Depend(..), Relationship(..))
import Model.Task (Task, TaskEntity, TaskId)
import Web.CSRF.Yesod (withCSRFToken)


data GetTasksResponse = GetTasksResponse (Array TaskEntity) (Array Depend)

instance decodeJsonGetTasksResponse :: DecodeJson GetTasksResponse where
  decodeJson json = do
    obj <- decodeJson json
    tasks <- obj .: "tasks"
    relationships <- obj .: "relationships"
    pure $ GetTasksResponse tasks relationships

getTasks
  :: forall g
  .  Default (g TaskId)
  => G.IdGraph g GraphId
  => G.RelGraph g
  => G.BodyGraph g Task
  => GraphId
  -> Boolean -- includesCompleted
  -> Aff (g TaskId)
getTasks graphId includesCompleted = do
  let query = if includesCompleted
              then "?includesCompleted=true"
              else ""
  (GetTasksResponse tasks relationships) <- orThrow $ AX.get ARes.json $ "/api/graphs/" <> graphId <> "/tasks" <> query
  let g1 = foldl (\g (Entity taskId task)-> G.addBody taskId task g) default tasks
      g2 = foldl (\g (Depend (Relationship from to))-> snd $ G.addRelationship from to g) g1 relationships
  pure (g2 # G.idL .~ graphId)

newtype CreateTaskRequest = CreateTaskRequest { graphId :: GraphId
                                              , dependsOnIds :: Array TaskId
                                              , dependedByIds :: Array TaskId
                                              , task :: Task
                                              , taskOrder :: Order TaskId
                                              }

instance encodeJsonCreateTaskRequest :: EncodeJson CreateTaskRequest where
  encodeJson (CreateTaskRequest r) = do
       "task" := r.task
    ~> "dependsOn" := r.dependsOnIds
    ~> "dependedBy" := r.dependedByIds
    ~> (case r.taskOrder of (Before taskId) -> Just $ "before" := taskId
                            (After taskId)  -> Just $ "after" := taskId
                            NoOrder         -> Nothing)
    ~>? jsonEmptyObject

data AddTaskResponse = AddTaskResponse TaskEntity (Array TaskEntity)

instance decodeJsonAddTaskResponse :: DecodeJson AddTaskResponse where
  decodeJson json = do
    obj <- decodeJson json
    tasks <- obj .: "task"
    relationships <- obj .: "updatedTasks"
    pure $ AddTaskResponse tasks relationships

createTask :: GraphId
           -> (Array TaskId) -- depends on task ids
           -> (Array TaskId) -- depended by task ids
           -> Order TaskId
           -> Task
           -> Aff (Tuple TaskEntity (Array TaskEntity))
createTask graphId dependsOnIds dependedByIds taskOrder task = do
  let req = CreateTaskRequest { graphId, dependsOnIds, dependedByIds, task, taskOrder }
  (AddTaskResponse newTask updatedTasks) <- orThrow $ withCSRFToken post ARes.json ("/api/graphs/" <> graphId <> "/tasks") (AReq.json $ encodeJson req)
  pure $ Tuple newTask updatedTasks

newtype ReorderTaskRequest = ReorderTaskRequest
  { order :: Order TaskId
  , dependDiff :: DependDiff
  }

instance encodeJsonReorderTaskRequest :: EncodeJson ReorderTaskRequest where
  encodeJson (ReorderTaskRequest r) =
       "deleteDependedBy" := r.dependDiff.deleteDependedBy
    ~> "deleteDependsOn"  := r.dependDiff.deleteDependsOn
    ~> "createDependedBy" := r.dependDiff.createDependedBy
    ~> "createDependsOn"  := r.dependDiff.createDependsOn
    ~> (case r.order of (Before taskId) -> Just $ "before" := taskId
                        (After taskId)  -> Just $ "after" := taskId
                        NoOrder         -> Nothing)
    ~>? jsonEmptyObject

type DependDiff = { createDependsOn :: Array TaskId
                  , deleteDependsOn :: Array TaskId
                  , createDependedBy :: Array TaskId
                  , deleteDependedBy :: Array TaskId
                  }

newtype ReorderTaskResponse = ReorderTaskResponse (Array TaskEntity)

instance decodeJsonReorderTaskResponse :: DecodeJson ReorderTaskResponse where
  decodeJson json = do
    obj <- decodeJson json
    relationships <- obj .: "updatedTasks"
    pure $ ReorderTaskResponse relationships

reorderTask :: Order TaskId -> DependDiff -> TaskId -> Aff (Array TaskEntity)
reorderTask order dependDiff taskId = do
  let req = ReorderTaskRequest { order, dependDiff }
  (ReorderTaskResponse updatedTasks) <- orThrow $ withCSRFToken put ARes.json ("/api/tasks/" <> taskId <> "/reorder") (AReq.json $ encodeJson req)
  pure updatedTasks

newtype UpdateTaskRequest = UpdateTaskRequest { task :: Maybe Task, dependDiff :: DependDiff }

instance encodeJsonUpdateTaskRequest :: EncodeJson UpdateTaskRequest where
  encodeJson (UpdateTaskRequest r) =
       "deleteDependedBy" := r.dependDiff.deleteDependedBy
    ~> "deleteDependsOn"  := r.dependDiff.deleteDependsOn
    ~> "createDependedBy" := r.dependDiff.createDependedBy
    ~> "createDependsOn"  := r.dependDiff.createDependsOn
    ~> "task" := r.task
    ~> jsonEmptyObject

data UpdateTaskResponse = UpdateTaskResponse TaskEntity (Array TaskEntity)

instance decodeJsonUpdateTaskResponse :: DecodeJson UpdateTaskResponse where
  decodeJson json = do
    obj <- decodeJson json
    tasks <- obj .: "task"
    relationships <- obj .: "updatedTasks"
    pure $ UpdateTaskResponse tasks relationships

updateTask :: TaskId -> Maybe Task -> DependDiff -> Aff (Tuple TaskEntity (Array TaskEntity))
updateTask taskId task dependDiff = do
  let req = UpdateTaskRequest { task, dependDiff }
  (UpdateTaskResponse newTask updatedTasks) <- orThrow $ withCSRFToken put ARes.json ("/api/tasks/" <> taskId) (AReq.json $ encodeJson req)
  pure (Tuple newTask updatedTasks)


newtype DeleteTaskRequest = DeleteTaskRequest { dependsOnMigration :: DependsOnMigration TaskId }

instance encodeJsonDeleteTaskRequest :: EncodeJson DeleteTaskRequest where
  encodeJson (DeleteTaskRequest r) = (case r.dependsOnMigration of
      DeleteRelationships -> "delete" := "relationships"
      DeleteOrphans -> "delete" := "orphans"
      BecomeDependsOn newParentTaskId -> "becomeDependsOn" := newParentTaskId)
    ~> jsonEmptyObject

newtype DeleteTaskResponse = DeleteTaskResponse (Array TaskEntity)

instance decodeJsonDeleteTaskResponse :: DecodeJson DeleteTaskResponse where
  decodeJson json = do
    obj <- decodeJson json
    relationships <- obj .: "updatedTasks"
    pure $ DeleteTaskResponse relationships

deleteTask :: TaskId -> DependsOnMigration TaskId -> Aff (Array TaskEntity)
deleteTask taskId dependsOnMigration = do
  let req = DeleteTaskRequest { dependsOnMigration }
  (DeleteTaskResponse updatedTasks) <- orThrow $ withCSRFToken put ARes.json ("/api/tasks/" <> taskId <> "/delete") (AReq.json $ encodeJson req)
  pure updatedTasks
