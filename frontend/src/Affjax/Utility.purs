module Affjax.Utility where

import Prelude

import Affjax as AX
import Control.Monad.Error.Class (throwError, class MonadThrow)
import Data.Argonaut (decodeJson, Json, class DecodeJson)
import Data.Either (Either(..))
import Effect.Aff (error)
import Effect.Exception (Error)

orThrow :: forall m r a
        .  Bind m
        => MonadThrow Error m
        => DecodeJson r
        => m (Either AX.Error { body :: Json | a })
        -> m r
orThrow m = do
  res <- m
  case res of
    Left err -> throwError $ error $ AX.printError err
    Right { body } ->
      case decodeJson body of
        Right r -> pure r
        Left errorMessage -> throwError $ error errorMessage
