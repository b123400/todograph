module Web.CSRF.Yesod where

import Prelude

import Affjax.RequestHeader (RequestHeader(..))
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Data.Maybe (Maybe(..))
import Data.Cookie (getCookieWithKey)

foreign import getCsrfCookieName :: forall a. (a -> Maybe a) -> Maybe a -> Effect (Maybe String)
foreign import getCsrfHeaderName :: forall a. (a -> Maybe a) -> Maybe a -> Effect (Maybe String)

csrfCookieName :: Effect (Maybe String)
csrfCookieName = getCsrfCookieName Just Nothing

csrfToken :: Effect (Maybe String)
csrfToken = do
  mName <- csrfCookieName
  case mName of
    Just n -> getCookieWithKey n
    Nothing -> pure Nothing

csrfHeaderName :: Effect (Maybe String)
csrfHeaderName = getCsrfHeaderName Just Nothing

withCSRFToken :: forall m a rf u c . MonadEffect m => (Array RequestHeader -> rf -> u -> c -> m a) -> rf -> u -> c -> m a
withCSRFToken fn rf u c = do
  headerName <- liftEffect csrfHeaderName
  token <- liftEffect csrfToken
  let headers = case {n: headerName, t: token} of
        {n: (Just name), t: (Just t)} -> [RequestHeader name t]
        _ -> []
  fn headers rf u c
