exports.getCsrfCookieName = function (Just){
  return function(Nothing) {
    return function() {
      if (window.csrfCookieName === undefined) {
        return Nothing;
      } else {
        return Just(window.csrfCookieName);
      }
    };
  }
}

exports.getCsrfHeaderName = function(Just) {
  return function(Nothing) {
    return function() {
      if (window.csrfHeaderName === undefined) {
        return Nothing;
      } else {
        return Just(window.csrfHeaderName);
      }
    }
  }
}
