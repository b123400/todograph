module Web.HTML.Event.DragEvent.Cursor where

import Prelude ((<<<))
import Unsafe.Coerce (unsafeCoerce)
import Web.HTML.Event.DragEvent (DragEvent)
import Web.UIEvent.MouseEvent as ME

clientX :: DragEvent -> Int
clientX = ME.clientX <<< unsafeCoerce

clientY :: DragEvent -> Int
clientY = ME.clientY <<< unsafeCoerce
