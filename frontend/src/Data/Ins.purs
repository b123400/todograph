module Data.Ins where

import Prelude

import Data.Bifunctor (lmap, rmap)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Show (genericShow)
import Data.Lens (Lens', (^.), (.~))
import Data.Tree (Order, DependsOnMigration)
import Data.Tuple (Tuple(..), fst)

type InsG v g k = Tuple (Array (Ins v k)) (g k)
data Ins v k
  = UpdateBody v k
  | AddRel k k
  | DeleteRel k k
  | DeleteBody k (DependsOnMigration k)
  | Reorder k (Order k)
  | Nudge k  -- When user explicit wants to submit something, e.g. pressing enter, unfocus text input

derive instance genericIns :: Generic (Ins v k) _
instance eqReq :: (Eq k, Eq v) => Eq (Ins v k) where eq = genericEq
instance showReq :: (Show v, Show k) => Show (Ins v k) where show = genericShow
instance insFunctor :: Functor (Ins v) where
  map fn = case _ of
    UpdateBody v k  -> UpdateBody v $ fn k
    AddRel a b      -> AddRel (fn a) (fn b)
    DeleteRel a b   -> DeleteRel (fn a) (fn b)
    DeleteBody k o  -> DeleteBody (fn k) (fn <$> o)
    Reorder k order -> Reorder (fn k) (map fn order)
    Nudge k         -> Nudge $ fn k

flat :: forall g v k. InsG v (InsG v g) k -> InsG v g k
flat (Tuple ins1 (Tuple ins2 g)) = Tuple (ins1 <> ins2) g

applyInsG :: forall g v k. (g k -> InsG v g k) -> InsG v g k -> InsG v g k
applyInsG applyFn = flat <<< rmap applyFn

composeInsG :: forall g v a. (g a -> InsG v g a) -> (g a -> InsG v g a) -> g a -> InsG v g a
composeInsG fn1 fn2 ga =
  let Tuple ins1 g1 = fn2 ga
      Tuple ins2 g2 = fn1 g1
  in Tuple (ins1 <> ins2) g2

modify :: forall g h v k. Lens' (g k) (h k) -> (h k -> InsG v h k) -> (g k -> InsG v g k)
modify lens fn gk =
  let hk = gk ^. lens
      Tuple ins newHk = fn hk
  in Tuple ins (gk # lens .~ newHk)

newtype InsM v g k r = InsM (g k -> Tuple (InsG v g k) r)

runInsM :: forall v g k r. InsM v g k r -> g k -> Tuple (InsG v g k) r
runInsM (InsM fn) = fn

runInsM_ :: forall v g k. InsM v g k Unit -> g k -> InsG v g k
runInsM_ (InsM fn) = fn >>> fst

class ToInsM i v (g :: Type -> Type) k r | i -> v, i -> g, i -> k, i -> r where
  insM :: i -> InsM v g k r


instance insMEndo :: ToInsM (g k -> g k) v g k Unit where
  insM fn = InsM $ \g-> Tuple (Tuple [] $ fn g) unit
else instance insMRaw :: ToInsM (g k -> Tuple (Tuple (Array (Ins v k)) (g k)) r) v g k r where
  insM = InsM

else instance insMUnit :: ToInsM (g k -> Tuple (Array (Ins v k)) (g k)) v g k Unit where
  insM = InsM <<< map (flip Tuple unit)

read :: forall v g k. InsM v g k (g k)
read = InsM $ \g-> Tuple (Tuple [] g) g

clearIns :: forall v g k r. InsM v g k r -> InsM v g k r
clearIns (InsM fn) = InsM $ lmap (lmap $ const []) <<< fn

instance insMFunctor :: Functor (InsM v g k) where
  map mapFn (InsM fn) = InsM (map mapFn <<< fn)

instance insMApply :: Apply (InsM v g k) where
  apply (InsM apFn) (InsM fn) = InsM $ \g->
    let Tuple (Tuple ins1 g1) r = fn g
        Tuple (Tuple ins2 g2) applyFn = apFn g1
    in Tuple (Tuple (ins1 <> ins2) g2) (applyFn r)

instance insMApplicative :: Applicative (InsM v g k) where
  pure a = InsM (\g-> Tuple (Tuple [] g) a)

instance insMBind :: Bind (InsM v g k) where
  bind (InsM fn) bindFn = InsM $ \g->
    let Tuple (Tuple ins1 g1) r1 = fn g
        InsM fn2 = bindFn r1
        Tuple (Tuple ins2 g2) r2 = fn2 g1
    in Tuple (Tuple (ins1 <> ins2) g2) r2

instance insMMonad :: Monad (InsM v g k)

mapInsG :: forall v g k l. Functor g => (k -> l) -> InsG v g k -> InsG v g l
mapInsG fn (Tuple ins graph) = Tuple (map fn <$> ins) (fn <$> graph)
