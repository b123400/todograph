module Data.Graph where

import Prelude

import Control.Alt ((<|>))
import Data.Array (mapMaybe, null, (!!), (:))
import Data.Array as Array
import Data.Bifunctor (bimap, lmap, rmap)
import Data.Default (class Default, default)
import Data.Foldable (any, for_)
import Data.Ins (Ins(..), InsG, insM, modify, runInsM_)
import Data.Ins as I
import Data.Lens (Lens', lens, over, view, (%~), (^.), (.~))
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe, isJust)
import Data.Set (Set)
import Data.Set as Set
import Data.Tree (DependsOnMigration(..), Diff, Forest, Order(..), Path, toForest)
import Data.Tree as Tree
import Data.Tuple (Tuple(..), snd)
import Model.Relationship (Relationship(..))

newtype Graph a = Graph { nodes :: Array a, relationships :: Set (Relationship a a) }
newtype WithBody v g k = WithBody { bodies :: Map k v, graph :: g k }
newtype WithForest g a = WithForest { forest :: Forest a, graph :: g a }
newtype SelectedA g a = SelectedA { selected :: Maybe (Path a), graph :: g a }
newtype WithId gk g k = WithId { id :: gk, graph :: g k }

instance defaultGraph :: Default (Graph a) where
  default = Graph { nodes: [], relationships: Set.empty }

instance defaultBGraph :: (Default (g k))=> Default (WithBody v g k) where
  default = WithBody { bodies: Map.empty :: Map k v, graph: default }

instance defaultForest :: (Default (g k))=> Default (WithForest g k) where
  default = WithForest { forest: [], graph: default }

instance defaultSelected :: (Default (g k))=> Default (SelectedA g k) where
  default = SelectedA { selected: Nothing, graph: default }

instance defaultWithStringId :: (Default (g k))=> Default (WithId String g k) where
  default = WithId { id: "", graph: default }

else instance defaultWithId :: (Default (g k), Default gk)=> Default (WithId gk g k) where
  default = WithId { id: default, graph: default }

class IsGraph g where
  graphL :: forall a. Lens' (g a) (Graph a)

instance graphIsGraph :: IsGraph Graph where
  graphL = lens identity const

instance bIsGraph :: (IsGraph g)=> IsGraph (WithBody v g) where
  graphL = graphL >>> innerL

instance treeIsGraph :: (IsGraph g)=> IsGraph (WithForest g) where
  graphL = graphL >>> innerL

instance selectedIsGraph :: (IsGraph g)=> IsGraph (SelectedA g) where
  graphL = graphL >>> innerL

instance withIdIsGraph :: (IsGraph g)=> IsGraph (WithId gk g) where
  graphL = graphL >>> innerL

class Inner g where
  innerL :: forall h a. Lens' (g h a) (h a)

instance innerBody :: Inner (WithBody v) where
  innerL = lens (\(WithBody { graph })-> graph) (\(WithBody r) g-> WithBody r { graph = g })

instance innerForest :: Inner WithForest where
  innerL = lens (\(WithForest { graph })-> graph) (\(WithForest r) g-> WithForest r { graph = g })

instance innerSelect :: Inner SelectedA where
  innerL = lens (\(SelectedA { graph })-> graph) (\(SelectedA r) g-> SelectedA r { graph = g })

instance innerWithId :: Inner (WithId gk) where
  innerL = lens (\(WithId { graph })-> graph) (\(WithId r) g-> WithId r { graph = g })

-- Graph without body
class RawGraph g where
  addNode :: forall a. (Ord a) => a -> g a -> g a
  deleteNode :: forall a. (Ord a) => a -> g a -> g a
  children :: forall a. (Ord a)=> a -> g a -> Array a
  parents :: forall a. (Ord a)=> a -> g a -> Array a
  pathExist :: forall a. (Ord a) => a -> a -> g a -> Boolean
  applyOrder :: forall k. (Ord k) => k -> Order k -> g k -> g k

instance rawGraph :: RawGraph Graph where
  addNode n (Graph g) | Array.elem n g.nodes = Graph g
                      | otherwise = Graph g { nodes = Array.snoc g.nodes n }

  deleteNode n (Graph {nodes, relationships}) =
    Graph { nodes: Array.filter ((/=) n) nodes
          , relationships: Set.filter (\(Relationship a b)-> a /= n && b /= n) relationships
          }

  children a (Graph { nodes, relationships }) =
    setOrderedByArray nodes $ Set.mapMaybe (\(Relationship f t)-> if f == a then Just t else Nothing) relationships

  parents a (Graph { nodes, relationships }) =
    setOrderedByArray nodes $ Set.mapMaybe (\(Relationship f t)-> if t == a then Just f else Nothing) relationships

  pathExist from to graph = pathExist' from to
    where pathExist' from' to'
            | from' == to' = true
            | otherwise  = any (\f-> pathExist' f to') $ children from' graph

  applyOrder key order = nodesLens %~ Tree.applyOrderInArray key order
    where nodesLens :: forall a. Lens' (Graph a) (Array a)
          nodesLens = lens (\(Graph { nodes })-> nodes) (\(Graph r) nodes-> Graph r { nodes = nodes })

descendant :: forall g k. Ord k => RawGraph g => k -> g k -> Array k
descendant key graph =
  let c = children key graph
      grandChildren = (flip descendant graph) =<< c
  in c <> grandChildren

orderInGraph
  :: forall g k l
  .  IsGraph g
  => Eq k
  => k
  -> (k -> Maybe l)
  -> g k
  -> Order l
orderInGraph key cond graph =
  let Graph { nodes } = graph ^. graphL
  in fromMaybe NoOrder $ do
       index <-Array.elemIndex key nodes
       if index == 0
         then Before <$> (firstOk cond =<< Array.tail nodes)
         else After <$> (lastOk cond $ Array.take index nodes)
  where firstOk c arr = do
          i <- Array.findIndex (isJust <<< c) arr
          c =<< arr !! i
        lastOk c arr = do
          i <- Array.findLastIndex (isJust <<< c) arr
          c =<< arr !! i

instance rawBody :: (RawGraph g)=> RawGraph (WithBody v g) where
  addNode = over innerL <<< addNode
  deleteNode key graph = snd $ deleteBody key graph
  children a = children a <<< view innerL
  parents a = parents a <<< view innerL
  pathExist from to = pathExist from to <<< view innerL
  applyOrder k = over innerL <<< applyOrder k

instance treeRaw :: (RawGraph g, ForestGraph (WithForest g))=> RawGraph (WithForest g) where
  addNode n = reloadTree <<< (over innerL $ addNode n)
  deleteNode n = reloadTree <<< (over innerL $ deleteNode n)
  children a = children a <<< view innerL
  parents a = parents a <<< view innerL
  pathExist from to = pathExist from to <<< view innerL
  applyOrder k o g = reloadTree $ over innerL (applyOrder k o) g

instance idRaw :: (RawGraph g)=> RawGraph (WithId gk g) where
  addNode = over innerL <<< addNode
  deleteNode = over innerL <<< deleteNode
  children a = children a <<< view innerL
  parents a = parents a <<< view innerL
  pathExist from to = pathExist from to <<< view innerL
  applyOrder k = over innerL <<< applyOrder k

-- Graph with body
class BodyGraph g v | g -> v where
  addBody :: forall k. (Ord k)=> k -> v -> g k -> g k
  getBody :: forall k. (Ord k)=> k -> g k -> Maybe v
  setBody :: forall k. (Ord k)=> k -> v -> g k -> InsG v g k
  deleteBody :: forall k. (Ord k)=> k -> g k -> InsG v g k

modifyBody :: forall g v k. Ord k => BodyGraph g v => k -> (v -> v) -> g k -> InsG v g k
modifyBody key modFn gk =
  case getBody key gk of
    Nothing -> Tuple [] gk
    Just body ->
      let newBody = modFn body
      in setBody key newBody gk

childrenBody :: forall g v k. Ord k => BodyGraph g v => RawGraph g => k -> g k -> Array (Tuple k v)
childrenBody key graph =
  let childrenIds = children key graph
  in mapMaybe (\id-> (Tuple id) <$> getBody id graph) childrenIds

parentsBody :: forall g v k. Ord k => BodyGraph g v => RawGraph g => k -> g k -> Array (Tuple k v)
parentsBody key graph =
  let parentIds = parents key graph
  in mapMaybe (\id-> (Tuple id) <$> getBody id graph) parentIds

childrenBody' :: forall g v k. Ord k => BodyGraph g v => RawGraph g => k -> g k -> Array v
childrenBody' key graph = snd <$> childrenBody key graph

parentsBody' :: forall g v k. Ord k => BodyGraph g v => RawGraph g => k -> g k -> Array v
parentsBody' key graph = snd <$> parentsBody key graph

instance bodyGraph :: (RawGraph g)=> BodyGraph (WithBody v g) v where
  addBody key value (WithBody { bodies, graph }) =
    let newBodies = Map.insert key value bodies
    in WithBody { bodies: newBodies, graph: addNode key graph }

  getBody k (WithBody { bodies }) = Map.lookup k bodies

  setBody k v bg@(WithBody g@{ bodies }) =
    if Map.member k bodies
       then Tuple [UpdateBody v k] $ WithBody g { bodies = Map.insert k v bodies }
       else Tuple [] bg
  deleteBody key (WithBody { bodies, graph }) =
    let newBodies = Map.delete key bodies
    in Tuple [DeleteBody key DeleteRelationships] $ WithBody { bodies: newBodies, graph: deleteNode key graph }

instance idBodyGraph :: (BodyGraph g v)=> BodyGraph (WithId gk g) v where
  addBody k = over innerL <<< addBody k
  getBody k = getBody k <<< view innerL
  setBody k = modify innerL <<< setBody k
  deleteBody = modify innerL <<< deleteBody

class RelGraph g where
  addRelationship :: forall a v. (Ord a)=> a -> a -> g a -> InsG v g a
  deleteRelationship :: forall a v. (Ord a)=> a -> a -> g a -> InsG v g a
  switchParent
    :: forall a v
    . (Ord a)
    => a -- Original parent
    -> a -- New parent
    -> a -- Target node
    -> g a
    -> InsG v g a

  -- Replace a node with another while maintaining the relationships
  swap :: forall a. (Ord a)=> a -> a -> g a -> g a

instance relGraph :: RelGraph Graph where
  addRelationship from to (Graph g)
    | Array.notElem from g.nodes || Array.notElem to g.nodes = Tuple [] $ Graph g
    | pathExist from to (Graph g) = Tuple [] $ Graph g
    | otherwise = Tuple [AddRel from to] $ Graph g { relationships = Set.insert (Relationship from to) g.relationships }

  deleteRelationship from to (Graph g) = Tuple [DeleteRel from to] $ Graph g { relationships = Set.delete (Relationship from to) g.relationships }
  switchParent orig new target g =
    let Tuple ins1 graph1 = deleteRelationship orig target g
        Tuple ins2 graph2 = addRelationship new target graph1
    in if null ins2
       then Tuple [] g -- If add fails, that means there is cycle
       else Tuple (ins1 <> ins2) graph2

  swap old new (Graph g)
    | Array.elem new g.nodes = Graph g
    | otherwise =
        let mNewNodes = do
              index <- Array.elemIndex old g.nodes
              Array.updateAt index new g.nodes
            newRelationships = Set.map mapRelFn g.relationships
        in case mNewNodes of Nothing -> Graph g
                             Just newNodes -> Graph { nodes: newNodes, relationships: newRelationships }
    where mapRelFn r@(Relationship f t)
            | f == old = Relationship new t
            | t == old = Relationship f new
            | otherwise = r

instance relBGraph :: (RelGraph g)=> RelGraph (WithBody v g) where
  addRelationship from to = modify innerL (addRelationship from to)
  deleteRelationship from to = modify innerL (deleteRelationship from to)
  switchParent orig new target = modify innerL (switchParent orig new target)
  swap old new (WithBody { bodies, graph }) = WithBody { bodies: newBodies, graph: newGraph }
    where newGraph = swap old new graph
          newBodies = let v = Map.lookup old bodies
                      in fromMaybe bodies $ Map.insert new <$> v <*> (pure bodies)

instance relTGraph :: (IsGraph g, RelGraph g, RawGraph g)=> RelGraph (WithForest g) where
  addRelationship from to = rmap reloadTree <<< modify innerL (addRelationship from to)
  deleteRelationship from to = rmap reloadTree <<< modify innerL (deleteRelationship from to)
  switchParent orig new target = rmap reloadTree <<< modify innerL (switchParent orig new target)
  swap old new = reloadTree <<< (innerL %~ swap old new)

instance idRelGraph :: (RelGraph g)=> RelGraph (WithId gk g) where
  addRelationship from to = modify innerL (addRelationship from to)
  deleteRelationship from to = modify innerL (deleteRelationship from to)
  switchParent orig new target = modify innerL (switchParent orig new target)
  swap old new = innerL %~ swap old new

class ForestGraph g where
  reloadTree :: forall a. Ord a=> g a -> g a
  move :: forall a v
       .  Ord a
       => Path a -- Move from path
       -> Path a -- to path
       -> Maybe a -- optional ordering, after this element, Nothing = as the first child
       -> g a
       -> InsG v g a
  shiftUp :: forall a v. Ord a=> Path a -> g a -> Tuple (InsG v g a) (Maybe (Path a))
  shiftDown :: forall a v. Ord a => Path a -> g a -> Tuple (InsG v g a) (Maybe (Path a))
  getForest :: forall a. g a -> Forest a

instance treeGraph :: (IsGraph g, RelGraph g, RawGraph g)=> ForestGraph (WithForest g) where
  reloadTree tg@(WithForest r) =
    let (Graph graph) = tg ^. graphL
        forest = toForest graph.nodes graph.relationships
    in  WithForest r { forest = forest }

  move p1 p2 o tg@(WithForest { forest })
    | Just lastPath <- Array.last p1
    , Just lastPath2 <- Array.last p2 =
        let p@{order} = Tree.move p1 p2 o forest
            Tuple ins newGraph = applyDiff p tg
            reordered = applyOrder lastPath2 order newGraph
        in  Tuple ((Reorder lastPath order) : ins) reordered
    | otherwise = Tuple [] tg

  shiftUp path g =
    let diff = Tree.shiftUp path
    in  Tuple (applyDiff diff g) diff.newPath

  shiftDown path tg@(WithForest { forest }) =
    let diff = Tree.shiftDown forest path
    in  Tuple (applyDiff diff tg) diff.newPath

  getForest (WithForest { forest }) = forest


instance treeWithId :: (ForestGraph g)=> ForestGraph (WithId gk g) where
  reloadTree = innerL %~ reloadTree
  move p1 p2 o = modify innerL (move p1 p2 o)
  shiftUp path g = let Tuple (Tuple ins newG) r = shiftUp path $ g ^. innerL
                   in Tuple (Tuple ins (g # innerL .~ newG)) r
  shiftDown path g = let Tuple (Tuple ins newG) r = shiftDown path $ g ^. innerL
                     in Tuple (Tuple ins (g # innerL .~ newG)) r
  getForest = getForest <<< view innerL

instance selectedGraph :: ForestGraph g => ForestGraph (SelectedA g) where
  reloadTree = innerL %~ reloadTree
  move p1 p2 o (SelectedA { selected, graph }) =
    let Tuple ins moved = move p1 p2 o graph
        newSelected = if Just p1 == selected then Just p2 else selected
    in Tuple ins $ SelectedA { selected: newSelected, graph: moved }

  shiftUp path (SelectedA { selected, graph }) =
    let (Tuple (Tuple ins newGraph) newPath) = shiftUp path graph
        newSelected = if selected == Just path then newPath <|> selected else selected
    in Tuple (Tuple ins $ SelectedA { selected: newSelected, graph: newGraph }) newPath

  shiftDown path (SelectedA { selected, graph }) =
    let (Tuple (Tuple ins newGraph) newPath) = shiftDown path graph
        newSelected = if selected == Just path then newPath <|> selected else selected
    in Tuple (Tuple ins $ SelectedA { selected: newSelected, graph: newGraph }) newPath

  getForest = getForest <<< view innerL

instance bodyForest :: (IsGraph g, BodyGraph g v, RelGraph g, RawGraph g)=> BodyGraph (WithForest g) v where
  addBody k v = reloadTree <<< over innerL (addBody k v)
  getBody k = getBody k <<< view innerL
  setBody k v = rmap reloadTree <<< modify innerL (setBody k v)
  deleteBody k = rmap reloadTree <<< modify innerL (deleteBody k)

class SelectAGraph g where
  selectedL :: forall k. Lens' (g k) (Maybe (Array k))

instance selectGraph :: SelectAGraph (SelectedA g) where
  selectedL = lens (\(SelectedA { selected })-> selected) (\(SelectedA r) s-> SelectedA r {selected = s})

instance selectForest :: (SelectAGraph g) => SelectAGraph (WithForest g) where
  selectedL = innerL <<< selectedL

instance selectWithId :: (SelectAGraph g) => SelectAGraph (WithId gk g) where
  selectedL = innerL <<< selectedL

instance rawSelect :: RawGraph g => RawGraph (SelectedA g) where
  addNode = over innerL <<< addNode
  deleteNode n (SelectedA { selected, graph }) = SelectedA { selected: newSelected, graph: deleteNode n graph }
    where isDeletingSelected = (Array.last =<< selected) == Just n
          newSelected = if isDeletingSelected then Nothing else selected
  children a = children a <<< view innerL
  parents a = parents a <<< view innerL
  pathExist from to = pathExist from to <<< view innerL
  applyOrder k = over innerL <<< applyOrder k

instance bodySelect :: BodyGraph g v => BodyGraph (SelectedA g) v where
  addBody k = over innerL <<< addBody k
  getBody k = getBody k <<< view innerL
  setBody k = modify innerL <<< setBody k
  deleteBody k (SelectedA { selected, graph }) =
   let Tuple ins g = deleteBody k graph
   in Tuple ins $ if (Array.last =<< selected) == Just k
                  then SelectedA { selected: Nothing, graph: g }
                  else SelectedA { selected, graph: g }

instance relSGraph :: (RelGraph g)=> RelGraph (SelectedA g) where
  addRelationship from to = modify innerL (addRelationship from to)
  deleteRelationship from to = modify innerL (deleteRelationship from to)
  switchParent orig new target = modify innerL (switchParent orig new target)
  swap old new (SelectedA { selected, graph }) = SelectedA { selected: newSelected, graph: newGraph }
    where newGraph = swap old new graph
          newSelected = map (map (\id-> if id == old then new else id)) selected

class IdGraph g gk | g -> gk where
  idL :: forall k . Lens' (g k) gk

instance withIdGraph :: IdGraph (WithId gk g) gk where
  idL = lens (\(WithId { id })-> id) (\(WithId r) id -> WithId r {id = id})

delete
  :: forall v g k
  .  Ord k
  => RawGraph g
  => BodyGraph g v
  => RelGraph g
  => k
  -> DependsOnMigration k
  -> g k
  -> InsG v g k
delete key childrenHandling = runInsM_ $ do
  graph <- I.read
  let childrenIds = children key graph
      descendantIds = descendant key graph
      parentIds = key : descendantIds
      orphans = Array.filter (\id-> Array.null $ Array.filter (flip Array.elem parentIds) $ parents id graph) descendantIds

  -- Delete body defaults to relationship only, we need to override the migration ourself
  insM $ overrideIns <<< deleteBody key

  -- Since the migration is override, we don't want these Ins to be in our InsG
  I.clearIns $ case childrenHandling of
       DeleteOrphans -> for_ orphans $ insM <<< deleteBody
       BecomeDependsOn newParent -> for_ childrenIds $ insM <<< addRelationship newParent
       DeleteRelationships -> pure unit
  where overrideIns = lmap (map replaceDelMigration)
        replaceDelMigration (DeleteBody k _)
          | k == key = DeleteBody k childrenHandling
        replaceDelMigration ins = ins

applyCreate :: forall r v g a. (RelGraph g)=> (Ord a)=> ({create :: Maybe (Relationship a a) | r}) -> g a -> InsG v g a
applyCreate {create: Nothing} = Tuple []
applyCreate {create: Just (Relationship from to)} = addRelationship from to

applyDelete :: forall r v g a. (RelGraph g)=> (Ord a)=> {delete :: Maybe (Relationship a a) | r} -> g a -> InsG v g a
applyDelete {delete: Nothing} = Tuple []
applyDelete {delete: Just (Relationship from to)} = deleteRelationship from to

applyDiff :: forall r v g a. (RelGraph g)=> (Ord a)=> Diff r a -> g a -> InsG v g a
applyDiff p g =
  let Tuple ins1 g1 = applyDelete p g
      Tuple ins2 g2 = applyCreate p g1
  in if null ins2 && isJust p.create
     then Tuple [] g -- Error when adding = cycle in graph
     else Tuple (ins1 <> ins2) g2

setOrderedByArray :: forall k. Ord k => Array k -> Set k -> Array k
setOrderedByArray arr s = Array.filter (\a-> Set.member a s) arr

nudge :: forall v g k. k -> g k -> InsG v g k
nudge = Tuple <<< Array.singleton <<< Nudge

-- This does not follow functor law
-- Because relationships is a Set
class MapKey g where
  mapKey :: forall a b. Ord b=> (a -> b) -> g a -> g b

instance graphMap :: MapKey Graph where
  mapKey fn (Graph { nodes, relationships }) = Graph
    { nodes: fn <$> nodes
    , relationships: Set.map (bimap fn fn) relationships }

instance bodyGraphMap :: MapKey g => MapKey (WithBody v g) where
  mapKey fn (WithBody { bodies, graph }) = WithBody
    { bodies: let arr = Map.toUnfoldable bodies :: Array _
              in Map.fromFoldable $ map (lmap fn) arr
    , graph: mapKey fn graph
    }

instance forestGraphMap :: (IsGraph g, RelGraph g, MapKey g, RawGraph g) => MapKey (WithForest g) where
  mapKey fn (WithForest { forest, graph }) = reloadTree $ WithForest
    { graph: mapKey fn graph
    , forest: []
    }

instance selectGraphMap :: (MapKey g)=> MapKey (SelectedA g) where
  mapKey fn (SelectedA { selected, graph }) = SelectedA
    { graph: mapKey fn graph
    , selected: (map fn) <$> selected
    }

instance idGraphMap :: (MapKey g)=> MapKey (WithId gk g) where
  mapKey fn (WithId { id, graph }) = WithId
    { id
    , graph: mapKey fn graph
    }
