module Data.Cookie (getCookieWithKey) where

import Prelude

import Data.Foldable (findMap)
import Data.Maybe (Maybe(..))
import Data.String (Pattern(..), split)
import Data.String.Common (trim)
import Effect (Effect)

foreign import getCookieString :: Effect String

getCookieWithKey :: String -> Effect (Maybe String)
getCookieWithKey key = getCookieString
                   <#> split (Pattern ";")
                   <#> (map $ split (Pattern "="))
                   <#> findMap case _ of
                                 [k, value] | (trim k) == key -> Just value
                                 _ -> Nothing
