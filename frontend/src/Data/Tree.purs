module Data.Tree where

import Prelude

import Control.Comonad.Cofree (Cofree, head, mkCofree, tail)
import Control.Monad.State (State, evalState, get, modify_)
import Data.Array (deleteAt, difference, filter, findIndex, findMap, length, (!!), (:))
import Data.Array as Array
import Data.Foldable (any, find)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Eq (genericEq)
import Data.Generic.Rep.Ord (genericCompare)
import Data.Generic.Rep.Show (genericShow)
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Set (Set)
import Data.Set as Set
import Data.Traversable (traverse)
import Model.Relationship (Relationship(..))


type Tree a = Cofree Array a
type Forest a = Array (Tree a)
type Path a = Array a

data Order a = Before a
             | After a
             | NoOrder
type Diff r a = { create :: Maybe (Relationship a a), delete :: Maybe (Relationship a a) | r }

data DependsOnMigration a = DeleteRelationships
                          | DeleteOrphans
                          | BecomeDependsOn a

derive instance genericOrder :: Generic (Order a) _
instance eqOrder :: Eq a => Eq (Order a) where eq = genericEq
instance ordOrder :: Ord a => Ord (Order a) where compare = genericCompare
instance showOrder :: Show a => Show (Order a) where show = genericShow
instance functorOrder :: Functor Order where
  map fn (Before a) = Before $ fn a
  map fn (After a)  = After $ fn a
  map _  NoOrder    = NoOrder

derive instance genericMigration :: Generic (DependsOnMigration a) _
instance eqMigration :: Eq a => Eq (DependsOnMigration a) where eq = genericEq
instance showMigration :: Show a => Show (DependsOnMigration a) where show = genericShow
instance functorMigration :: Functor DependsOnMigration where
  map fn DeleteRelationships = DeleteRelationships
  map fn DeleteOrphans = DeleteOrphans
  map fn (BecomeDependsOn a) = BecomeDependsOn $ fn a

toForest :: forall a. Ord a => Array a -> Set (Relationship a a) -> Forest a
toForest nodes relationships = toTree <$> topLevels
  where toTree :: a -> Tree a
        toTree node = let childrenIds = filter (isChild node) nodes
                          childrenTrees = toTree <$> childrenIds
                        in mkCofree node childrenTrees
        isChild a b = Set.member (Relationship a b) relationships
        topLevels = nodes `difference` ((\(Relationship _ to)-> to) <$> (Set.toUnfoldable relationships))

move :: forall a
     .  Eq a
     => Path a -- Move from path
     -> Path a -- to path
     -> Maybe a -- optional ordering, after this element, Nothing = as the first child
     -> Forest a
     -> Diff (order :: Order a) a
move from to afterThis forest =
  let d = lastRelationship from
      c = lastRelationship to
  in if d == c then { delete: Nothing, create: Nothing, order: fromMaybe NoOrder $ order' forest to }
               else { delete: d,       create: c,       order: fromMaybe NoOrder $ order' forest to }
  where lastRelationship path
          | length path <= 1 = Nothing
          | otherwise = Relationship <$> (path !! (length path - 2)) <*> (path !! (length path - 1))

        order' forest' to'
          | length to' == 0 = Nothing
          | length to' == 1 = case afterThis of
                                  Nothing -> do
                                    let result = Before <$> head <$> Array.head forest'
                                        afterFrom = Array.last =<< nextInForest forest' from
                                        fromLast = Array.last from
                                    case {result, fromLast, afterFrom} of
                                      -- It's trying to move before itself, which means no moving
                                      {result: Just (Before node), fromLast: Just node'} | node == node' -> Nothing
                                      -- It's trying to move before the next item of itself, which means no moving
                                      {result: Just (Before node), afterFrom: Just af } | node == af -> Nothing
                                      _ -> result
                                  Just x -> if any ((==) x <<< head) forest'
                                                then do
                                                  let fromLast = Array.last from
                                                      prevFrom = Array.last =<< prevInForest forest' from
                                                  case {fromLast, prevFrom} of
                                                    -- It's trying to move after itself, which means no moving
                                                    {fromLast: Just node} | node == x -> Nothing
                                                    -- It's trying to move after the prev node, which means no moving
                                                    {prevFrom: Just node} | node == x -> Nothing
                                                    _ -> Just (After x)
                                                else Nothing
          | length to' == 2 = do first <- Array.head to'
                                 rest <- Array.tail to'
                                 firstTree <- find ((==) first <<< head) forest'
                                 if length (tail firstTree) == 0
                                     then pure $ After first
                                     else order' (tail firstTree) rest
          | otherwise = do first <- Array.head to'
                           rest <- Array.tail to'
                           child <- find ((==) first <<< head) forest'
                           order' (tail child) rest

shiftUp :: forall a. Path a -> Diff (newPath :: Maybe (Path a)) a
shiftUp path = { delete: do last <- path !! (length path - 1)
                            secondLast <- path !! (length path - 2)
                            pure $ Relationship secondLast last
               , create: do last <- path !! (length path - 1)
                            thirdLast <- path !! (length path - 3)
                            pure $ Relationship thirdLast last
               , newPath: let len = length path
                          in if len <= 1 then Nothing else deleteAt (len - 2) path
               }

shiftDown :: forall a. Eq a => Forest a -> Path a -> Diff (newPath :: Maybe (Path a)) a
shiftDown forest path
  | length path == 0 = { delete: Nothing, create: Nothing, newPath: Nothing }
  | length path == 1 = { delete: Nothing
                       , create: do currentValue <- Array.head path
                                    index <- findIndex ((==) currentValue <<< head) forest
                                    prev <- if index == 0 then Nothing
                                                          else head <$> forest !! (index - 1)
                                    pure $ Relationship prev currentValue
                       , newPath: do currentValue <- Array.head path
                                     index <- findIndex ((==) currentValue <<< head) forest
                                     prev <- if index == 0 then Nothing
                                             else head <$> forest !! (index - 1)
                                     pure $ [prev, currentValue]
                       }

  | length path == 2 = { delete: do currentPath <- Array.head path
                                    lastPath <- path !! 1
                                    currentNode <- find ((==) currentPath <<< head) forest
                                    subIndex <- findIndex ((==) lastPath <<< head) (tail currentNode)
                                    if subIndex /= 0
                                        then pure $ Relationship currentPath lastPath
                                        else Nothing

                       , create: do currentPath <- Array.head path
                                    pathRest <- Array.tail path
                                    index <- findIndex ((==) currentPath <<< head) forest
                                    currentNode <- forest !! index
                                    (shiftDown (tail currentNode) pathRest).create
                       , newPath: do currentPath <- Array.head path
                                     pathRest <- Array.tail path
                                     index <- findIndex ((==) currentPath <<< head) forest
                                     currentNode <- forest !! index
                                     childPath <- (shiftDown (tail currentNode) pathRest).newPath
                                     pure $ Array.cons currentPath childPath
                       }
  | otherwise = let nested = do currentPath <- Array.head path
                                pathRest <- Array.tail path
                                index <- findIndex ((==) currentPath <<< head) forest
                                currentNode <- forest !! index
                                let child = shiftDown (tail currentNode) pathRest
                                pure $ child { newPath = Array.cons currentPath <$> child.newPath }
                in fromMaybe { delete: Nothing, create: Nothing, newPath: Nothing } nested

pathExistsInForest :: forall a. Eq a => Path a -> Forest a -> Boolean
pathExistsInForest path forest = case Array.uncons path of
  Nothing -> false
  Just { head: h, tail: rest }->
    let mTree = Array.find ((==) h <<< head) forest
    in case mTree of
      Nothing -> false
      Just tree -> if rest == []
                   then true
                   else pathExistsInForest rest (tail tree)

nextInForest :: forall a. Eq a => Forest a -> Path a -> Maybe (Path a)
nextInForest forest path
  | length path == 1 = do first <- Array.head path
                          index <- findIndex ((==) first <<< head) forest
                          node <- forest !! index
                          case Array.head (tail node) of
                            Just c -> Just [first, head c]
                            Nothing -> Array.singleton <$> head <$> forest !! (index + 1)
  | otherwise = do first <- Array.head path
                   rest <- Array.tail path
                   index <- findIndex ((==) first <<< head) forest
                   node <- forest !! index
                   case nextInForest (tail node) rest of
                     Just c -> Just $ Array.cons first c
                     Nothing -> Array.singleton <$> head <$> forest !! (index + 1)

prevInForest :: forall a. Eq a => Forest a -> Path a -> Maybe (Path a)
prevInForest forest path
  | length path == 1 = do first <- Array.head path
                          index <- findIndex ((==) first <<< head) forest
                          lastOfTree <$> forest !! (index - 1)
    where lastOfTree :: Tree a -> Array a
          lastOfTree tree = case Array.last (tail tree) of
                                Just t -> Array.cons (head tree) (lastOfTree t)
                                Nothing -> [head tree]

  | otherwise = do first <- Array.head path
                   rest <- Array.tail path
                   node <- find ((==) first <<< head) forest
                   case prevInForest (tail node) rest of
                     Just c -> Just $ Array.cons first c
                     Nothing -> Just [first]

closedForest :: forall a. Ord a => Set (Array a) -> Forest a -> Forest a
closedForest closedPaths forest = (closedTree closedPaths) <$> forest
  where closedTree paths tree = mkCofree (head tree) (treeTail paths tree)
        treeTail paths tree | Set.member [head tree] paths = []
                            | otherwise = closedForest (childPaths (head tree) paths) (tail tree)
        childPaths prefix = Set.map (Array.drop 1)
                            <<< Set.filter (not <<< Array.null)
                            <<< Set.filter ((==) (Just prefix) <<< Array.head)

removeDuplicates :: forall a. Eq a => Ord a => Forest a -> Forest a
removeDuplicates trees = evalState (traverse removeDuplicates' trees) Set.empty
  where removeDuplicates' :: Tree a -> State (Set a) (Tree a)
        removeDuplicates' tree = do
          skipping <- get
          let parent = head tree
              subTrees = filter (not <<< flip Set.member skipping <<< head) $ tail tree
          modify_ (Set.union $ Set.fromFoldable $ head <$> subTrees)
          newSubtrees <- traverse removeDuplicates' subTrees
          pure $ mkCofree parent newSubtrees

applyOrderInArray :: forall k. Eq k => k -> Order k -> Array k -> Array k
applyOrderInArray key order arr = fromMaybe arr $ do
  index <- case order of
               NoOrder -> Nothing
               After x -> ((+) 1) <$> (Array.elemIndex x arr)
               Before x -> Array.elemIndex x arr
  currIndex <- Array.elemIndex key arr
  if index == currIndex
     then Nothing
     else if index < currIndex
     then (Array.insertAt index key) =<< (Array.deleteAt currIndex arr)
     else (Array.insertAt index key arr) >>= (Array.deleteAt currIndex)

isValidTreePath :: forall k. Eq k => Path k -> Tree k -> Boolean
isValidTreePath path tree =
  case Array.uncons path of
    Nothing -> false
    Just { head: h, tail: rest }
      | h == head tree -> isValidForestPath rest (tail tree)
      | otherwise -> false

isValidForestPath :: forall k. Eq k => Path k -> Forest k -> Boolean
isValidForestPath = any <<< isValidTreePath

firstPathWithLeaf :: forall k. Eq k => k -> Forest k -> Maybe (Path k)
firstPathWithLeaf key forest =
  if any ((==) key) (head <$> forest)
  then Just [key]
  else findMap findLeaf forest
  where findLeaf tree = ((:) (head tree)) <$> (firstPathWithLeaf key $ tail tree)
