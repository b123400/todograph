module Data.GraphManager where

import Prelude

import Data.Array as Array
import Data.Default (class Default, default)
import Data.Ins (Ins, InsG)
import Data.Maybe (Maybe(..), maybe)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Ref (Ref)
import Effect.Ref as Ref
import IxQueue as Q
import Queue.Types (readOnly)

newtype GraphManager v g a = GraphManager
  { graphRef :: Ref (g a)
  , queue :: Q.IxQueue (read :: Q.READ, write :: Q.WRITE) (Array (Ins v a))
  }

create :: forall v g a. (Default (g a))=> Effect (GraphManager v g a)
create = do
  graphRef <- Ref.new default
  queue <- Q.new
  pure $ GraphManager { graphRef, queue }

subscribe :: forall v g a. GraphManager v g a -> Q.IxQueue (read :: Q.READ) (Array (Ins v a))
subscribe (GraphManager { queue }) = readOnly queue

readGraph :: forall v g a. GraphManager v g a -> Effect (g a)
readGraph (GraphManager { graphRef }) = Ref.read graphRef

update :: forall v g a. InsG v g a -> GraphManager v g a -> Effect Unit
update ins = updateWithId ins Nothing

-- Functions with "WithId" means the message will be broadcasted
-- to all handlers except for the source
updateWithId :: forall v g a. InsG v g a -> Maybe String -> GraphManager v g a -> Effect Unit
updateWithId (Tuple ins g) mSrc (GraphManager { graphRef, queue }) = do
  Ref.write g graphRef
  Q.broadcastExcept queue (maybe [] Array.singleton mSrc) ins

update' :: forall v g a. g a -> GraphManager v g a -> Effect Unit
update' = update <<< Tuple []

modifyWithIdV :: forall v g k r. Maybe String -> GraphManager v g k -> (g k -> Tuple (InsG v g k) r) -> Effect r
modifyWithIdV src gm modFn = do
  gk <- readGraph gm
  let Tuple ins res = modFn gk
  updateWithId ins src gm
  pure res

modifyV :: forall v g k r. GraphManager v g k -> (g k -> Tuple (InsG v g k) r) -> Effect r
modifyV = modifyWithIdV Nothing

modifyWithId :: forall v g k. Maybe String -> GraphManager v g k -> (g k -> InsG v g k) -> Effect Unit
modifyWithId src gm = modifyWithIdV src gm <<< map (flip Tuple unit)

modify :: forall v g k. GraphManager v g k -> (g k -> InsG v g k) -> Effect Unit
modify = modifyWithId Nothing

modify' :: forall v g k. GraphManager v g k -> (g k -> g k) -> Effect Unit
modify' gm = modify gm <<< map (Tuple [])
