module Data.DateTime.DateTimeLocal where

import Prelude

import Data.DateTime (DateTime)
import Data.Formatter.DateTime (FormatterCommand(..), format)
import Data.List (List(..), (:))

toDateTimeLocalString :: DateTime -> String
toDateTimeLocalString = format $ YearFull
                               : Placeholder "-"
                               : MonthTwoDigits
                               : Placeholder "-"
                               : DayOfMonthTwoDigits
                               : Placeholder "T"
                               : Hours24
                               : Placeholder ":"
                               : MinutesTwoDigits
                               : Placeholder ":"
                               : SecondsTwoDigits
                               : Placeholder "."
                               : MillisecondsTwoDigits
                               : Nil
