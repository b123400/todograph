module Data.String.Random where

import Prelude

import Data.Array (catMaybes, (..))
import Data.Char (fromCharCode)
import Data.Int (round, toNumber)
import Data.String.CodeUnits (fromCharArray)
import Data.Traversable (sequence)
import Effect (Effect)
import Effect.Random (randomRange)


randomString :: Effect String
randomString = fromCharArray <$> catMaybes <$> (map (fromCharCode <<< round)) <$> (sequence $ (randomRange (toNumber 65) (toNumber 122)) <$ (0..31))
