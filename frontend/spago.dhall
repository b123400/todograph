{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
, name =
    "frontend"
, dependencies =
    [ "affjax"
    , "argonaut"
    , "console"
    , "datetime"
    , "datetime-iso"
    , "debug"
    , "effect"
    , "foreign-generic"
    , "formatters"
    , "free"
    , "generics-rep"
    , "halogen"
    , "ordered-collections"
    , "prelude"
    , "psci-support"
    , "queue"
    , "random"
    , "read"
    , "routing"
    , "web-html"
    ]
, packages =
    ./packages.dhall
}
