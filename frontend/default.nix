let pkgs = import <nixpkgs> {};
    spagoPkgs = import ./spago-packages.nix { inherit pkgs; };

    easyPS = import (pkgs.fetchFromGitHub {
      owner = "justinwoo";
      repo = "easy-purescript-nix";
      rev = "7ff5a12af5750f94d0480059dba0ba6b82c6c452"; # This is the last commit for Purescript 0.13.8
      sha256 = "0af25dqhs13ii4mx9jjkx2pww4ddbs741vb5gfc5ckxb084d69fq";
    }) { inherit pkgs; };

    src = ./.;
    projectOut = spagoPkgs.mkBuildProjectOutput {
      inherit src;
      purs = easyPS.purs;
    };
in
pkgs.stdenv.mkDerivation rec {
  name = "todograph";
  version = "v1.0.0";
  inherit src;
  buildInputs = [
    easyPS.purs
  ];
  buildPhase =
  ''
    purs bundle '${projectOut}/output/**/*.js' -m Main --main Main -o ./bundle.js
  '';
  installPhase = ''
    mkdir -p $out
    mv ./bundle.js $out
  '';
}
