{-# LANGUAGE PackageImports #-}
import "todograph" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
