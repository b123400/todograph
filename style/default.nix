{
  pkgs ? import <nixpkgs> { inherit system; },
  system ? builtins.currentSystem,
  nodejs ? pkgs."nodejs-10_x"
}:

let
  nodeEnv = (import ./package.nix { inherit system pkgs nodejs; }).package;
in
pkgs.stdenv.mkDerivation rec {
  name = "todograph-style";
  version = "v1.0.0";
  src = ./.;
  buildInputs = [ nodeEnv ];
  BASE_DIR="${nodeEnv}/lib/node_modules/todograph/";
  buildPhase =
  ''
    echo ${nodeEnv}
    mkdir -p $out
    $BASE_DIR/node_modules/.bin/stylus $BASE_DIR/index.styl -o ./index.css
  '';
  installPhase = ''
    mkdir -p $out
    cp ./index.css $out
  '';
}
