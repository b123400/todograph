{
  pkgs ? import <nixpkgs> { inherit system; },
  system ? builtins.currentSystem,
  nodejs ? pkgs."nodejs-10_x",
  src ? ./.,
}:
let backend = (import ./nix/backend/default.nix { inherit pkgs src; });
    # frontend = import ./frontend/default.nix;
    # style = import ./style/default.nix { inherit pkgs system nodejs; };
in
pkgs.stdenv.mkDerivation {
  name = "todograph";
  version = "v1.0.0";
  inherit src;
  buildInputs = [
    #frontend
    backend
    #style
  ];
    # ln -s ${frontend}/bundle.js $out/static/js/bundle.js
    # ln -s ${style}/index.css $out/static/css/index.css
  installPhase = ''
    mkdir -p $out/bin
    cp -r . $out
    ln -s ${backend}/bin/todograph $out/bin/todograph
  '';
}
