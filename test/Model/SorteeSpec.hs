{-# LANGUAGE NoImplicitPrelude #-}
module Model.SorteeSpec where

import Data.List ((!!))
import TestImport
import Test.QuickCheck

import qualified Test.HUnit as HUnit

import Model.Sortee (chars, between, Sortee(..))

instance Arbitrary Sortee where
    arbitrary = sized $ \n-> do
                size <- choose (1, n)
                string <- forM [0..size] $ \_-> choose (0, length chars - 1)
                pure $ Sortee $ (chars !!) <$> string

assertResult :: Result -> IO ()
assertResult r = HUnit.assertBool "" (case r of (Success _ _ _ _ _ _) -> True; _ -> False)

spec :: Spec
spec = do
    describe "Sortee" $ do

        it "generates value in between" $ do
            result <- quickCheckResult $ \(a, b)->
                let (smaller :: Sortee, larger :: Sortee) = if a < b then (a, b) else (b, a)
                    answer = between smaller larger
                in if larger == (Sortee "0") || smaller == larger
                       then True
                       else maybe False (\x-> x > smaller && x < larger) answer
            assertResult result

        it "must generate values that can be further processed" $ do
            result <- quickCheckResult $ \(a, b)->
                let (smaller :: Sortee, larger :: Sortee) = if a < b then (a, b) else (b, a)
                    answer = between smaller larger
                in case answer of
                    Nothing -> True
                    Just ans ->
                        let withSmall = case between smaller ans of
                                            Just x -> x > smaller && x < ans
                                            Nothing -> False
                            withLarge = case between ans larger of
                                            Just x -> x > ans && x < larger
                                            Nothing -> False
                        in withSmall && withLarge
            assertResult result
