{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE InstanceSigs #-}

module Foundation where

import Import.NoFoundation  hiding (fromList)

import Control.Monad.Logger        (LogSource)


import Data.Pool                   (Pool)
import Control.Monad.Trans.Maybe   (runMaybeT, MaybeT(..))
import Network.Mail.Mime           (Address(..), Encoding(..), Mail(..), Part(..), PartContent(..), Disposition(..), emptyMail, renderSendMail)
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import Text.Hamlet                 (hamletFile)
import Text.Shakespeare.Text       (stext)
import Yesod.Auth                  (YesodAuth, AuthId, loginDest, logoutDest, authPlugins, Auth, maybeAuthId, getAuth, credsKey, authenticate, credsIdent, AuthenticationResult(..), Route(LoginR, LogoutR))
import Yesod.Auth.Email            (authEmail, YesodAuthEmail(..), AuthEmailId, afterPasswordRoute, addUnverified, sendVerifyEmail, getVerifyKey, setVerifyKey, getPassword, setPassword, getEmailCreds, EmailCreds(..), getEmail, verifyAccount, needOldPassword, loginLinkKey, registerR)
import Yesod.Auth.Message          (AuthMessage(InvalidEmailPass))

import Yesod.Core.Types            (Logger)
import Yesod.Default.Util          (addStaticContentExternal)

import ApiSubsite.Data

import qualified Yesod.Core.Unsafe as Unsafe
import qualified Data.CaseInsensitive as CI
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Lazy.Encoding
import qualified Data.Text as T

import Handler.Auth.Register as A
import Handler.Auth.Login as A
import Handler.Auth.SetPassword as A
import qualified Database.Operation as DB

import Data.Time                   (addUTCTime)

import Database.Bolt               (Pipe)
import Model.Entity                (Entity(..), (=.), (==.))
import Model.Graph                 (GraphId)
import Model.User                  (User(..), UserFields(..), UserId)


-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appStatic      :: Static -- ^ Settings for static file serving.
    , appHttpManager :: Manager
    , appLogger      :: Logger
    , boltPool       :: Pool Pipe
    , appApiSubsite  :: ApiSubsite
    }

data MenuItem = MenuItem
    { menuItemLabel :: Text
    , menuItemRoute :: Route App
    , menuItemAccessCallback :: Bool
    }

data MenuTypes
    = NavbarLeft MenuItem
    | NavbarRight MenuItem

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the following documentation
-- for an explanation for this split:
-- http://www.yesodweb.com/book/scaffolding-and-the-site-template#scaffolding-and-the-site-template_foundation_and_application_modules
--
-- This function also generates the following type synonyms:
-- type Handler = HandlerT App IO
-- type Widget = WidgetT App IO ()
mkYesodData "App" $(parseRoutesFile "config/routes")

instance DB.HasBoltPool App where
    boltPool = boltPool

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerFor App) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    -- Controls the base of generated URLs. For more information on modifying,
    -- see: https://github.com/yesodweb/yesod/wiki/Overriding-approot
    approot :: Approot App
    approot = ApprootRequest $ \app req ->
        case appRoot $ appSettings app of
            Nothing -> getApprootText guessApproot app req
            Just root -> root

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend :: App -> IO (Maybe SessionBackend)
    makeSessionBackend _ = Just <$> defaultClientSessionBackend
        43200    -- timeout in minutes = 30 days
        "config/client_session_key.aes"

    -- Yesod Middleware allows you to run code before and after each handler function.
    -- The defaultYesodMiddleware adds the response header "Vary: Accept, Accept-Language" and performs authorization checks.
    -- Some users may also want to add the defaultCsrfMiddleware, which:
    --   a) Sets a cookie with a CSRF token in it.
    --   b) Validates that incoming write requests include that token in either a header or POST parameter.
    -- To add it, chain it together with the defaultMiddleware: yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware
    -- For details, see the CSRF documentation in the Yesod.Core.Handler module of the yesod-core package.
    yesodMiddleware :: ToTypedContent res => Handler res -> Handler res
    yesodMiddleware = defaultYesodMiddleware . defaultCsrfMiddleware

    defaultLayout :: Widget -> Handler Html
    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage

        -- mcurrentRoute <- getCurrentRoute

        -- Get the breadcrumbs, as defined in the YesodBreadcrumbs instance.
        -- (title, parents) <- breadcrumbs

        -- Define the menu items of the header.
        let menuItems =
                [ NavbarLeft $ MenuItem
                    { menuItemLabel = "Home"
                    , menuItemRoute = HomeR
                    , menuItemAccessCallback = True
                    }
                ]

        let navbarLeftMenuItems = [x | NavbarLeft x <- menuItems]
        let navbarRightMenuItems = [x | NavbarRight x <- menuItems]

        let navbarLeftFilteredMenuItems = [x | x <- navbarLeftMenuItems, menuItemAccessCallback x]
        let navbarRightFilteredMenuItems = [x | x <- navbarRightMenuItems, menuItemAccessCallback x]

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        mAuthId <- maybeAuthId
        pc <- widgetToPageContent $ do
            $(widgetFile "default-layout")
        withUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    isAuthorized
        :: Route App  -- ^ The route the user is visiting.
        -> Bool       -- ^ Whether or not this is a "write" request.
        -> Handler AuthResult
    -- Routes not requiring authenitcation.
    isAuthorized FaviconR _ = return Authorized
    isAuthorized RobotsR _ = return Authorized
    -- Default to Authorized for now.
    isAuthorized _ _ = return Authorized

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent
        :: Text  -- ^ The file extension
        -> Text -- ^ The MIME content type
        -> LByteString -- ^ The contents of the file
        -> Handler (Maybe (Either Text (Route App, [(Text, Text)])))
    addStaticContent ext mime content = do
        master <- getYesod
        let staticDir = appStaticDir $ appSettings master
        addStaticContentExternal
            Right -- No minify
            genFileName
            staticDir
            (StaticR . flip StaticRoute [])
            ext
            mime
            content
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs = "autogen-" ++ base64md5 lbs

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLogIO :: App -> LogSource -> LogLevel -> IO Bool
    shouldLogIO app _source level =
        return $
        appShouldLogAll (appSettings app)
            || level == LevelWarn
            || level == LevelError

    makeLogger :: App -> IO Logger
    makeLogger = return . appLogger

-- Define breadcrumbs.
instance YesodBreadcrumbs App where
    -- Takes the route that the user is currently on, and returns a tuple
    -- of the 'Text' that you want the label to display, and a previous
    -- breadcrumb route.
    breadcrumb
        :: Route App  -- ^ The route the user is visiting currently.
        -> Handler (Text, Maybe (Route App))
    breadcrumb HomeR = return ("Home", Nothing)
    breadcrumb  _ = return ("home", Nothing)

instance YesodAuth App where
    type AuthId App = UserId

    loginDest _ = ApplicationR
    logoutDest _ = HomeR
    authPlugins _ = [authEmail]

    -- Need to find the UserId for the given email address.
    authenticate creds = liftHandler $ DB.withBoltPipe $ \pipe-> do
        users <- DB.getWhere pipe $ [UserEmail ==. credsIdent creds]
        case users of
            (user:[]) -> return $ Authenticated $ entityId user
            _         -> return $ UserError InvalidEmailPass

    maybeAuthId = runMaybeT $ do
        session <- MaybeT $ lookupSession credsKey
        aid <- MaybeT $ return $ fromPathPiece session
        return aid


instance YesodAuthEmail App where
    type AuthEmailId App = UserId

    afterPasswordRoute _ = HomeR

    addUnverified email verkey = do
        when ("\"" `isInfixOf` email) $ invalidArgs ["email"]
        DB.withBoltPipe $ \pipe-> do
            DB.addUserWithoutPassword pipe (User { userEmail = email
                                                 , userVerkey = Just verkey
                                                 , userVerified = False
                                                 })

    sendVerifyEmail email _ verurl = do
        -- Print out to the console the verification email, for easier
        -- debugging.
        liftIO $ putStrLn $ "Copy/ Paste this URL in your browser:" <> verurl

        mailFrom <- getsYesod $ appMailFrom . appSettings

        -- Send email.
        liftIO $ renderSendMail (emptyMail $ Address Nothing mailFrom)
            { mailTo = [Address Nothing email]
            , mailHeaders =
                [ ("Subject", "Verify your email address")
                ]
            , mailParts = [[textPart, htmlPart]]
            }
      where
        textPart = Part
            { partType = "text/plain; charset=utf-8"
            , partEncoding = None
            , partDisposition = DefaultDisposition
            , partContent = PartContent $ Data.Text.Lazy.Encoding.encodeUtf8
                [stext|
                    Please confirm your email address by clicking on the link below.

                    #{verurl}

                    Thank you
                |]
            , partHeaders = []
            }
        htmlPart = Part
            { partType = "text/html; charset=utf-8"
            , partEncoding = None
            , partDisposition = DefaultDisposition
            , partContent = PartContent $ renderHtml
                [shamlet|
                    <p>Please confirm your email address by clicking on the link below.
                    <p>
                        <a href=#{verurl}>#{verurl}
                    <p>Thank you
                |]
            , partHeaders = []
            }
    getVerifyKey userId = DB.withBoltPipe $ \pipe-> (>>= userVerkey) <$> (fmap entityValue) <$> DB.get pipe userId

    verifyAccount userId = DB.withBoltPipe $ \pipe-> do
        (fmap entityId) <$> DB.update pipe userId [UserVerKey =. Nothing, UserVerified =. True]

    setVerifyKey userId key = DB.withBoltPipe $ \pipe-> do
        void $ DB.update pipe userId [UserVerKey =. Just key]

    getPassword userId = DB.withBoltPipe $ \pipe-> DB.getUserPassword pipe userId

    setPassword userId password = DB.withBoltPipe $ \pipe-> do
        DB.updateUserPassword pipe userId password

    needOldPassword aid' = do
        mkey <- lookupSession loginLinkKey
        case mkey >>= readMay . T.unpack of
            Just (aidT, time) | Just aid <- fromPathPiece aidT, toPathPiece (aid `asTypeOf` aid') == toPathPiece aid' -> do
                now <- liftIO getCurrentTime
                return $ addUTCTime (60 * 30) time <= now
            _ -> do
                return True

    getEmailCreds email = DB.withBoltPipe $ \pipe-> do
        muser <- listToMaybe <$> DB.getWhere pipe [UserEmail ==. email]
        case muser of
            Nothing -> pure Nothing
            Just (Entity userId user) -> do
                return $ Just EmailCreds { emailCredsId = userId
                     , emailCredsAuthId = Just userId
                     , emailCredsStatus = userVerified user
                     , emailCredsVerkey = userVerkey user
                     , emailCredsEmail = email
                     }

    getEmail userId = DB.withBoltPipe $ \pipe-> (fmap userEmail) <$> (fmap entityValue) <$> DB.get pipe userId

    registerHandler = A.registerHandler
    emailLoginHandler = A.loginHandler
    setPasswordHandler = A.setPasswordHandler

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage :: App -> [Lang] -> FormMessage -> Text
    renderMessage _ _ = defaultFormMessage

-- Useful when writing code that is re-usable outside of the Handler context.
-- An example is background jobs that send email.
-- This can also be useful for writing code that works across multiple Yesod applications.
instance HasHttpManager App where
    getHttpManager :: App -> Manager
    getHttpManager = appHttpManager

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger
