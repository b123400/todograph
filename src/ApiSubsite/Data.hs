{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies    #-}
module ApiSubsite.Data where

import Yesod
import Model.Graph (GraphId)
import Model.Task (TaskId)

-- Subsites have foundations just like master sites.
data ApiSubsite = ApiSubsite

type ApiHandler master = SubHandlerFor ApiSubsite master

-- We have a familiar analogue from mkYesod, with just one extra parameter.
-- We'll discuss that later.
mkYesodSubData "ApiSubsite" [parseRoutes|
/graphs GraphsR GET POST
/graphs/#GraphId/tasks GraphsTasksR GET POST
/tasks/#TaskId TasksR PUT
/tasks/#TaskId/reorder TasksReorderR PUT
/tasks/#TaskId/delete TasksDeleteR PUT
|]
