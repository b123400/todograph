{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
module ApiSubsite
    ( module ApiSubsite.Data
    , module ApiSubsite
    ) where

import ApiSubsite.Data
import Yesod
import Yesod.Auth (AuthId, YesodAuth)

import Model.User (UserId)
import Database.Operation (HasBoltPool)
import Handler.API.Graph (getGraphsR, postGraphsR)
import Handler.API.Task (postGraphsTasksR)
import Handler.API.Task.Get (getGraphsTasksR)
import Handler.API.Task.Update (putTasksR)
import Handler.API.Task.Delete (putTasksDeleteR)
import Handler.API.Task.Reorder (putTasksReorderR)


instance (Yesod master, YesodAuth master, AuthId master ~ UserId, HasBoltPool master) => YesodSubDispatch ApiSubsite master where
    yesodSubDispatch = $(mkYesodSubDispatch resourcesApiSubsite)
