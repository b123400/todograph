module Model.Graph where

import Prelude
import Data.Aeson (ToJSON, object, toJSON, (.=), FromJSON, parseJSON, withObject, (.:))
import Database.Bolt (exactMaybe, Value(T))
import Data.Bifunctor (first)
import Data.Bolt.Read (ReadBolt, readBolt)
import Data.Map ((!?))
import Data.Text (Text, unpack)
import Data.Typeable (Typeable)
import Yesod.Core.Content ( ToContent, toContent
                          , ToTypedContent, toTypedContent
                          , TypedContent(..)
                          , typeJson
                          )
import Yesod.Core.Dispatch (PathPiece, fromPathPiece, toPathPiece)

import Model.Entity (Entity(..), Key, Fields, fieldName, IsEntity, entityTypeName, ToBolt, toBolt)

newtype GraphId = GraphId Text deriving (Eq, Ord, Typeable)

instance Show GraphId where
    show (GraphId a) = unpack a

instance Read GraphId where
    readsPrec x str = (first GraphId) <$> readsPrec x str

instance PathPiece GraphId where
    fromPathPiece x = GraphId <$> fromPathPiece x
    toPathPiece (GraphId x) = toPathPiece x

instance ToJSON GraphId where
    toJSON (GraphId x) = toJSON x

instance ToBolt GraphId where
    toBolt (GraphId x) = T x

data Graph = Graph { graphName :: Text
                   } deriving (Show)

data GraphFields = GraphName Text

type instance Key Graph = GraphId

instance IsEntity Graph where
    entityTypeName = "Graph"

instance Fields Graph GraphFields where
    fieldName (GraphName _) = "name"

instance FromJSON Graph where
    parseJSON = withObject "Graph" $ \v -> Graph <$> v .: "name"

instance ToJSON (Entity Graph) where
    toJSON (Entity gid g) = object [ "id" .= gid
                                   , "name" .= (graphName g)
                                   ]

instance ToContent (Entity Graph) where
    toContent = toContent . toJSON

instance ToTypedContent (Entity Graph) where
    toTypedContent = TypedContent typeJson . toContent

instance ReadBolt (Entity Graph) where
    readBolt r = Entity <$> (GraphId <$> (exactMaybe =<< r !? "id"))
                        <*> (Graph <$> (exactMaybe =<< r !? "name"))
