module Model.Relationship where

import Data.Aeson (ToJSON, toJSON, object, (.=))
import Data.Text (Text)

import Model.Graph (GraphId)
import Model.Task (TaskId)
import Model.User (UserId)

type Path = [Relationship]

data Relationship = Access UserId GraphId
                  | Contain GraphId TaskId
                  | Depend TaskId TaskId
                  deriving (Eq)

instance ToJSON Relationship where
    toJSON (Depend a b) = object [ "taskId" .= toJSON a
                                 , "dependsOnTaskId" .= toJSON b
                                 ]
    toJSON (Contain a b) = object [ "graphId" .= toJSON a
                                  , "containsTaskId" .= toJSON b
                                  ]
    toJSON (Access a b) = object [ "userId" .= toJSON a
                                 , "canAccessGraphId" .= toJSON b
                                 ]

toBoltName :: Relationship -> Text
toBoltName (Access _ _) = "ACCESS"
toBoltName (Contain _ _) = "CONTAIN"
toBoltName (Depend _ _) = "DEPEND"
