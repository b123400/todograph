module Model.Task where

import ClassyPrelude (Text, tshow)
import Database.Bolt (Value(..), exactMaybe)
import Data.Aeson (ToJSON, Value(String), object, toJSON, (.=), FromJSON, parseJSON, withObject, withText, (.:), (.:?))
import Data.Aeson.Types (typeMismatch)
import Data.Bifunctor (first)
import Data.Bolt.Read (ReadBolt, readBolt, readBoltPrimitive)
import Data.Map ((!?))
import Data.Text (unpack)
import Data.Text.ReadText (ReadText, readText)
import Data.Time.Clock (UTCTime)
import Data.Time.Format.ISO8601 (iso8601Show)
import Data.Typeable (Typeable)
import Yesod.Core.Content ( ToContent, toContent
                          , ToTypedContent, toTypedContent
                          , TypedContent(..)
                          , typeJson
                          )
import Yesod.Core.Dispatch (PathPiece, fromPathPiece, toPathPiece)
import Model.Entity (Entity(..), Key, Fields, fieldName, IsEntity, entityTypeName, ToBolt, toBolt)
import Model.Sortee (Sortee(..), empty)

data TaskState = Pending
               | Done
               deriving (Eq)

instance ReadText TaskState where
    readText "pending" = Just Pending
    readText "done"    = Just Done
    readText _         = Nothing

instance Show TaskState where
    show Pending = "pending"
    show Done    = "done"

instance FromJSON TaskState where
    parseJSON = withText "TaskState" $ \v ->
        case readText v of
            Just t  -> pure t
            Nothing -> typeMismatch "TaskState" (String v)

instance ToJSON TaskState where
    toJSON = String . tshow

instance ToBolt TaskState where
    toBolt = T . tshow

newtype TaskId = TaskId Text deriving (Eq, Ord, Typeable)
data Task = Task { taskState :: TaskState
                 , taskBody :: Text
                 , taskDueDate :: Maybe UTCTime
                 , taskComment :: Text
                 , taskAutocomplete :: Bool
                 , taskSortee :: Sortee
                 } deriving (Show)

newtype TaskWithoutSortee = TaskWithoutSortee Task

data TaskFields = TaskState TaskState
                | TaskBody Text
                | TaskDueDate (Maybe UTCTime)
                | TaskComment Text
                | TaskAutocomplete Bool
                | TaskSortee Sortee

type instance Key Task = TaskId

instance IsEntity Task where
    entityTypeName = "Task"

instance Fields Task TaskFields where
    fieldName (TaskState _) = "state"
    fieldName (TaskBody _) = "body"
    fieldName (TaskDueDate _) = "dueDate"
    fieldName (TaskComment _) = "comment"
    fieldName (TaskAutocomplete _) = "autocomplete"
    fieldName (TaskSortee _) = "sortee"

instance ReadBolt (Entity Task) where
    readBolt r = Entity <$> (TaskId <$> (exactMaybe =<< r !? "id"))
                        <*> (Task <$> (readText =<< exactMaybe =<< r !? "state")
                                  <*> (exactMaybe =<< r !? "body")
                                  <*> (pure $ readBoltPrimitive =<< r !? "dueDate")
                                  <*> (exactMaybe =<< r !? "comment")
                                  <*> (exactMaybe =<< r !? "autocomplete")
                                  <*> (Sortee <$> unpack <$> (exactMaybe =<< r !? "sortee")))

instance Show TaskId where
    show (TaskId a) = unpack a

instance Read TaskId where
    readsPrec x str = (first TaskId) <$> readsPrec x str

instance PathPiece TaskId where
    fromPathPiece x = TaskId <$> fromPathPiece x
    toPathPiece (TaskId x) = toPathPiece x

instance ToBolt TaskId where
    toBolt (TaskId x) = T x

instance FromJSON TaskId where
    parseJSON = withText "TaskId" (pure . TaskId)

instance ToJSON TaskId where
    toJSON (TaskId x) = toJSON x

instance FromJSON TaskWithoutSortee where
    parseJSON = withObject "TaskWithoutSortee" $ \v ->
        TaskWithoutSortee <$> (Task <$> v .: "state"
                                    <*> v .: "body"
                                    <*> v .:? "dueDate"
                                    <*> v .: "comment"
                                    <*> v .: "autocomplete"
                                    <*> (pure empty))

instance ToJSON (Entity Task) where
    toJSON task = object $ [ "id"    .= (entityId task)
                           , "state" .= (toJSON $ taskState $ entityValue task)
                           , "body"  .= (taskBody $ entityValue task)
                           , "dueDate" .= (iso8601Show <$> (taskDueDate $ entityValue task))
                           , "comment" .= (taskComment $ entityValue task)
                           , "autocomplete" .= (taskAutocomplete $ entityValue task)
                           ]

instance ToContent (Entity Task) where
    toContent = toContent . toJSON

instance ToTypedContent (Entity Task) where
    toTypedContent = TypedContent typeJson . toContent

withSortee :: TaskWithoutSortee -> Sortee -> Task
withSortee (TaskWithoutSortee (Task state body dueDate comment autocomplete _)) s =
  Task state body dueDate comment autocomplete s
