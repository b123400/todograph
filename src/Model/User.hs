module Model.User where

import Prelude
import Data.Aeson (ToJSON, object, toJSON, (.=))
import Database.Bolt (exactMaybe, Value(T))
import Data.Bolt.Read (ReadBolt, readBolt)
import Data.Map ((!?))
import Data.Text (Text)
import Data.Typeable (Typeable)
import Yesod.Core.Dispatch (PathPiece, fromPathPiece, toPathPiece)

import Model.Entity (Entity(..), Key, Fields, fieldName, IsEntity, entityTypeName, ToBolt, toBolt)

newtype UserId = UserId Text deriving (Eq, Ord, Show, Typeable)
data User = User { userEmail :: Text
                 -- , userPassword :: Text <-- Make it not accessible from Haskell code at all
                 , userVerkey :: Maybe Text
                 , userVerified :: Bool
                 } deriving (Show)

data UserFields = UserEmail Text
                | UserVerKey (Maybe Text)
                | UserVerified Bool

type instance Key User = UserId

instance IsEntity User where
    entityTypeName = "User"

instance Fields User UserFields where
    fieldName (UserEmail _) = "email"
    fieldName (UserVerKey _) = "verkey"
    fieldName (UserVerified _) = "verified"

instance ToJSON UserId where
    toJSON (UserId x) = toJSON x

instance ToJSON (Entity User) where
    toJSON u = object [ "id" .= (entityId u)
                      ]

instance ReadBolt (Entity User) where
    readBolt r = Entity <$> (UserId <$> (exactMaybe =<< r !? "id"))
                        <*> (User <$> (exactMaybe =<< r !? "email")
                                  -- "password" field omitted
                                  <*> (maybe (Just Nothing) exactMaybe $ r !? "verkey")
                                  <*> (exactMaybe =<< r !? "verified"))

instance ToBolt UserId where
    toBolt (UserId x) = T x

instance PathPiece UserId where
    fromPathPiece x = UserId <$> fromPathPiece x
    toPathPiece (UserId x) = toPathPiece x
