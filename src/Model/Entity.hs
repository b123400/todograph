{-# LANGUAGE AllowAmbiguousTypes #-}

module Model.Entity where

import Data.Aeson (ToJSON, toJSON)
import Data.Text (Text, pack)
import Data.Time.Clock (UTCTime, NominalDiffTime, picosecondsToDiffTime)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import Database.Bolt (Value(..), Structure(..))
import Model.Sortee (Sortee(..))
import Yesod.Core.Content ( ToContent, toContent
                          , ToTypedContent, toTypedContent
                          , TypedContent(..)
                          , typeJson
                          )

data Entity x = IsEntity x => Entity { entityId :: Key x
                                     , entityValue :: x
                                     }

instance (Show x) => Show (Entity x) where
    show = show . entityValue

type family Key e = k | k -> e

class IsEntity a where
    entityTypeName :: Text

class IsEntity e => Fields e f | e -> f, f -> e where
    fieldName :: f -> Text

data Pair f = forall a e. (Fields e f, ToBolt a) => Pair (a -> f) a

(=.) :: forall f a e. (Fields e f, ToBolt a)=> (a -> f) -> a -> Pair f
(=.) = Pair @f @a @e

data QueryPair f = forall a e. (Fields e f, ToBolt a) => Equal (a -> f) a

(==.) :: forall f a e. (Fields e f, ToBolt a)=> (a -> f) -> a -> QueryPair f
(==.) = Equal @f @a @e

class ToBolt a where
    toBolt :: a -> Value

instance ToBolt Text where
    toBolt = T

instance ToBolt Bool where
    toBolt = B

instance ToBolt a => ToBolt (Maybe a) where
    toBolt val = case val of
        Just v -> toBolt v
        Nothing -> N ()

instance ToBolt a => ToBolt [a] where
    toBolt v = L $ toBolt <$> v

instance ToBolt Sortee where
    toBolt (Sortee v) = toBolt $ pack v

instance ToBolt UTCTime where
    -- See here:
    -- https://github.com/neo4j/neo4j/blob/3.5/community/bolt/src/main/java/org/neo4j/bolt/v2/messaging/Neo4jPackV2.java
    toBolt t = S (Structure 70 (I epochSecondsLocal : I nano : I offsetSeconds : []))
        where epochSeconds :: NominalDiffTime
              epochSeconds = utcTimeToPOSIXSeconds t
              epochSecondsLocal :: Int
              epochSecondsLocal = fromInteger (floor epochSeconds)
              nano :: Int
              nano = round $ (epochSeconds - (fromIntegral $ (floor epochSeconds :: Integer))) / (realToFrac $ picosecondsToDiffTime 1000)
              offsetSeconds :: Int
              offsetSeconds = 0

instance (ToContent a, ToJSON a) => ToContent [a] where
    toContent = toContent . toJSON

instance (ToTypedContent a, ToJSON a) => ToTypedContent [a] where
    toTypedContent = TypedContent typeJson . toContent
