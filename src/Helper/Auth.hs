module Helper.Auth where

import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Database.Bolt (Pipe)
import Database.Operation (withBoltPipe, HasBoltPool, get)
import Yesod.Auth (YesodAuth, AuthId, maybeAuthId)
import Yesod.Core (HandlerSite, MonadHandler, MonadUnliftIO)
import Yesod.Core.Handler (notAuthenticated)

import Model.Entity (Entity)
import Model.User (User, UserId)

type WithAuthedUser m =
    ( MonadHandler m
    , AuthId (HandlerSite m) ~ UserId
    , YesodAuth (HandlerSite m)
    , HasBoltPool (HandlerSite m)
    , MonadUnliftIO m
    )

withAuthId ::
    ( MonadHandler m
    , master ~ HandlerSite m
    , YesodAuth master
    , HasBoltPool master
    , MonadUnliftIO m
    )=> (AuthId master -> m a) -> m a
withAuthId fn = do
    mAuthId <- maybeAuthId
    case mAuthId of
        Nothing -> notAuthenticated
        Just authId -> fn authId

withAuthedUser ::
    ( MonadHandler m
    , master ~ HandlerSite m
    , AuthId master ~ UserId
    , YesodAuth master
    , HasBoltPool master
    , MonadUnliftIO m
    )=> (Entity User -> m a) -> m a
withAuthedUser fn = do
    user <- runMaybeT $ do
        authId <- MaybeT $ maybeAuthId
        MaybeT $ withBoltPipe $ flip get authId
    case user of
        Nothing -> notAuthenticated
        Just x -> fn x

-- Also pass pipe to handler for extra db operations
withAuthedUser' ::
    ( MonadHandler m
    , master ~ HandlerSite m
    , AuthId master ~ UserId
    , YesodAuth master
    , HasBoltPool master
    , MonadUnliftIO m
    )=> (Pipe -> Entity User -> m a) -> m a
withAuthedUser' fn = do
    result <- runMaybeT $ do
        authId <- MaybeT $ maybeAuthId
        MaybeT $ withBoltPipe $ \pipe-> runMaybeT $ do
            user <- MaybeT $ get pipe authId
            lift $ fn pipe user
    case result of
        Nothing -> notAuthenticated
        Just x -> pure x
