module Handler.API.Task where


import ApiSubsite.Data
import Database.Operation ( TaskOrder(..)
                          , addTask
                          , checkGraphAccess
                          , findEndoRelationships
                          )
import Data.Aeson (withObject, (.:?), (.!=))
import Model.Task (Task, TaskWithoutSortee, TaskId)
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..))
import Model.Graph (GraphId)
import Model.Relationship (Relationship(..))
import Import hiding ((.), null)

import Handler.API.Task.Update (TaskCyclicError(..))

data TaskRequest = TaskRequest
    { reqDependsOn :: [TaskId]
    , reqDependedBy :: [TaskId]
    , reqTask :: TaskWithoutSortee
    , reqOrder :: TaskOrder
    }

instance FromJSON TaskRequest where
    parseJSON = withObject "TaskRequest" $ \v->
        TaskRequest <$> v .: "dependsOn"
                    <*> v .: "dependedBy"
                    <*> v .: "task"
                    <*> (   (fmap Before <$> v .:? "before")
                        <|> (fmap After  <$> v .:? "after"))
                        .!=       NoOrder

data AddTaskResponse = AddTaskResponse (Entity Task) [Entity Task]

instance ToJSON AddTaskResponse where
    toJSON (AddTaskResponse t ts) = object [ "task" .= t
                                           , "updatedTasks" .= ts
                                           ]

instance ToContent AddTaskResponse where
    toContent = toContent . toJSON

instance ToTypedContent AddTaskResponse where
    toTypedContent = TypedContent typeJson . toContent

postGraphsTasksR :: (WithAuthedUser (ApiHandler master)) => GraphId -> ApiHandler master AddTaskResponse
postGraphsTasksR graphId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    hasAccess <- checkGraphAccess pipe graphId userId
    when (not hasAccess) $ permissionDenied "Such graph does not exist or you do not have permission"

    body <- requireCheckJsonBody
    backwardPaths <- findEndoRelationships pipe Depend (reqDependsOn body) (reqDependedBy body)
    when (not $ null backwardPaths) $ sendStatusJSON status422 $ TaskCyclicError backwardPaths

    (task, affectedTasks) <- addTask pipe graphId (reqOrder body) (reqTask body) (reqDependsOn body) (reqDependedBy body)

    pure $ AddTaskResponse task affectedTasks
