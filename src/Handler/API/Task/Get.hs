module Handler.API.Task.Get where

import ApiSubsite.Data
import Database.Operation ( checkGraphAccess
                          , getGraphTasks
                          )
import Model.Task (Task(..))
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..))
import Model.Relationship (Relationship(..))
import Model.Graph (GraphId)
import Import hiding ((.), null, Response)

data Response = Response [Entity Task] [Relationship]

instance ToJSON Response where
    toJSON (Response ts rs) = object [ "tasks" .= ts
                                     , "relationships" .= rs
                                     ]

instance ToContent Response where
    toContent = toContent . toJSON

instance ToTypedContent Response where
    toTypedContent = TypedContent typeJson . toContent

getGraphsTasksR :: (WithAuthedUser (ApiHandler master)) => GraphId -> ApiHandler master Response
getGraphsTasksR graphId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    hasAccess <- checkGraphAccess pipe graphId userId
    if hasAccess
        then do
            includesCompleted <- (== "true") <$> fromMaybe "false" <$> lookupGetParam "includesCompleted"
            (tasks, relationships) <- getGraphTasks pipe graphId includesCompleted
            pure $ Response tasks relationships
        else permissionDenied "Such graph does not exist or you do not have permission"
