module Handler.API.Task.Update where

import ApiSubsite.Data
import Database.Operation ( checkTaskAccess
                          , updateTaskExceptSortee
                          , updateRelationships
                          , findEndoRelationships
                          , get
                          )
import Data.Aeson (withObject, (.:?))
import Data.List (nubBy)
import Model.Task (Task(..), TaskWithoutSortee, TaskId)
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..))
import Model.Relationship (Relationship(..), Path)
import Import hiding ((.), get, null, filter, any, elem)


data UpdateTaskRequest = UpdateTaskRequest
    { reqTask :: Maybe TaskWithoutSortee
    , reqCreateDependsOn :: [TaskId]
    , reqDeleteDependsOn :: [TaskId]
    , reqCreateDependedBy :: [TaskId]
    , reqDeleteDependedBy :: [TaskId]
    }

instance FromJSON UpdateTaskRequest where
    parseJSON = withObject "UpdateTaskRequest" $ \v->
        UpdateTaskRequest <$> v .:? "task"
                          <*> v .: "createDependsOn"
                          <*> v .: "deleteDependsOn"
                          <*> v .: "createDependedBy"
                          <*> v .: "deleteDependedBy"

data UpdateTaskResponse = UpdateTaskResponse (Entity Task) [Entity Task]

instance ToJSON UpdateTaskResponse where
    toJSON (UpdateTaskResponse t ts) = object [ "task" .= t
                                              , "updatedTasks" .= ts
                                              ]

instance ToContent UpdateTaskResponse where
    toContent = toContent . toJSON

instance ToTypedContent UpdateTaskResponse where
    toTypedContent = TypedContent typeJson . toContent


data TaskCyclicError = TaskCyclicError [Path]

instance ToJSON TaskCyclicError where
    toJSON (TaskCyclicError ps) = object [ "error" .= ("CyclicDependenciesFound" :: Text)
                                         , "paths" .= ps
                                         ]


putTasksR :: (WithAuthedUser (ApiHandler master)) => TaskId -> ApiHandler master UpdateTaskResponse
putTasksR taskId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    body <- requireCheckJsonBody

    hasAccess <- isJust <$> checkTaskAccess pipe taskId userId
    when (not hasAccess) $ permissionDenied "Such task does not exist or you do not have permission."

    -- wants to create (taskId)-[depend]->(dependsOn)
    -- wants to create (dependedBy)-[depend]->(taskId)
    -- If applied: dependedBy -> taskId -> dependsOn
    -- need to make sure there is zero: dependsOn -> (taskId + dependedBy)
    -- assumption: The current graph is not cyclic already

    let toDelete = ((\tid-> Depend tid taskId) <$> reqDeleteDependedBy body)
                <> ((\tid-> Depend taskId tid) <$> reqDeleteDependsOn body)
        toCreate = ((\tid-> Depend tid taskId) <$> reqCreateDependedBy body)
                <> ((\tid-> Depend taskId tid) <$> reqCreateDependsOn body)

    backwardPaths <- findEndoRelationships pipe Depend (reqCreateDependsOn body) ([taskId] <> reqCreateDependedBy body)
    let filteredBackwordPaths = filter (any $ not . flip elem toDelete) backwardPaths
        selfRef = elem (Depend taskId taskId) toCreate
    when (not $ null filteredBackwordPaths) $ sendStatusJSON status422 $ TaskCyclicError filteredBackwordPaths
    when selfRef $ sendStatusJSON status422 $ TaskCyclicError [[Depend taskId taskId]]

    mUpdateTaskResult <- case reqTask body of
      Just reqBody -> updateTaskExceptSortee pipe taskId reqBody
      Nothing -> do
        mTask <- get pipe taskId
        pure $ (, []) <$> mTask

    case mUpdateTaskResult of
        Nothing -> notFound
        Just (task, updatedTasks) -> do
            someOtherUpdatedTasks <- updateRelationships pipe toCreate toDelete
            pure $ UpdateTaskResponse task $ deduplicateTasks $ someOtherUpdatedTasks <> updatedTasks

    where deduplicateTasks = nubBy $ \(Entity t1 _) (Entity t2 _)-> t1 == t2
