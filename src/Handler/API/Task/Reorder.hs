module Handler.API.Task.Reorder where


import ApiSubsite.Data
import Database.Operation ( TaskOrder(..)
                          , checkTaskAccess
                          , getSortee
                          , update
                          , updateRelationships
                          , findEndoRelationships
                          )
import Data.Aeson (withObject, (.:?), (.!=))
import Data.Maybe (fromJust)
import Model.Task (TaskId, Task, TaskFields(..))
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..), (=.))
import Model.Relationship (Relationship(..))
import Import hiding ((.), null, filter, any, elem, putStrLn)

import Handler.API.Task.Update (TaskCyclicError(..))

data ReorderTaskRequest = ReorderTaskRequest
    { reqOrder :: TaskOrder
    , reqDeleteDependedBy :: [TaskId]
    , reqDeleteDependsOn :: [TaskId]
    , reqCreateDependedBy :: [TaskId]
    , reqCreateDependsOn :: [TaskId]
    } deriving (Show)

instance FromJSON ReorderTaskRequest where
    parseJSON = withObject "ReorderTaskRequest" $ \v->
        ReorderTaskRequest <$> ((Before <$> v .: "before") <|> (After <$> v .: "after") <|> (pure NoOrder))
                           <*> v .:? "deleteDependedBy" .!= []
                           <*> v .:? "deleteDependsOn" .!= []
                           <*> v .:? "createDependedBy" .!= []
                           <*> v .:? "createDependsOn" .!= []


data ReorderTaskResponse = ReorderTaskResponse [Entity Task]

instance ToJSON ReorderTaskResponse where
    toJSON (ReorderTaskResponse ts) = object [ "updatedTasks" .= ts ]

instance ToContent ReorderTaskResponse where
    toContent = toContent . toJSON

instance ToTypedContent ReorderTaskResponse where
    toTypedContent = TypedContent typeJson . toContent

-- support updating dependedby + dependson

putTasksReorderR :: (WithAuthedUser (ApiHandler master)) => TaskId -> ApiHandler master ReorderTaskResponse
putTasksReorderR taskId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    body <- requireCheckJsonBody

    mGraph <- checkTaskAccess pipe taskId userId
    when (isNothing mGraph) $ permissionDenied "Such task does not exist or you do not have permission."
    let (Entity graphId _graph) = fromJust mGraph

    let toDelete = ((\tid-> Depend tid taskId) <$> reqDeleteDependedBy body)
                <> ((\tid-> Depend taskId tid) <$> reqDeleteDependsOn body)
        toCreate = ((\tid-> Depend tid taskId) <$> reqCreateDependedBy body)
                <> ((\tid-> Depend taskId tid) <$> reqCreateDependsOn body)

    backwardPaths <- findEndoRelationships pipe Depend (reqCreateDependsOn body) ([taskId] <> reqCreateDependedBy body)
    let filteredBackwordPaths = filter (any $ not . flip elem toDelete) backwardPaths
        selfRef = elem (Depend taskId taskId) toCreate
    when (not $ null filteredBackwordPaths) $ sendStatusJSON status422 $ TaskCyclicError filteredBackwordPaths
    when selfRef $ sendStatusJSON status422 $ TaskCyclicError [[Depend taskId taskId]]

    case reqOrder body of
        NoOrder -> pure ()
        order -> do newSortee <- getSortee pipe graphId order
                    void $ update pipe taskId [TaskSortee =. newSortee]

    updatedTasks <- updateRelationships pipe toCreate toDelete

    pure $ ReorderTaskResponse updatedTasks
