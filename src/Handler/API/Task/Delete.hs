module Handler.API.Task.Delete where

import ApiSubsite.Data
import Database.Operation ( checkTaskAccess
                          , delete
                          , deleteOrphans
                          , findEndoRelationships
                          , moveDependsOnToAnotherTask
                          )
import Data.Aeson (withObject)
import Model.Task (TaskId, Task)
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..))
import Model.Relationship (Relationship(..))
import Import hiding ((.), null, filter, any, elem)

import Handler.API.Task.Update (TaskCyclicError(..))


data DependsOnMigration = DeleteRelationships
                        | DeleteOrphans
                        | BecomeDependsOn TaskId

data DeleteTaskRequest = DeleteTaskRequest { migration :: DependsOnMigration }

instance FromJSON DeleteTaskRequest where
    parseJSON = withObject "DeleteTaskRequest" $ \v->
        DeleteTaskRequest <$> (((\(text :: Text)-> case text of "relationships" -> pure DeleteRelationships
                                                                "orphans" -> pure DeleteOrphans
                                                                _ -> fail "Invalid delete value, has to be either relationships or orphans"
                                ) =<< v .: "delete")
                          <|> (BecomeDependsOn <$> v .: "becomeDependsOn"))

data DeleteTaskResponse = DeleteTaskResponse [Entity Task]

instance ToJSON DeleteTaskResponse where
    toJSON (DeleteTaskResponse ts) = object [ "updatedTasks" .= ts ]

instance ToContent DeleteTaskResponse where
    toContent = toContent . toJSON

instance ToTypedContent DeleteTaskResponse where
    toTypedContent = TypedContent typeJson . toContent


putTasksDeleteR :: (WithAuthedUser (ApiHandler master)) => TaskId -> ApiHandler master DeleteTaskResponse
putTasksDeleteR taskId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    body <- requireCheckJsonBody

    hasAccess <- isJust <$> checkTaskAccess pipe taskId userId
    when (not hasAccess) $ permissionDenied "Such task does not exist or you do not have permission."

    updatedTasks <- case migration body of
        BecomeDependsOn newParent -> do
            backwardPaths <- findEndoRelationships pipe Depend [taskId] [newParent]
            when (not $ null backwardPaths) $ sendStatusJSON status422 $ TaskCyclicError backwardPaths
            moveDependsOnToAnotherTask pipe taskId newParent
        DeleteOrphans -> do
            deleteOrphans pipe taskId
            pure []
        DeleteRelationships -> pure []

    delete pipe taskId

    pure $ DeleteTaskResponse updatedTasks
