module Handler.API.Graph where


import ApiSubsite.Data
import Database.Operation (addGraph, getUserGraphs)
import Helper.Auth (WithAuthedUser, withAuthedUser')
import Model.Entity (Entity(..))
import Model.Graph (Graph)
import Import

postGraphsR :: (WithAuthedUser (ApiHandler master)) => ApiHandler master (Entity Graph)
postGraphsR = withAuthedUser' $ \pipe (Entity userId _user)-> do
    graph <- requireCheckJsonBody
    addGraph pipe userId graph

getGraphsR :: (WithAuthedUser (ApiHandler master)) => ApiHandler master [Entity Graph]
getGraphsR = withAuthedUser' $ \pipe (Entity userId _user)-> getUserGraphs pipe userId
