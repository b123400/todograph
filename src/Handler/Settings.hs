module Handler.Settings where

import Import

import Helper.Auth (withAuthId)
import Yesod.Auth.Email (setpassR)

data NewGraph = NewGraph
    { _graphName :: Text
    } deriving (Show)

getSettingsR :: Handler Html
getSettingsR = withAuthId $ \_-> do
    defaultLayout $ [whamlet|
        <h2>Settings
        <ul>
            <li>
                <a href=@{AuthR setpassR}>Change password
    |]
