module Handler.NewGraph where

import Import

import Database.Operation (addGraph)
import Model.Entity (Entity(..))
import Model.Graph (Graph(..))
import Helper.Auth (withAuthId, withAuthedUser')

data NewGraph = NewGraph
    { _graphName :: Text
    } deriving (Show)

getNewGraphR :: Handler Html
getNewGraphR = withAuthId $ \_-> do
    (formWidget, formEnctype) <- generateFormPost sampleForm

    defaultLayout $ [whamlet|
        <h2>Create new graph
        <form method=post action=@{NewGraphR} enctype=#{formEnctype}>
            ^{formWidget}

            <button .button.--primary type="submit">
                Create
    |]

postNewGraphR :: Handler Html
postNewGraphR = withAuthedUser' $ \pipe (Entity userId _user)-> do
    ((result, _), _) <- runFormPost sampleForm

    _ <- case result of
        FormSuccess res -> do
            addGraph pipe userId (Graph { graphName = _graphName res })
        _ -> invalidArgs []

    redirect ApplicationR

sampleForm :: Form NewGraph
sampleForm extra = do
    let nameSettings = FieldSettings
            { fsLabel = "Graph Name"
            , fsTooltip = Nothing
            , fsId = Just "name"
            , fsName = Just "name"
            , fsAttrs =
                [ ("autofocus", "")
                ]
            }
    (nameRes, nameView) <- mreq textField nameSettings Nothing

    let userRes = NewGraph <$> nameRes
    let widget = [whamlet|
            #{extra}
            <label>
                <p .field-name>^{fvLabel nameView}
                ^{fvInput nameView}
        |]

    return (userRes, widget)
