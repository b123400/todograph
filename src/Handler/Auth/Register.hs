module Handler.Auth.Register where

import Import.NoFoundation
import Yesod.Auth (AuthHandler, authLayout)
import Yesod.Auth.Email (YesodAuthEmail, registerR)

data UserForm = UserForm { _userFormEmail :: Text }

registerHandler :: YesodAuthEmail master => AuthHandler master Html
registerHandler = do
    (widget, enctype) <- generateFormPost registrationForm
    toParentRoute <- getRouteToParent
    authLayout $ do
        setTitle "Signing up Todograph"
        [whamlet|
            <form method="post" action="@{toParentRoute registerR}" enctype=#{enctype}>
                <div id="registerForm">
                    ^{widget}
                <button .button.--primary>Sign up
        |]
    where
        registrationForm extra = do
            let emailSettings = FieldSettings {
                fsLabel = "Email",
                fsTooltip = Nothing,
                fsId = Just "email",
                fsName = Just "email",
                fsAttrs = [("autofocus", "")]
            }

            (emailRes, emailView) <- mreq emailField emailSettings Nothing

            let userRes = UserForm <$> emailRes
            let widget = [whamlet|
                    #{extra}
                    <label>
                        <p .field-name>^{fvLabel emailView}
                        ^{fvInput emailView}
                |]

            return (userRes, widget)
