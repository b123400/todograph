module Handler.Auth.Login where

import Import.NoFoundation
import Yesod.Auth (Auth)
import Yesod.Auth.Email (YesodAuthEmail, loginR, registerR)

data UserLoginForm = UserLoginForm { _loginEmail :: Text, _loginPassword :: Text }

loginHandler
  :: YesodAuthEmail master
  => (Route Auth -> Route master)
  -> WidgetFor master ()
loginHandler toParent = do
        (widget, enctype) <- generateFormPost loginForm

        [whamlet|
            <form method="post" action="@{toParent loginR}", enctype=#{enctype}>
                <div id="emailLoginForm">
                    ^{widget}
                    <div>
                        <button type=submit .button.--primary>
                            Login
                        &nbsp;
                        <a href="@{toParent registerR}" .btn .btn-default>
                            Sign up
        |]
  where
    loginForm extra = do
        (emailRes, emailView) <- mreq emailField emailSettings Nothing
        (passwordRes, passwordView) <- mreq passwordField passwordSettings Nothing

        let userRes = UserLoginForm <$> emailRes
                                    <*> passwordRes
        let widget = [whamlet|
                #{extra}
                <label>
                    <p .field-name>Email
                    ^{fvInput emailView}
                <label>
                    <p .field-name>Password
                    ^{fvInput passwordView}
            |]

        return (userRes, widget)
    emailSettings = do
        FieldSettings {
            fsLabel = "Email",
            fsTooltip = Nothing,
            fsId = Just "email",
            fsName = Just "email",
            fsAttrs = [("autofocus", "")]
        }
    passwordSettings =
         FieldSettings {
            fsLabel = "Password",
            fsTooltip = Nothing,
            fsId = Just "password",
            fsName = Just "password",
            fsAttrs = []
        }
