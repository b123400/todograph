module Handler.Auth.SetPassword where

import Import.NoFoundation
import Yesod.Auth (AuthHandler, authLayout)
import Yesod.Auth.Email (YesodAuthEmail, setpassR)

import qualified Yesod.Auth.Message       as Msg

data PasswordForm = PasswordForm { _passwordCurrent :: Text, _passwordNew :: Text, _passwordConfirm :: Text }

setPasswordHandler :: YesodAuthEmail master => Bool -> AuthHandler master TypedContent
setPasswordHandler needOld = do
    toParent <- getRouteToParent
    fmap toTypedContent $ authLayout $ do
        (widget, enctype) <- generateFormPost setPasswordForm
        setTitleI Msg.SetPassTitle
        [whamlet|
            <h2>_{Msg.SetPass}
            <form method="post" action="@{toParent setpassR}" enctype=#{enctype}>
                ^{widget}
                <button .button.--primary type=submit>_{Msg.SetPassTitle}
        |]
  where
    setPasswordForm extra = do
        (currentPasswordRes, currentPasswordView) <- mreq passwordField currentPasswordSettings Nothing
        (newPasswordRes, newPasswordView) <- mreq passwordField newPasswordSettings Nothing
        (confirmPasswordRes, confirmPasswordView) <- mreq passwordField confirmPasswordSettings Nothing

        let passwordFormRes = PasswordForm <$> currentPasswordRes <*> newPasswordRes <*> confirmPasswordRes
        let widget = [whamlet|
                #{extra}
                $if needOld
                    <label>
                        <p .field-name>^{fvLabel currentPasswordView}
                        ^{fvInput currentPasswordView}
                <label>
                    <p .field-name>^{fvLabel newPasswordView}
                    ^{fvInput newPasswordView}

                <label>
                    <p .field-name>^{fvLabel confirmPasswordView}
                    ^{fvInput confirmPasswordView}
            |]

        return (passwordFormRes, widget)
    currentPasswordSettings =
         FieldSettings {
             fsLabel = SomeMessage Msg.CurrentPassword,
             fsTooltip = Nothing,
             fsId = Just "currentPassword",
             fsName = Just "current",
             fsAttrs = [("autofocus", "")]
         }
    newPasswordSettings =
        FieldSettings {
            fsLabel = SomeMessage Msg.NewPass,
            fsTooltip = Nothing,
            fsId = Just "newPassword",
            fsName = Just "new",
            fsAttrs = [("autofocus", ""), (":not", ""), ("needOld:autofocus", "")]
        }
    confirmPasswordSettings =
        FieldSettings {
            fsLabel = SomeMessage Msg.ConfirmPass,
            fsTooltip = Nothing,
            fsId = Just "confirmPassword",
            fsName = Just "confirm",
            fsAttrs = [("autofocus", "")]
        }
