{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.DoNow (getDoNowIndexR, getDoNowR, postDoNowR) where

import Import

import Database.Operation (checkGraphAccess, getRandomTaskWithNoDependencies, getUserGraphs, update)
import Helper.Auth (withAuthedUser')
import Model.Entity (Entity(..), (=.))
import Model.Graph (GraphId, Graph(..))
import Model.Task (Task(..), TaskId(..), TaskFields(..), TaskState(..))

getDoNowIndexR :: Handler Html
getDoNowIndexR = withAuthedUser' $ \pipe (Entity userId _user)-> do
    graphs <- getUserGraphs pipe userId
    defaultLayout $ do
        setTitle "Do this now"
        $(widgetFile "do-now-index")

getDoNowR :: GraphId -> Handler Html
getDoNowR graphId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    hasAccess <- checkGraphAccess pipe graphId userId
    when (not hasAccess) $ permissionDenied "You do not have permission."

    mTask <- getRandomTaskWithNoDependencies pipe graphId
    mCsrfToken <- lookupCookie (decodeUtf8 defaultCsrfCookieName)

    defaultLayout $ do
        setTitle "Do this now"
        $(widgetFile "do-now")

postDoNowR :: GraphId -> Handler Html
postDoNowR graphId = withAuthedUser' $ \pipe (Entity userId _user)-> do
    hasAccess <- checkGraphAccess pipe graphId userId
    when (not hasAccess) $ permissionDenied "You do not have permission."

    mTaskId <- lookupPostParam "taskId"
    case mTaskId of
        Just taskId -> do
            _ <- update pipe (TaskId taskId) [TaskState =. Done]
            redirect $ DoNowR graphId
        Nothing -> invalidArgs ["taskId"]
