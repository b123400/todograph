module Handler.Application where

import Import
import Yesod.Auth (maybeAuthId)

getApplicationR :: Handler Html
getApplicationR = do
  mAuthId <- maybeAuthId
  case mAuthId of
    Nothing -> notAuthenticated
    Just _ -> do
      defaultLayout $ do
          setTitle "Your todograph"
          $(widgetFile "application")
