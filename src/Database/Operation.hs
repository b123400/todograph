module Database.Operation where

import Prelude hiding (lookup)
import ClassyPrelude (Typeable, tshow, swap, join)
import Control.Monad.Catch (Exception, MonadThrow, throwM)
import Control.Monad.Trans.Maybe   (runMaybeT, MaybeT(..))
import Database.Bolt (Pipe, Value(..), pathNodes, queryP, queryP_, run, exactMaybe, nodeProps, startNodeId, endNodeId, nodeIdentity)
import Data.Bolt.Read (ReadBolt, readBoltNode, readBoltValue)
import Data.List (find, nub)
import Data.Map (fromList, toList, elems, lookup, (!), (!?))
import Data.Maybe (listToMaybe, catMaybes, fromMaybe, mapMaybe)
import Data.Pool (withResource, Pool)
import Data.UUID.V4 (nextRandom)
import Data.Text (Text, unpack, pack, intercalate, concat)
import Data.Typeable (cast, typeOf)
import Foundation.Monad (MonadIO, liftIO)
import System.Random (randomRIO)
import Yesod.Core (HandlerSite, MonadHandler, MonadUnliftIO, getYesod, withRunInIO)

import Model.Entity (Entity(..), IsEntity, Key, entityTypeName, Fields, fieldName, Pair(..), QueryPair(..), ToBolt, toBolt, (=.))
import Model.Graph (Graph(..), GraphId)
import Model.User (User(..), UserId)
import Model.Task (Task(..), TaskWithoutSortee(..), TaskId(..), TaskState(..), TaskFields(..), withSortee)
import Model.Relationship (Relationship(..), toBoltName)
import Model.Sortee as S

import qualified Data.UUID

data TaskOrder = Before TaskId
               | After TaskId
               | NoOrder -- default to after the last task
               deriving (Show)

data SorteeException = RefTaskIdNotFound TaskId
    deriving (Show, Typeable)

instance Exception SorteeException

class HasBoltPool a where
    boltPool :: a -> Pool Pipe

withBoltPipe :: (MonadHandler m, HasBoltPool (HandlerSite m), MonadUnliftIO m)=> (Pipe -> m a) -> m a
withBoltPipe fn = boltPool <$> getYesod >>= \pool->
    withRunInIO $ \toIO-> liftIO $ withResource pool $ \pipe-> (toIO $ fn pipe)

get
  :: forall e f m
  . ( MonadIO m
    , IsEntity e
    , Fields e f
    , ReadBolt (Entity e)
    , ToBolt (Key e)
    )
  => Pipe
  -> Key e
  -> m (Maybe (Entity e))
get pipe key = do
    records <- Database.Bolt.run pipe $ queryP ("MATCH (x:" <> entityTypeName @e <> ") WHERE x.id={id} RETURN x;") $ fromList [("id", toBolt key)]
    pure $ readBoltValue =<< (lookup "x") =<< listToMaybe records

getWhere
  :: forall e f m
  . ( MonadIO m
    , IsEntity e
    , Fields e f
    , ReadBolt (Entity e)
    , ToBolt (Key e)
    )
  => Pipe
  -> [QueryPair f]
  -> m [Entity e]
getWhere pipe queries = do
    let (query, params) = queryAndParams queries
    records <- Database.Bolt.run pipe $ queryP ("MATCH (x:" <> entityTypeName @e <> ") WHERE " <> query <> " RETURN x;") params
    pure $ catMaybes $ ((=<<) readBoltValue) <$> (lookup "x") <$> records
    where queryAndParams qs = let qAndP = queryToStringAndParam <$> qs
                                  q = intercalate " AND " $ fst <$> qAndP
                                  p = fromList $ (snd <$> qAndP)
                              in (q, p)
          queryToStringAndParam :: QueryPair f -> (Text, (Text, Value))
          queryToStringAndParam (Equal f a) = let name = fieldName $ f a
                                              in ("x."<> name <> "=" <> "{" <> name <> "}", (name, toBolt a))

updateMultiple
  :: forall e f m
  . ( MonadIO m
    , IsEntity e
    , Fields e f
    , ReadBolt (Entity e)
    , ToBolt (Key e)
    )
  => Pipe
  -> [Key e]
  -> [Pair f]
  -> m [Entity e]
updateMultiple pipe identifiers fields = if null fields then pure [] else do
    let qAndP = queryAndParam <$> fields
        query = intercalate "," $ fst <$> qAndP
        params = fromList $ (snd <$> qAndP) <> [("ids", toBolt identifiers)]
        cypherQuery = "MATCH (x:"<> entityTypeName @e <> ") WHERE x.id IN {ids} SET " <> query <> " RETURN x;"
    records <- Database.Bolt.run pipe $ queryP cypherQuery params
    pure $ catMaybes $ ((=<<) readBoltValue) <$> (lookup "x") <$> records
    where queryAndParam :: Pair f -> (Text, (Text, Value))
          queryAndParam (Pair f a) = let name = fieldName @e @f $ f a
                                     in ("x." <> name <> "=" <> "{" <> name <> "}", (name, toBolt a))

update :: forall e f m. (MonadIO m, IsEntity e, Fields e f, ReadBolt (Entity e), ToBolt (Key e))=> Pipe -> Key e -> [Pair f] -> m (Maybe (Entity e))
update pipe identifier fields = listToMaybe <$> updateMultiple pipe [identifier] fields

delete :: forall m e. (MonadIO m, IsEntity e, ToBolt (Key e))=> Pipe -> Key e -> m ()
delete pipe identifier = do
    Database.Bolt.run pipe $ queryP_ ("MATCH (x:"<> entityTypeName @e <>") WHERE x.id={xid} DETACH DELETE x;")
                                     (fromList [("xid", toBolt identifier)])

addUserWithoutPassword :: (MonadIO m) => Pipe -> User -> m UserId
addUserWithoutPassword pipe user = do
    userId <- pack <$> Data.UUID.toString <$> liftIO Data.UUID.V4.nextRandom
    records <- Database.Bolt.run pipe $ queryP (
        "CREATE (user:User {id: {userId}"
                       <> ",email: {email}"
                       -- <> ",password: \"\"" Without password
                       <> ",verkey: {verkey}"
                       <> ",verified: {verified}"
                       <> "}) RETURN user;")
        (fromList [("userId", toBolt userId)
                  ,("email", toBolt $ userEmail user)
                  ,("verkey", toBolt $ userVerkey user)
                  ,("verified", B $ userVerified user)])
    case entityId <$> (readBoltValue =<< (lookup "user") =<< listToMaybe records) of
        Just i -> pure i
        Nothing -> error "Cannot read id from user!?"

updateUserPassword :: MonadIO m => Pipe -> UserId -> Text -> m ()
updateUserPassword pipe userId password = do
    Database.Bolt.run pipe $ queryP_ "MATCH (user:User) WHERE user.id = {userId} SET user.password = {password};"
                           $ fromList [ ("userId", toBolt userId)
                                      , ("password", T password)]

getUserPassword :: MonadIO m => Pipe -> UserId -> m (Maybe Text)
getUserPassword pipe userId = runMaybeT $ do
    record <- MaybeT $ listToMaybe <$> (Database.Bolt.run pipe $ queryP "MATCH (user:User) WHERE user.id = {userId} RETURN user;"
                                                               $ fromList [("userId", toBolt userId)])
    user <- MaybeT $ pure $ (exactMaybe =<<) (lookup "user" record)
    MaybeT $ pure $ exactMaybe =<< (nodeProps user) !? "password"

getUserGraphs :: MonadIO m => Pipe -> UserId -> m [Entity Graph]
getUserGraphs pipe userId = do
    records <- Database.Bolt.run pipe $ queryP "MATCH (u:User)-[:ACCESS]->(g:Graph) WHERE u.id = {userId} RETURN g;"
                                      $ fromList [("userId", toBolt userId)]
    pure $ catMaybes $ ((=<<) readBoltValue) <$> (lookup "g") <$> records

getGraphTasks :: MonadIO m => Pipe -> GraphId -> Bool -> m ([Entity Task], [Relationship])
getGraphTasks pipe graphId includesCompleted = do
    let extraCondition =
          if includesCompleted
          then ""
          else " AND t.state <> \"" <> (pack $ show Done) <>  "\" "
    records <- Database.Bolt.run pipe $ queryP ("MATCH (g:Graph)-[:CONTAIN]->(t:Task) WHERE g.id = {graphId} "
                                            <> extraCondition
                                            <> " RETURN t ORDER BY t.sortee;")
                                      $ fromList [("graphId", toBolt graphId)]
    rRecords <- Database.Bolt.run pipe $ queryP "MATCH (g:Graph)-[:CONTAIN]->(t1: Task)-[x:DEPEND]->(t2:Task) WHERE g.id = {graphId} RETURN t1, t2;"
                                       $ fromList [("graphId", toBolt graphId)]
    -- TODO: better performance by only querying id?
    -- can we do this is with a single query?
    let tasks = catMaybes $ ((=<<) readBoltValue) <$> (lookup "t") <$> records
    let relationships = catMaybes $ (\r -> Depend <$> (entityId <$> (readBoltValue =<< lookup "t1" r)) <*> (entityId <$> (readBoltValue =<< lookup "t2" r))) <$> rRecords
    pure $ (tasks, relationships)

-- Only for finding relationship in the same type so it's endo
findEndoRelationships :: forall a r m. (MonadIO m, IsEntity a, ReadBolt (Entity a), ToBolt (Key a)) => Pipe -> (Key a -> Key a -> r) -> [Key a] -> [Key a] -> m [[r]]
findEndoRelationships pipe constructor fromIds toIds = do
    records <- Database.Bolt.run pipe $ queryP
            ("MATCH (x1:" <> entityTypeName @a <> ") WHERE x1.id IN {x1ids} "
          <> "MATCH (x2:" <> entityTypeName @a <> ") WHERE x2.id IN {x2ids} "
          <> "MATCH path = (x1)-[*]->(x2) "
          <> "RETURN path;")
            (fromList [("x1ids", toBolt fromIds)
                      ,("x2ids", toBolt toIds)])
    let paths = catMaybes $ ((=<<) exactMaybe) <$> (lookup "path") <$> records
    pure $ idsToRelationships <$> (fmap entityId) <$> catMaybes <$> (fmap readBoltNode) <$> pathNodes <$> paths

    where idsToRelationships [] = []
          idsToRelationships (_:[]) = [] -- Impossible!
          idsToRelationships ids@(_:xs) = zipWith (constructor . (ids !!)) [0..] xs


addGraph :: (MonadIO m) => Pipe -> UserId -> Graph -> m (Entity Graph)
addGraph pipe userId graph = do
    graphId <- pack <$> Data.UUID.toString <$> liftIO Data.UUID.V4.nextRandom
    record <- Database.Bolt.run pipe $ queryP
        ("MATCH (u:User) WHERE u.id = {userId} "
      <> "CREATE (u)-[:ACCESS]->(g:Graph {id: {graphId}"
                                         <> ",name: {name}"
                                         <> "}) RETURN g;")
        (fromList [("userId", toBolt userId)
                  ,("graphId", toBolt graphId)
                  ,("name", toBolt $ graphName graph)])
    case readBoltValue =<< (lookup "g") =<< listToMaybe record of
        Just x -> pure x
        Nothing -> error "Cannot read a just-added-graph!?"

addTask
  :: (MonadIO m, MonadThrow m)
  => Pipe
  -> GraphId
  -> TaskOrder
  -> TaskWithoutSortee
  -> [TaskId]   -- depends on
  -> [TaskId]   -- depended by
  -> m (Entity Task, [Entity Task] {- Other tasks that are affected, e.g. undone parents -})
addTask pipe graphId taskOrder tempTask dependOnTaskIds dependedByTaskIds = do
    taskId <- pack <$> Data.UUID.toString <$> liftIO Data.UUID.V4.nextRandom
    let taskIdsMap = mapId $ nub $ dependOnTaskIds <> dependedByTaskIds

    task <- withSortee tempTask <$> getSortee pipe graphId taskOrder
    records <- Database.Bolt.run pipe $ queryP
        ("MATCH (g:Graph) WHERE g.id = {graphId} "
      <> (intercalate " " $ matchingPhases taskIdsMap)
      <> "CREATE (g)-[:CONTAIN]->(task:Task {id: {taskId}"
                                         <> ",state: {state}"
                                         <> ",body: {body}"
                                         <> ",dueDate: {dueDate}"
                                         <> ",comment: {comment}"
                                         <> ",sortee: {sortee}"
                                         <> ",autocomplete: {autocomplete}"
                                         <> "}) "
      <> (Data.Text.concat $ dependingOnCreatePhase taskIdsMap <$> dependOnTaskIds)
      <> (Data.Text.concat $ dependedByCreatePhase taskIdsMap <$> dependedByTaskIds)
      <> "RETURN task;")
        (fromList $ [("graphId", toBolt graphId)
                    ,("taskId", toBolt taskId)
                    ,("state", toBolt $ taskState task)
                    ,("body", toBolt $ taskBody task)
                    ,("dueDate", toBolt $ taskDueDate task)
                    ,("comment", toBolt $ taskComment task)
                    ,("sortee", toBolt $ taskSortee task)
                    ,("autocomplete", toBolt $ taskAutocomplete task)
                    ] <> (params taskIdsMap))
    newTask <- case readBoltValue =<< (lookup "task") =<< listToMaybe records of
        Just x -> pure x
        Nothing -> error "Cannot read a just-added-task!?"

    -- Set parents to Pending if the new task is not done
    affectedParents <-
        if taskState task == Done
        then pure []
        else do
            parentRecords <- Database.Bolt.run pipe $ queryP
                             ("MATCH (t:Task)<-[:DEPEND*]-(p:Task) WHERE t.id = {taskId} SET p.state = {state} RETURN p;")
                             (fromList $ [("taskId", toBolt taskId)
                                         ,("state", toBolt Pending)
                                         ])
            pure $ catMaybes $ ((=<<) readBoltValue) <$> (lookup "p") <$> parentRecords
    pure (newTask, affectedParents)
    where -- { "taskId1" : "t1", "taskId2" : "t2" .... }
          mapId ids = (("t" <>) . tshow) <$> (fromList $ zipWith (,) ids [(0 :: Int)..])
          matchingPhases idsMap = (\k -> " MATCH (" <> k <> ":Task) WHERE " <> k <> ".id={"<> k <> "id} ") <$> elems idsMap
          params idsMap = (\(tid, boltId)-> (boltId <> "id", toBolt tid)) <$> toList idsMap

          dependingOnCreatePhase idsMap tid = ",(task)-[:DEPEND]->(" <> (idsMap ! tid) <> ")"
          dependedByCreatePhase idsMap tid = ",(task)<-[:DEPEND]-(" <> (idsMap ! tid) <> ")"

-- This method does not update Sortee.
updateTaskExceptSortee
  :: MonadIO m
  => Pipe
  -> TaskId
  -> TaskWithoutSortee
  -> m (Maybe (Entity Task {- Updated task (id=taskId) -}, [Entity Task] {- The tasks that are affected (e.g. cascade completion) -}))
updateTaskExceptSortee pipe taskId (TaskWithoutSortee task)
    | (taskState task /= Done) = do
        mtask <- update pipe taskId [ TaskState =. taskState task
                                    , TaskBody  =. taskBody task
                                    , TaskDueDate =. taskDueDate task
                                    , TaskComment =. taskComment task
                                    , TaskAutocomplete =. taskAutocomplete task
                                    ]
        updatedParents <- autoIncompleteParents pipe taskId
        return $ (, updatedParents) <$> mtask
    | otherwise = do
        -- When a parent task completes, all dependencies are completed
        records <- Database.Bolt.run pipe $ queryP
            ("MATCH (t1:Task) "
            <> "WHERE t1.id = {taskId} "
            <> "OPTIONAL MATCH (t1)-[:DEPEND*]->(t2:Task) "
            <> "SET t1.state={state}, t1.body={body}, t1.dueDate={dueDate}, t1.comment={comment}, t1.autocomplete={autocomplete}, t2.state={state} "
            <> "RETURN t1, t2;")
            (fromList $ [("taskId", toBolt taskId)
                        ,("state", toBolt $ taskState task)
                        ,("body", toBolt $ taskBody task)
                        ,("dueDate", toBolt $ taskDueDate task)
                        ,("comment", toBolt $ taskComment task)
                        ,("autocomplete", toBolt $ taskAutocomplete task)
                        ])
        let t1  = readBoltValue =<< (lookup "t1") =<< (listToMaybe records)
            t2s = catMaybes $ ((=<<) readBoltValue) <$> (lookup "t2") <$> records

        autoCompletedTask <- autoCompleteParents pipe $ taskId : (entityId <$> t2s)

        pure $ (, t2s <> autoCompletedTask) <$> t1

autoCompleteParents :: MonadIO m => Pipe -> [TaskId] -> m [Entity Task]
autoCompleteParents pipe taskIds = do
    -- Assumption: Task of {taskId} has been set to Done now
    -- this get all the continuous parents that has auto complete = true;
    -- e.g. [me]->[1: autocomplete=true]->[2: autocomplete=false]->[3: autocomplete=true]
    -- return: [me]->[1].
    parentRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH p=(t:Task)<-[:DEPEND*]-(t2:Task) "
        <> "WHERE t.id IN {taskIds} AND ALL(r IN relationships(p) WHERE startNode(r).autocomplete = true) "
        <> "RETURN t, t2, relationships(p) as r;")
        (fromList $ [("taskIds", toBolt taskIds)
                    ])

    let taskNode = exactMaybe =<< (lookup "t") =<< (listToMaybe parentRecords)
        allNodes = maybe parentNodes (: parentNodes) taskNode
        parentNodes = catMaybes $ ((=<<) exactMaybe) <$> (lookup "t2") <$> parentRecords
        parents = catMaybes $ readBoltNode <$> parentNodes :: [Entity Task]
        parentIds = entityId <$> parents
        paths = catMaybes $ ((=<<) exactMaybe) <$> (lookup "r") <$> parentRecords
        taskRelationships = nub $ join $ catMaybes $ pathToIdPairs allNodes <$> paths
        reversedTaskRelationships = swap <$> taskRelationships

    -- These are nodes that are:
    --   - Parents of the taskIds
    --   - autocomplete = true
    --   - All other siblings are done
    -- Which means "If some more children are done, these tasks should also be mark as done"
    -- e.g:
    -- [me, marking as done] -> [ 1:autocomplete = true ] -> [3: autocomplete = true] -> [5: autocomplete= true]
    --           [2: done] -------^              [4: pending] -----^
    -- In this case,
    -- parentIds = 1, 3, 5
    -- completingParents = 1, 5
    -- But 5 should not be marked as done because 3 will not be mark as done
    -- We have to traverse through the tasks to see what has to be updated, i.e. find a direct depend path from me to 5
    completingParents <- Database.Bolt.run pipe $ queryP
        ("MATCH p=(t:Task)-[:DEPEND]->(sibling: Task) "
        <> "WHERE t.id IN {parentIds} "
        <> "WITH DISTINCT t, collect(sibling) as c "
        <> "WHERE ALL(node IN c WHERE node.id IN {taskIds} OR node.state = {done} OR node.id IN {parentIds}) "
        <> "RETURN t.id;")
        (fromList $ [("parentIds", toBolt parentIds)
                    ,("taskIds", toBolt taskIds)
                    ,("done", toBolt Done)
                    ])

    let completingParentIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t.id") <$> completingParents
        taskIdsToComplete = filter (\end -> any (reachable completingParentIds reversedTaskRelationships end) taskIds) completingParentIds

    updateMultiple pipe taskIdsToComplete [TaskState =. Done]

    where pathToIdPairs nodes path = sequence $ relationshipToPair nodes <$> path
          relationshipToPair nodes r =
              let startNode = find ((==) (startNodeId r) . nodeIdentity) nodes
                  endNode   = find ((==) (endNodeId r) . nodeIdentity) nodes
                  startTask = readBoltNode =<< startNode
                  endTask   = readBoltNode =<< endNode
              in (,) <$> (entityId <$> startTask) <*> (entityId <$> endTask)

          reachable allowedIds relationships end start =
              let fromMe = filter (\(s, _)-> s == start) relationships
                  nexts = snd <$> fromMe
                  allowedNexts = filter (flip elem allowedIds) nexts
              in elem end allowedNexts
                 || end == start
                 || any (reachable allowedIds relationships end) allowedNexts

autoIncompleteParents :: MonadIO m => Pipe -> TaskId -> m [Entity Task]
autoIncompleteParents pipe taskId = do
    -- Assumption: The task of {taskId} has now been set to pending from done
    -- Or, relationship has changed that the task (state=pending) has new parents.
    -- so it's parents with done = true need to be undone to pending
    parentRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH p=(t:Task {id:{taskId}})<-[:DEPEND*]-(t2:Task) "
        <> "WHERE ALL(r IN relationships(p) WHERE startNode(r).state = {done}) "
        <> "RETURN t2.id;")
        (fromList $ [("taskId", toBolt taskId)
                    ,("done", toBolt Done)
                    ])
    let parentIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t2.id") <$> parentRecords
    updateMultiple pipe parentIds [TaskState =. Pending]

ensurePendingTasksParentsState :: MonadIO m => Pipe -> [TaskId] -> m [Entity Task]
ensurePendingTasksParentsState pipe taskIds = do
    -- Some tasks now has new parents, this function ensure if a task is pending,
    -- all of its parents will become pending.
    parentRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH p=(t:Task)<-[:DEPEND*]-(t2:Task) "
        <> "WHERE ALL(r IN relationships(p) WHERE startNode(r).state = {done}) "
        <> "AND t.id IN {taskIds} AND t.state = {pending} "
        <> "RETURN t2.id;")
        (fromList $ [("taskIds", toBolt taskIds)
                    ,("pending", toBolt Pending)
                    ,("done", toBolt Done)
                    ])
    let parentIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t2.id") <$> parentRecords
    updateMultiple pipe parentIds [TaskState =. Pending]

data SomeEntityId = forall k e. (ToBolt k, Eq k, Ord k, Typeable k, IsEntity e, k ~ Key e) => SomeEntityId k

instance Eq SomeEntityId where
    (SomeEntityId a) == (SomeEntityId b) =
        if typeOf a == typeOf b
            then case cast a of
                     Nothing -> False
                     Just a' -> a' == b
            else False

instance Ord SomeEntityId where
    compare (SomeEntityId a) (SomeEntityId b) =
        case compare (typeOf a) (typeOf b) of
            LT -> LT
            GT -> GT
            EQ -> case cast a of
                      Nothing -> EQ
                      Just a' -> compare a' b

updateRelationships :: MonadIO m => Pipe -> [Relationship] -> [Relationship] -> m [Entity Task]
updateRelationships pipe toCreates toDelete = do
    if (length toCreates + length toDelete) == 0
        then pure []
        else do
            Database.Bolt.run pipe $ queryP_ (Data.Text.concat $ matchingPhases <> deletePhases <> createPhases)
                                             (fromList matchingParams)
            ensurePendingTasksParentsState pipe taskIdsWithNewParents

    where matchingPhases = (\((SomeEntityId (_ :: (Key e))), v)-> "MATCH (" <> v <> ":" <> entityTypeName @e <> ") WHERE " <> v <> ".id={" <> v <> "Id} ") <$> toList taskIdsMap
          matchingParams = (\((SomeEntityId a), v)-> (v <> "Id", toBolt a)) <$> toList taskIdsMap
          createPhases = (\r-> let (from, to) = case r of
                                                    Depend a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                                                    Contain a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                                                    Access a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                               in "MERGE (" <> from <> ")-[:" <> (toBoltName r) <> "]->(" <> to <> ") ") <$> toCreates

          deletePhases = zipWith (\index r-> let (from, to) = case r of
                                                    Depend a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                                                    Contain a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                                                    Access a b -> (taskIdsMap ! (SomeEntityId a), taskIdsMap ! (SomeEntityId b))
                                       in "MATCH (" <> from <> ")-[x" <> (tshow index) <> ":" <> (toBoltName r) <> "]->(" <> to <> ") DELETE x" <> (tshow index) <>" ")
                                 [(0 :: Int)..]
                                 toDelete

          idsInRelationship r = case r of
                                    Depend a b -> [SomeEntityId a, SomeEntityId b]
                                    Contain a b -> [SomeEntityId a, SomeEntityId b]
                                    Access a b -> [SomeEntityId a, SomeEntityId b]

          taskIdsMap = mapId $ nub $ idsInRelationship =<< (toCreates <> toDelete)
          mapId ids = (("t" <>) . tshow) <$> (fromList $ zipWith (,) ids [(0 :: Int)..])

          taskIdsWithNewParents = mapMaybe mapFn toCreates
              where mapFn (Depend _ childId) = Just childId
                    mapFn _ = Nothing

moveDependsOnToAnotherTask :: (MonadIO m) => Pipe -> TaskId -> TaskId -> m [Entity Task]
moveDependsOnToAnotherTask pipe from to = do
    childRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH (from:Task {id: {fromId}})-[x:DEPEND]->(t:Task) "
       <>"MERGE (to:Task {id: {toId}})-[:DEPEND]->(t) "
       <>"DELETE x "
       <>"RETURN t.id;")
        (fromList [("fromId", toBolt from)
                  ,("toId", toBolt to)])
    let childrenIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t.id") <$> childRecords
    ensurePendingTasksParentsState pipe childrenIds

deleteOrphans :: (MonadIO m) => Pipe -> TaskId -> m ()
deleteOrphans pipe taskId =
    Database.Bolt.run pipe $ queryP_ ("MATCH (t:Task {id: {taskId}})-[:DEPEND]->(t2:Task)<-[:DEPEND]-(t3:Task) "
                                    <>"MATCH (t3)-[:DEPEND]->(t4) MATCH (t)-[:DEPEND]->(t5:Task) WHERE t4 <> t5 DELETE t5;")
                                     (fromList [("taskId", toBolt taskId)])

getRandomTaskWithNoDependencies :: MonadIO m => Pipe -> GraphId -> m (Maybe (Entity Task))
getRandomTaskWithNoDependencies pipe graphId = do
    noDependencyIdRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH (g:Graph)-[:CONTAIN]->(t:Task) "
       <>"WHERE g.id = {graphId} "
       <>"AND NOT (t)-[:DEPEND]->() "
       <>"AND t.state = {pending} "
       <>"RETURN t.id")
         (fromList [("graphId", toBolt graphId)
                   ,("pending", toBolt Pending)])
    let noDependencyIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t.id") <$> noDependencyIdRecords

    allDependenciesDoneIdRecords <- Database.Bolt.run pipe $ queryP
        ("MATCH (g:Graph)-[:CONTAIN]->(t:Task)-[:DEPEND]->(t1:Task) "
       <>"WITH g, t, collect(t1) as c "
       <>"WHERE g.id = {graphId} "
       <>"AND t.state = {pending} "
       <>"AND NONE(tx in c WHERE tx.state = {pending}) "
       <>"RETURN t.id")
        (fromList [("graphId", toBolt graphId)
                   ,("pending", toBolt Pending)])
    let allDependenciesDoneIds = catMaybes $ (fmap TaskId) <$> ((=<<) exactMaybe) <$> (lookup "t.id") <$> allDependenciesDoneIdRecords
        allIds = noDependencyIds <> allDependenciesDoneIds
    if null allIds
        then pure Nothing
        else do
            index <- liftIO $ randomRIO (0, length allIds - 1)
            get pipe $ allIds !! index

checkTaskAccess :: MonadIO m => Pipe -> TaskId -> UserId -> m (Maybe (Entity Graph))
checkTaskAccess  pipe taskId userId = do
    records <- Database.Bolt.run pipe $ queryP
        ("MATCH (u:User)-[a:ACCESS]->(g:Graph)-[:CONTAIN]->(t:Task) WHERE u.id = {userId} AND t.id = {taskId} RETURN g LIMIT 1;")
        (fromList [("userId", toBolt userId)
                  ,("taskId", toBolt taskId)])
    pure $ listToMaybe records >>= (lookup "g") >>= readBoltValue

checkGraphAccess :: MonadIO m => Pipe -> GraphId -> UserId -> m Bool
checkGraphAccess pipe graphId userId = do
    relationshipCount <- Database.Bolt.run pipe $ queryP
        ("MATCH (u:User)-[a:ACCESS]->(g:Graph) WHERE u.id = {userId} AND g.id = {graphId} RETURN count(*) as c;")
        (fromList [("userId", toBolt userId)
                  ,("graphId", toBolt graphId)])
    case (lookup "c" $ relationshipCount !! 0) >>= exactMaybe of
        Just (1 :: Int) -> pure True
        _               -> pure False

getSortee :: (MonadIO m, MonadThrow m) => Pipe -> GraphId -> TaskOrder -> m S.Sortee
getSortee pipe graphId NoOrder = do
    records <- Database.Bolt.run pipe $ queryP
        "MATCH (g:Graph)-[:CONTAIN]->(t:Task) WHERE g.id = {graphId} RETURN t.sortee ORDER BY t.sortee DESC LIMIT 1;"
        (fromList $ [("graphId", toBolt graphId)])
    case listToMaybe records >>= (lookup "t.sortee") >>= exactMaybe of
        Just s -> pure $ fromMaybe (error "Impossible - getSortee1") $ S.maybeBetween (Just $ Sortee $ unpack s) Nothing
        Nothing -> pure S.initial

getSortee pipe graphId (Before taskId) = do
    records <- Database.Bolt.run pipe $ queryP
        (  "MATCH (g1:Graph)-[:CONTAIN]->(t1:Task) WHERE g1.id = {graphId} AND t1.id = {taskId} "
        <> "OPTIONAL MATCH (g2:Graph)-[:CONTAIN]->(t2:Task) WHERE g2.id = {graphId} AND t2.sortee < t1.sortee "
        <> "RETURN t1.sortee, t2.sortee ORDER BY t2.sortee DESC LIMIT 1;")
        (fromList $ [("taskId", toBolt taskId)
                    ,("graphId", toBolt graphId)
                    ])
    let record = listToMaybe records
    let mSortee = case (record >>= (lookup "t1.sortee") >>= exactMaybe, record >>= (lookup "t2.sortee")) of
                       (Just t1, Just (N _)) ->
                           S.maybeBetween Nothing $ (Just $ Sortee $ unpack t1)
                       (Just t1, Just (T t2)) ->
                           S.maybeBetween (Just $ Sortee $ unpack t2) (Just $ Sortee $ unpack t1)
                       _ -> Nothing
    case mSortee of
        Just s -> pure s
        Nothing -> throwM $ RefTaskIdNotFound taskId

getSortee pipe graphId (After taskId) = do
    records <- Database.Bolt.run pipe $ queryP
         (  "MATCH (g1:Graph)-[:CONTAIN]->(t1:Task) WHERE g1.id = {graphId} AND t1.id = {taskId} "
         <> "OPTIONAL MATCH (g2:Graph)-[:CONTAIN]->(t2:Task) WHERE g2.id = {graphId} AND t2.sortee > t1.sortee "
         <> "RETURN t1.sortee, t2.sortee ORDER BY t2.sortee LIMIT 1;")
         (fromList $ [("taskId", toBolt taskId)
                     ,("graphId", toBolt graphId)
                     ])
    let record = listToMaybe records
    let mSortee = case (record >>= (lookup "t1.sortee") >>= exactMaybe, record >>= (lookup "t2.sortee")) of
                       (Just t1, Just (N _)) ->
                           S.maybeBetween (Just $ Sortee $ unpack t1) Nothing
                       (Just t1, Just (T t2)) ->
                           S.maybeBetween (Just $ Sortee $ unpack t1) (Just $ Sortee $ unpack t2)
                       _ -> Nothing
    case mSortee of
        Just s -> pure s
        Nothing -> throwM $ RefTaskIdNotFound taskId
