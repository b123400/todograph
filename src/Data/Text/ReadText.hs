module Data.Text.ReadText where

import Data.Text (Text)

class ReadText a where
  readText :: Text -> Maybe a
