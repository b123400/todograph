module Data.Time.Format.ISO8601 where

import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime)
import Data.Time.Format (defaultTimeLocale)

iso8601Show :: UTCTime -> String
iso8601Show = formatTime defaultTimeLocale "%FT%T%QZ"
