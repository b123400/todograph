module Data.Bolt.Read where

import Database.Bolt (Node, Value(..), Structure(..), nodeProps, exactMaybe)
import Data.Map (Map)
import Data.Text (Text)
import Data.Time.Calendar (Day, addDays, fromGregorian)
import Data.Time.Clock (UTCTime(..), addUTCTime, picosecondsToDiffTime)
import Data.Time.Clock.POSIX (posixSecondsToUTCTime)

class ReadBolt a where
    readBolt :: Map Text Value -> Maybe a

    readBoltValue :: Value -> Maybe a
    default readBoltValue :: Value -> Maybe a
    readBoltValue v = exactMaybe v >>= readBoltNode

    readBoltNode :: Node -> Maybe a
    default readBoltNode :: Node -> Maybe a
    readBoltNode = readBolt . nodeProps

class ReadBoltPrimitive a where
    readBoltPrimitive :: Value -> Maybe a

instance ReadBoltPrimitive UTCTime where
    -- See here:
    -- https://github.com/neo4j/neo4j/blob/3.5/community/bolt/src/main/java/org/neo4j/bolt/v2/messaging/Neo4jPackV2.java
    -- Date
    readBoltPrimitive (S (Structure 68 (I epochDays :[]))) =
        Just $ UTCTime (addDays (toInteger epochDays) epoch) 0
    -- LocalDateTime
    readBoltPrimitive (S (Structure 100 (I epochSeconds : I nano : []))) =
        Just $ addUTCTime (realToFrac $ picosecondsToDiffTime (fromIntegral nano) * 1000)
             $ epochToUTC epochSeconds
    -- DateTimeWithZoneOffset
    readBoltPrimitive (S (Structure 70 (I epochSecondsLocal : I nano : I offsetSeconds : []))) =
        Just $ addUTCTime (realToFrac $ picosecondsToDiffTime (fromIntegral nano) * 1000)
             $ addUTCTime (fromIntegral offsetSeconds)
             $ epochToUTC epochSecondsLocal
    readBoltPrimitive _ = Nothing

epoch :: Day
epoch = fromGregorian 1970 1 1

epochToUTC :: Integral a => a -> UTCTime
epochToUTC = posixSecondsToUTCTime . fromIntegral
